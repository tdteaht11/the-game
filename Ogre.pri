# Mac specific
macx {
# Dependencies location
DEP_LOC = $${OGRE_LOC}/Dependencies

    # Include OGRE
    INCLUDEPATH += \
$${OGRE_LOC}/lib/macosx/Debug/Ogre.framework/Versions/1.9.0/Headers \
$${OGRE_LOC}/include

    DEPENDPATH += \
$${OGRE_LOC}/lib/macosx/Debug/Ogre.framework/Versions/1.9.0/Headers \
$${OGRE_LOC}/include

    # Include dependencies
    INCLUDEPATH += $${DEP_LOC}/include
    DEPENDPATH += $${DEP_LOC}/include

    # Dependency libraries
    LIBS += -L$${DEP_LOC}/lib
    release: LIBS += -L$${DEP_LOC}/lib/Release
    debug: LIBS += -L$${DEP_LOC}/lib/Debug

    # Frameworks
    INCLUDEPATH += $${OGRE_LOC}/lib/macosx/Debug
    DEPENDPATH += $${OGRE_LOC}/lib/macosx/Debug

    LIBS += -F$${OGRE_LOC}/lib/macosx/Debug \
	-framework CoreFoundation \
	-framework Cocoa \
	-framework IOKit \
	-framework Ogre \
	-framework RenderSystem_GL
}

# Linux specific
unix:!macx {
    INCLUDEPATH += /usr/local/include/OGRE
    DEPENDPATH += /usr/local/include/OGRE

    LIBS += \
    -L/usr/local/lib \
	-L/usr/local/lib/OGRE

    LIBS += -lOgreMain -lboost_system
}
