#ifndef PHYSICSOBJECT_H
#define PHYSICSOBJECT_H

#include "PreProcessor.h"
#include "PhysicsRoot.h"
#include "OgreDebugDrawer.h"
#include "CollisionPair.h"
#include "CustomData.h"
#include "MyMap.h"
#include "Base.h"

namespace Physics {

/**
 * @brief The PhysicsObject class is the interface that the game will use when communicating with the physics library
 */
class PhysicsObject {
public:
	PhysicsObject(PhysicsCallback *pc, const usint &aps);
	virtual ~PhysicsObject();

	/**
	 * @brief Set the velocity of an entity. Used to shoot projectiles and whatnot!
	 * @param Ogre entity
	 * @param The desired velocity
	 * @param true if the entity should react to gravity
	 */
	void setEntityVelocity(Ogre::Entity *entity, const Ogre::Vector3 &velocity, const bool &gravity = true);

	/**
	 * @brief Set impulse on an entity. Used to shoot projectiles and whatnot!
	 * @param Ogre entity
	 * @param Impulse
	 */
	void setEntityImpulse(Ogre::Entity *entity, const Ogre::Vector3 &impulse);

	/**
	 * @brief registerEntity Takes data from an Ogre entity and scene node to create a bullet body
	 * @param entity Ogre::Entity
	 * @param node Ogre::SceneNode
	 * @param mass btScalar with the mass of the body
	 * @param special bool that says if the body is special, e.g. a weapon. False by default
	 */
	btRigidBody *registerEntity(Ogre::Entity *entity, Ogre::SceneNode *node, const btScalar &mass, bool special = false);

	/**
	 * @brief registerStaticEntity Same as registerEntity but with static entities
	 * @param entity Ogre::Entity
	 * @param node Ogre::SceneNode
	 */
	void registerStaticEntity(Ogre::Entity *entity, Ogre::SceneNode *node);

	/**
	 * @brief removeEntity Removes a bullet body with the help of an Ogre::Entity
	 * @param entity Ogre::Entity
	 */
	void removeEntity(Ogre::Entity *entity);

	/**
	 * @brief Remove a bullet body
	 * @param Name of the bullet body
	 */
	void removeEntity(const std::string name);

	/**
	 * @brief moveEntity Moves a bullet body
	 * @param entity Ogre::MovableObject
	 * @param transVec Ogre::Vector3 Where the body will move to
	 */
	void moveEntity(Ogre::MovableObject *entity, const Ogre::Vector3 &transVec);

	/**
	 * @brief jumpEntity Makes a body jump
	 * @param entity Ogre::MovableObject
	 */
	void jumpEntity(Ogre::MovableObject *entity);

	/**
	 * @brief setEntityPosition Set the position of the body
	 * @param entity Ogre::MovableObject
	 * @param position Ogre::Vector3 with the position
	 */
	void setEntityPosition(Ogre::MovableObject *entity, const Ogre::Vector3 &position);

	/**
	 * @brief setEntityRotation Set the rotation of the body
	 * @param entity Ogre::MovableObject
	 * @param rotation Ogre::Quaternion with the rotation
	 */
	void setEntityRotation(Ogre::MovableObject *entity, const Ogre::Quaternion &rotation);

	/**
	 * @brief Set body as character
	 * @param Name of the body
	 */
	void setAsCharacter(const std::string &name) const;

	/**
	 * @brief Perform a raycast between two points
	 * @param start Ogre::Vector3 start point
	 * @param end Ogre::Vector3 end point
	 * @return bool True if the raycast hit something. If not, false.
	 */
	bool raycast(const Ogre::Vector3 &start, const Ogre::Vector3 &end) const;

	/**
	 * @brief Register body and listen to collisions
	 * @param Name of the body
	 */
	void setCollision(const std::string &name);

	CustomData *getCustomData(const std::string &name) const;

	/**
	 * @brief simulate Simulate the dynamics world
	 */
	void simulate();

	/**
	 * @brief registerBase Register the base
	 * @param node
	 */
	void registerBase(Ogre::SceneNode *node);

#ifdef _DEBUG
	inline void setDebugDrawer(btIDebugDraw *debugDrawer){
		debugDrawer->setDebugMode(btIDebugDraw::DBG_DrawWireframe);
		root->getDynamicsWorld()->setDebugDrawer(debugDrawer);
	}
#endif
private:
	/**
	 * @brief PhysicsObject Private copy constructor, do not use.
	 * @param obj PhysicsObject
	 */
	PhysicsObject(const PhysicsObject &obj) :
		simulationTimeStep(0) {
		throw std::runtime_error("Not allowed to copy PhysicsObject");
	}

	typedef MyMap<std::string, btRigidBody *> RigidBodyList;

	const double simulationTimeStep;
	PhysicsRoot *root;
	PhysicsCallback *callback;
	RigidBodyList bodies;
	RigidBodyList specialBodies;
	Base *base;

#ifdef _DEBUG
	bool isSimulating;
#endif

	/**
	 * @brief checkCollisions is used during simulation
	 */
	CollisionPair::List checkCollisions() const;

	/**
	 * @brief performRaycast is used during simulation
	 */
	void performRaycast() const;

	/**
	 * @brief updateBodies is used during simulation
	 */
	void updateBodies() const;

	/**
	 * @brief setEntityPosition moves body to a position
	 * @param body btRigidBody the body to be moved
	 * @param position Ogre::Vector3 the position
	 */
	void setEntityPosition(btRigidBody *const body, const Ogre::Vector3 &position);

	/**
	 * @brief setEntityRotation rotates a body
	 * @param body btRigidBody the body to be rotated
	 * @param rotation Ogre::Quaternion the rotation
	 */
	void setEntityRotation(btRigidBody *const body, const Ogre::Quaternion &rotation);

	/**
	 * @brief checkBase Call to base listener
	 */
	void checkBase() const;

};

}

#endif // PHYSICSOBJECT_H
