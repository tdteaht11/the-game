TEMPLATE = lib
CONFIG -= app_bundle
CONFIG -= qt

macx {
CONFIG += lib
}

DEFINES += DEBUG _DEBUG
TARGET = physics
DESTDIR = $${PWD}/../lib

SOURCES += \
    PhysicsRoot.cpp \
    PhysicsObject.cpp \
    OgreDebugDrawer.cpp \
    OgreUtils.cpp \
    OgreCollisionShape.cpp \
    CollisionPair.cpp \
    GhostObject.cpp \
    Base.cpp

HEADERS += \
    Physics.h \
    PreProcessor.h \
    PhysicsRoot.h \
    PhysicsObject.h \
    OgreDebugDrawer.h \
    OgreUtils.h \
    OgreCollisionShape.h \
    CustomData.h \
    PhysicsCallback.h \
    CollisionPair.h \
    MyMap.h \
    GhostObject.h \
    Base.h

include(../Bullet.pri)
include(../Ogre.pri)
