#include "Base.h"
#include "OgreUtils.h"

namespace Physics {

Base::Base(Ogre::SceneNode *node) :
	GhostObject(new btCylinderShape(btVector3(2, 2, 2))),
	node(node)
{
	btTransform trans;
	trans.setOrigin(OgreUtils::toBulletVector(node->getPosition()));
	trans.setRotation(OgreUtils::toBulletQuaternion(node->getOrientation()));
	setWorldTransform(trans);
}

Base::~Base()
{

}

}
