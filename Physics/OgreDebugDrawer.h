#ifndef OGREDEBUGDRAWER_H
#define OGREDEBUGDRAWER_H

#include "PreProcessor.h"

namespace Physics {

class OgreDebugDrawer: public btIDebugDraw, Ogre::FrameListener {
public:
	OgreDebugDrawer(Ogre::SceneManager *scene, const std::string &materialName = "DebugLines");
	OgreDebugDrawer(const OgreDebugDrawer &debug);
	virtual ~OgreDebugDrawer ();

	const std::string materialName;

private:
	Ogre::SceneManager *scene;
	Ogre::ManualObject *lines;
	Ogre::ManualObject *triangles;
	DebugDrawModes drawMode;

	struct ContactPoint {
		Ogre::Vector3 from;
		Ogre::Vector3 to;
		Ogre::ColourValue color;
		ulint dieTime;
	};

	typedef std::list<ContactPoint> ContactPointList;
	ContactPointList contactList;
	ulint timer;

	void init();
	void drawContactPointList();

	// btIDebugDraw interface
public:
	virtual void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color);
	virtual void drawContactPoint(const btVector3 &PointOnB, const btVector3 &normalOnB, btScalar distance, int lifeTime, const btVector3 &color);
	virtual void reportErrorWarning(const char *warningString);
	virtual void draw3dText(const btVector3 &location, const char *textString);
	virtual void setDebugMode(int debugMode);
	int getDebugMode() const;
	virtual void drawTriangle(const btVector3 &v0, const btVector3 &v1, const btVector3 &v2, const btVector3 &color, btScalar alpha);

	// FrameListener interface
public:
	virtual bool frameEnded(const Ogre::FrameEvent &evt);
	virtual bool frameStarted(const Ogre::FrameEvent &evt);
};

}

#endif // OGREDEBUGDRAWER_H
