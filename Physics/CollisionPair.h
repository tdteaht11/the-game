#ifndef COLLISIONPAIR_H
#define COLLISIONPAIR_H

#include "PreProcessor.h"

namespace Physics {

/**
 * @brief Contains the keys about two colliding entities
 */
class CollisionPair
{
public:
	typedef std::set<CollisionPair> List;

	CollisionPair();
	CollisionPair(const std::string a, const std::string b);
	CollisionPair(const CollisionPair &pair);
	virtual ~CollisionPair();

	std::string getA() const;
	void setA(const std::string &value);

	std::string getB() const;
	void setB(const std::string &value);

	// Operator overloading
	bool operator==(const CollisionPair &rhs) const;
	bool operator<(const CollisionPair &rhs) const;
	bool operator>(const CollisionPair &rhs) const;
private:
	std::string a;
	std::string b;
};

}

#endif // COLLITIONPAIR_H
