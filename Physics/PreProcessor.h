#ifndef PHYSICS_PREPROCESSOR_H
#define PHYSICS_PREPROCESSOR_H

// stl
#include <stdexcept>
#include <vector>
#include <string.h>
#include <list>
#include <map>

#pragma GCC diagnostic ignored "-Wall"
#include <Ogre.h>
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionShapes/btShapeHull.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <BulletCollision/NarrowPhaseCollision/btRaycastCallback.h>
#include <BulletCollision/Gimpact/btGImpactShape.h>
#pragma GCC diagnostic pop

typedef short int sint;
typedef unsigned short int usint;
typedef unsigned int uint;
typedef long int lint;
typedef unsigned long int ulint;
typedef unsigned char uchar;

#endif // PHYSICS_PREPROCESSOR_H
