#ifndef PHYSICS_MYMAP_H
#define PHYSICS_MYMAP_H

#include "PreProcessor.h"

namespace Physics {

template<typename KEY, typename VAL>
class MyMap : public std::map<KEY, VAL>
{
public:
	inline virtual ~MyMap() {

	}

	/**
	 * @brief getValue Get a value from the map
	 * @param key The key to find
	 * @return The value of key
	 * @throws std::runtime_error
	 */
	inline VAL getValue(const KEY &key) const throw(std::runtime_error) {
		typename std::map<KEY, VAL>::const_iterator it = this->find(key);

		if(it == this->end()) {
			throw std::runtime_error("Could not find " + key);
		}

		return it->second;
	}

	/**
	 * @brief inMap Check if key exists in map
	 * @param key Key to check
	 * @return true if key exists in the map
	 */
	inline bool inMap(const KEY &key) const {
		try {
			getValue(key);
			return true;
		} catch(const std::exception &ex) {

		}

		return false;
	}
};

}

#endif // MYMAP_H
