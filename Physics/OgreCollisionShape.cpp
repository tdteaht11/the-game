#include "OgreCollisionShape.h"

namespace Physics {

OgreCollisionShape::OgreCollisionShape(Ogre::MeshPtr mesh) :
	mesh(mesh) {

}

OgreCollisionShape::OgreCollisionShape(const OgreCollisionShape &shape) :
	mesh(shape.mesh) {

}

OgreCollisionShape::~OgreCollisionShape() {

}

btCollisionShape *OgreCollisionShape::convertDynamic() {
	Cache::List &cacheShapes = Cache::getInstance().cacheShapes;
	// Check if mesh exists in the cache
	Cache::List::const_iterator it = cacheShapes.find(mesh->getName());
	if(it != cacheShapes.end()) {
		return it->second;
	}

	VertexList vertices(0);
	IndicesList indices(0);
	bool use32bitindexes;

	getMeschInformation(vertices, indices, use32bitindexes);

	// The indices should always come 3 in a pair, i.e. indices size should be divisible by 3
	if(indices.size() % 3 != 0) {
		throw std::runtime_error("Indices for " + mesh->getName() + " is invalid");
	}

	btCollisionShape *shape = createShape(vertices, indices, use32bitindexes);
	// Add in cache
	cacheShapes[mesh->getName()] = shape;
	return shape;
}

void OgreCollisionShape::getMeschInformation(OgreCollisionShape::VertexList &vertices, OgreCollisionShape::IndicesList &indices, bool &use32bitindexes) const {
	Ogre::Mesh::SubMeshIterator subIt = mesh->getSubMeshIterator();
	while(subIt.hasMoreElements()) {
		Ogre::SubMesh &subMesh = *subIt.getNext();

		const Ogre::VertexData *vertexData = subMesh.useSharedVertices ?
					mesh->sharedVertexData : subMesh.vertexData;

		// Get all vertices and merge them in to the vertex vector
		const VertexList tmpVertices = getVertices(*vertexData);
		vertices.reserve(vertices.size() + tmpVertices.size());
		vertices.insert(vertices.end(), tmpVertices.begin(), tmpVertices.end());

		// Get all indices and merge them into the indices vector
		const IndicesList tmpIndices = getIndices(*subMesh.indexData, use32bitindexes);
		//indices.reserve(indices.size() + tmpIndices.size());
		indices.insert(indices.end(), tmpIndices.begin(), tmpIndices.end());
	}
}

// http://www.bulletphysics.org/mediawiki-1.5.8/index.php/BtShapeHull_vertex_reduction_utility
btConvexHullShape *OgreCollisionShape::optimizeMesh(const btConvexTriangleMeshShape &shape) {
	btShapeHull hull(&shape);
	const btScalar margin = shape.getMargin();
	hull.buildHull(margin);

	btConvexHullShape *convexShape = new btConvexHullShape();
	const btVector3 *const vertexPointer = hull.getVertexPointer();
	for(int i = 0; i < hull.numVertices(); i++) {
		convexShape->addPoint(vertexPointer[i]);
	}

	convexShape->recalcLocalAabb();
	return convexShape;
}

btCollisionShape *OgreCollisionShape::createShape(const OgreCollisionShape::VertexList &vertices, const OgreCollisionShape::IndicesList &indices, const bool &use32bitindexes) {
	// The Bullet triangle mesh
	btTriangleMesh *triMesh = new btTriangleMesh(use32bitindexes);

	// For every triangle
	for (IndicesList::const_iterator indicesIt = indices.begin(); indicesIt != indices.end();  ) {
		const ulint indice1 = *indicesIt++;
		const ulint indice2 = *indicesIt++;
		const ulint indice3 = *indicesIt++;

		// Set each vertex
		const btVector3 vert0(vertices[indice1].x, vertices[indice1].y, vertices[indice1].z);
		const btVector3 vert1(vertices[indice2].x, vertices[indice2].y, vertices[indice2].z);
		const btVector3 vert2(vertices[indice3].x, vertices[indice3].y, vertices[indice3].z);

		// Add it into the trimesh
		triMesh->addTriangle(vert0, vert1, vert2);
	}

	btConvexTriangleMeshShape *triangleShape = new btConvexTriangleMeshShape(triMesh);

	// If there is more then or equal to 100 vertices, optimize the shape. Recommendation
	// from bullet
	const ulint numVertices = indices.size();
	if(numVertices >= 100) {
		btConvexHullShape *convexShape = optimizeMesh(*triangleShape);
		delete triangleShape;
		return convexShape;
	}

	return triangleShape;
}

OgreCollisionShape::VertexList OgreCollisionShape::getVertices(const Ogre::VertexData &vertexData) {
	// Get the position element
	const Ogre::VertexElement* posElem = vertexData.vertexDeclaration
			->findElementBySemantic(Ogre::VES_POSITION);

	// Get a pointer to the vertex buffer
	Ogre::HardwareVertexBufferSharedPtr vBuffer = vertexData.vertexBufferBinding
			->getBuffer(posElem->getSource());

	// Allocate space for the vertices and indices
	VertexList vertices(vertexData.vertexCount);

	// Lock the vertex buffer (READ ONLY)
	uchar* vertex =   static_cast<uchar*>(vBuffer->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

	for (uint i = 0; i < vertexData.vertexCount; i++, vertex += vBuffer->getVertexSize() ) {
		float* pReal;
		posElem->baseVertexPointerToElement(vertex, &pReal);
		vertices[i] = Ogre::Vector3(pReal[0], pReal[1], pReal[2]);
	}

	vBuffer->unlock();
	return vertices;
}

OgreCollisionShape::IndicesList OgreCollisionShape::getIndices(const Ogre::IndexData &indexData, bool &use32BitIndexes) {
	// Get a pointer to the index buffer
	Ogre::HardwareIndexBufferSharedPtr iBuffer = indexData.indexBuffer;
	IndicesList indices(indexData.indexCount);
	use32BitIndexes = iBuffer->getType() == Ogre::HardwareIndexBuffer::IT_32BIT;
	// Lock the index buffer (READ ONLY)
	ulint* pLong = static_cast<ulint*>(iBuffer->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

	IndicesList::iterator indicesIt = indices.begin();
	if (use32BitIndexes) {
		for (uint i = 0; indicesIt != indices.end(); i++, indicesIt++) {
			*indicesIt = pLong[i];
		}
	} else {
		usint* pShort = reinterpret_cast<usint*>(pLong);
		for (uint i = 0; indicesIt != indices.end(); i++, indicesIt++) {
			*indicesIt = static_cast<ulint>(pShort[i]);
		}
	}

	iBuffer->unlock();
	return indices;
}

}
