#include "GhostObject.h"

namespace Physics {

GhostObject::GhostObject(btCollisionShape *shape) :
	btGhostObject(),
	shape(shape)
{
	setCollisionFlags(getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
	setCollisionShape(shape);
}

GhostObject::~GhostObject()
{
	delete shape;
}

}
