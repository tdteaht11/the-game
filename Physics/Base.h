#ifndef BASE_H
#define BASE_H

#include "PreProcessor.h"
#include "GhostObject.h"

namespace Physics {

class Base : public GhostObject
{
public:
	Base(Ogre::SceneNode *node);
	virtual ~Base();

private:
	Ogre::SceneNode *node;
};

}

#endif // BASE_H
