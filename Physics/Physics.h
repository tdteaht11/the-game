#ifndef PHYSICS_H
#define PHYSICS_H

#include "PhysicsObject.h"
#include "PhysicsRoot.h"
#include "PhysicsCallback.h"
#include "CustomData.h"
#include "OgreDebugDrawer.h"
#include "OgreCollisionShape.h"
#include "OgreUtils.h"
#include "CollisionPair.h"

#endif // PHYSICS_H
