#ifndef GHOSTOBJECT_H
#define GHOSTOBJECT_H

#include "PreProcessor.h"

namespace Physics {

class GhostObject : public btGhostObject
{
public:
	GhostObject(btCollisionShape *shape);
	virtual ~GhostObject();
private:
	btCollisionShape *shape;
};

}

#endif // GHOSTOBJECT_H
