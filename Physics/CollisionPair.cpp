#include "CollisionPair.h"

namespace Physics {

CollisionPair::CollisionPair()
{
}

CollisionPair::CollisionPair(const std::string a, const std::string b) :
	a(a),
	b(b)
{

}

CollisionPair::CollisionPair(const CollisionPair &pair) :
	a(pair.a),
	b(pair.b)
{

}

CollisionPair::~CollisionPair()
{

}

std::string CollisionPair::getA() const
{
	return a;
}

void CollisionPair::setA(const std::string &value)
{
	a = value;
}

std::string CollisionPair::getB() const
{
	return b;
}

void CollisionPair::setB(const std::string &value)
{
	b = value;
}

bool CollisionPair::operator==(const CollisionPair &rhs) const
{
	return a == rhs.a && b == rhs.b
			|| b == rhs.a && a == rhs.b;
}

bool CollisionPair::operator<(const CollisionPair &rhs) const
{
	return a < rhs.a && b < rhs.b
			|| a < rhs.b && b < rhs.a;
}

bool CollisionPair::operator>(const CollisionPair &rhs) const
{
	return a > rhs.a && b > rhs.b
			|| a > rhs.b && b > rhs.a;
}

}
