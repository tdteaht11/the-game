#ifndef PHYSICSCALLBACK_H
#define PHYSICSCALLBACK_H

#include "PreProcessor.h"
#include "CollisionPair.h"

namespace Physics {

/**
 * @brief The PhysicsCallback class is an abstract class used for callbacks from the physics library to the game
 */
class PhysicsCallback {
public:

	/**
	 * @brief onContact will be called when there is a contact
	 * @param Set of keys that has collided
	 */
	virtual void onContact(const CollisionPair::List &collisionList) = 0;

	/**
	 * @brief onBaseContact Callback when someone enters the base
	 * @param name
	 */
	virtual void onBaseContact(const std::string &name) = 0;

};

}
#endif // PHYSICSCALLBACK_H
