#ifndef OGRECOLLISIONSHAPE_H
#define OGRECOLLISIONSHAPE_H

#include "PreProcessor.h"

namespace Physics {

/**
 * @brief The OgreCollisionShape class dynamically converts OGRE meshes to bullet collision shapes
 */
class OgreCollisionShape {
public:
	typedef std::vector<Ogre::Vector3> VertexList;
	typedef std::list<ulint> IndicesList;

	OgreCollisionShape(Ogre::MeshPtr mesh);
	OgreCollisionShape(const OgreCollisionShape &shape);
	virtual ~OgreCollisionShape();

	/**
	 * @brief Convert the mesh, given in the constructor, to a dynamic body
	 * @return Dynamic bullet body of mesh
	 */
	btCollisionShape *convertDynamic();

	/**
	 * @brief Get information about the mesh
	 * @param List where vertices about the mesh till be stored
	 * @param List where the mesh indices will be stored
	 * @param True if the mesh uses 32 bit indexes
	 */
	void getMeschInformation(VertexList &vertices, IndicesList &indices, bool &use32bitindexes) const;

	/**
	 * @brief Optimizes hull shapes. This should be done on shapes with >= 100 vertices
	 * @param The shape to be optimized
	 * @return  The optimized version of the shape param. The param shape can be deleted.
	 */
	static btConvexHullShape *optimizeMesh(const btConvexTriangleMeshShape &shape);

	/**
	 * @brief Create the shape given from vertices and indices
	 * @param The vertices to construct the shape from
	 * @param The indices to construct the shape form
	 * @param If the shap should use 32 bit indexes
	 * @return The created bullet collision shape
	 */
	static btCollisionShape *createShape(const VertexList &vertices, const IndicesList &indices, const bool &use32bitindexes);

	/**
	 * @brief Get vertices from a given mesh vertice data
	 * @param Mesh vertex data
	 * @return List of vertices
	 */
	static VertexList getVertices(const Ogre::VertexData &vertexData);

	/**
	 * @brief Get indices from a mesh index that
	 * @param Mesh index data
	 * @param Will return if the indices uses 32 bit indexes
	 * @return List of indices
	 */
	static IndicesList getIndices(const Ogre::IndexData &data, bool &use32bitindexes);
private:
	/**
	 * @brief Mesh to construct shapes from
	 */
	Ogre::MeshPtr mesh;

	/**
	 * @brief Singleton class to encapsulate a static cache list
	 */
	class Cache {
	public:
		typedef std::map<std::string, btCollisionShape *> List;

		inline static Cache &getInstance() {
			static Cache reference;
			return reference;
		}

		inline virtual ~Cache() {
			for(List::iterator it = cacheShapes.begin(); it != cacheShapes.end(); it++) {
				delete it->second;
			}
		}

		List cacheShapes;
	private:
		// Singleton
		inline Cache() {

		}

		// Do not allow copy
		inline Cache(const Cache &cache) {
			throw std::runtime_error("Not allowed to copy OgreCollisionShapeCache");
		}
	};
};

}

#endif // OGRECOLLISIONSHAPE_H
