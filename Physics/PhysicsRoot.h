#ifndef PHYSICSROOT_H
#define PHYSICSROOT_H

#include "PreProcessor.h"
#include "PhysicsCallback.h"

#ifdef _DEBUG
#include "OgreDebugDrawer.h"
#endif

namespace Physics {

/**
 * @brief The PhysicsRoot class
 */
class PhysicsRoot {
public:

	/**
	 * @brief PhysicsRoot Instantiates the physics with earthlike gravitation and a gound plane
	 */
	PhysicsRoot();
	virtual ~PhysicsRoot();

	/**
	 * @brief getDynamicsWorld
	 * @return btDiscreteDynamicsWorld The physics world
	 */
	btDiscreteDynamicsWorld *getDynamicsWorld() const;

	/**
	 * @brief addRigidBody Add a rigid body to the world
	 * @param body btRigidBody that is the body to be added
	 */
	void addRigidBody(btRigidBody *body) const;

	/**
	 * @brief removeRigidBody Remove a rigid body from the world
	 * @param body btRigidBody that is to be removed
	 */
	void removeRigidBody(btRigidBody *body) const;

	/**
	 * @brief addCollisionBody
	 * @param body
	 */
	void addCollisionBody(btCollisionObject *body) const;

	/**
	 * @brief removeCollisionBody
	 * @param body
	 */
	void removeCollisionBody(btCollisionObject *body) const;

private:
	btBroadphaseInterface *broadphase;
	btDefaultCollisionConfiguration *collisionCfg;
	btCollisionDispatcher *dispatcher;
	btSequentialImpulseConstraintSolver *solver;
	btDiscreteDynamicsWorld *dynamicsWorld;
	btGhostPairCallback *ghostCallback;
};
}
#endif // PHYSICSROOT_H
