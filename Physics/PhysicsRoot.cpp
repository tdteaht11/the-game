#include "PhysicsRoot.h"

namespace Physics {

PhysicsRoot::PhysicsRoot() :
	// Choose broadphase algorithm for collision detection (?)
	broadphase(new btDbvtBroadphase()),
	// Used for full collision detection
	collisionCfg(new btDefaultCollisionConfiguration()),
	dispatcher(new btCollisionDispatcher(collisionCfg)),
	// Solver, used for calculating gravity etc
	solver(new btSequentialImpulseConstraintSolver()),
	// Create a dynamics world
	dynamicsWorld(new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionCfg))
{
#ifdef _DEBUG
	assert(dynamicsWorld);
#endif

	// Set gravity
	dynamicsWorld->setGravity(btVector3(0, -9.8, 0));
	ghostCallback = new btGhostPairCallback;
	dynamicsWorld->getPairCache()->setInternalGhostPairCallback(ghostCallback);
}

PhysicsRoot::~PhysicsRoot(){
	delete ghostCallback;
	delete dynamicsWorld;
	delete solver;
	delete dispatcher;
	delete collisionCfg;
	delete broadphase;
}

btDiscreteDynamicsWorld *PhysicsRoot::getDynamicsWorld() const{
	return dynamicsWorld;
}

void PhysicsRoot::addRigidBody(btRigidBody *body) const{
	dynamicsWorld->addRigidBody(body);
}

void PhysicsRoot::removeRigidBody(btRigidBody *body) const{
	dynamicsWorld->removeRigidBody(body);
}

void PhysicsRoot::addCollisionBody(btCollisionObject *body) const
{
	dynamicsWorld->addCollisionObject(body);
}

void PhysicsRoot::removeCollisionBody(btCollisionObject *body) const
{
	dynamicsWorld->removeCollisionObject(body);
}

}
