#ifndef PHYSICS_CUSTOMDATA_H
#define PHYSICS_CUSTOMDATA_H

#include "PreProcessor.h"

namespace Physics {

/**
 * @brief The CustomData class is used for storing custom data about an object
 */
class CustomData {
public:
	CustomData() {}
	CustomData(Ogre::SceneNode *sn) { sceneNode = sn; airborne = false; }
	CustomData(Ogre::SceneNode *sn, const std::string &n) { sceneNode = sn; name = n; airborne = false; }

	CustomData(const CustomData &cd){ sceneNode = cd.sceneNode; name = cd.name; airborne = cd.airborne; }

	std::string getName() const { return name; }
	void setName(const std::string &n) { name = n; }

	Ogre::SceneNode *getSceneNode() const { return sceneNode; }
	void setSceneNode(Ogre::SceneNode *sn) { sceneNode = sn; }

	bool isAirborne() const { return airborne; }
	void setAirborne(const bool &a) { airborne = a; }

	bool isMoving() const { return moving; }
	void setMoving(const bool &m) { moving = m; }
private:
	std::string name;
	Ogre::SceneNode *sceneNode;
	bool airborne;
	bool moving;
};

}

#endif // PHYSICS_CUSTOMDATA_H
