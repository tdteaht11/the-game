#ifndef PHYSICS_OGREUTILS_H
#define PHYSICS_OGREUTILS_H

#include "PreProcessor.h"

namespace Physics {

namespace OgreUtils {

Ogre::Vector3 toOgreVector(const btVector3 &vector);
Ogre::ColourValue toOgreColor(const btVector3 &vector, const btScalar &alpha = 1.0f);
btVector3 toBulletVector(const Ogre::Vector3 &vector);
btQuaternion toBulletQuaternion(const Ogre::Quaternion &quaternion);

}

}

#endif // OGREUTILS_H
