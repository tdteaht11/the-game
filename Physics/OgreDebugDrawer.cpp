#include "OgreDebugDrawer.h"
#include "OgreUtils.h"

namespace Physics {

OgreDebugDrawer::OgreDebugDrawer(Ogre::SceneManager *scene, const std::string &materialName) :
	// Get first scene manager
	scene(scene),
	lines(new Ogre::ManualObject("PHYSICS_LINES")),
	triangles(new Ogre::ManualObject("PHYSICS_TRIANGLES")),
	materialName(materialName),
	timer(0){

	init();
}

OgreDebugDrawer::OgreDebugDrawer(const OgreDebugDrawer &debug) :
	scene(debug.scene),
	lines(new Ogre::ManualObject(*debug.lines)),
	triangles(new Ogre::ManualObject(*debug.triangles)),
	materialName(debug.materialName),
	contactList(debug.contactList),
	timer(debug.timer){

	init();
}

OgreDebugDrawer::~OgreDebugDrawer() {
	Ogre::Root::getSingletonPtr()->removeFrameListener(this);
	delete lines;
	delete triangles;
}

void OgreDebugDrawer::init() {
	lines->setDynamic(true);
	triangles->setDynamic(true);

	scene->getRootSceneNode()->attachObject(lines);
	scene->getRootSceneNode()->attachObject(triangles);

	lines->begin(materialName, Ogre::RenderOperation::OT_LINE_LIST);
	lines->position(Ogre::Vector3::ZERO);
	lines->colour(Ogre::ColourValue::Blue);
	lines->position(Ogre::Vector3::ZERO);
	lines->colour(Ogre::ColourValue::Blue);

	triangles->begin(materialName, Ogre::RenderOperation::OT_TRIANGLE_LIST);
	triangles->position(Ogre::Vector3::ZERO);
	triangles->colour(Ogre::ColourValue::Blue);
	triangles->position(Ogre::Vector3::ZERO);
	triangles->colour(Ogre::ColourValue::Blue);
	triangles->position(Ogre::Vector3::ZERO);
	triangles->colour(Ogre::ColourValue::Blue);

	Ogre::Root::getSingletonPtr()->addFrameListener(this);
}

void OgreDebugDrawer::drawContactPointList() {
	const ulint now = Ogre::Root::getSingleton().getTimer()->getMilliseconds();

	for(ContactPointList::iterator it = contactList.begin(); it != contactList.end(); it++){
		ContactPoint cp = *it;

		// Delete the ones that has died
		if(now > cp.dieTime) {
			contactList.erase(it);
			continue;
		}

		// The ones that are still alive, draw them
		lines->position(cp.from);
		lines->colour(cp.color);
		lines->position(cp.to);
		lines->colour(cp.color);
	}
}

void OgreDebugDrawer::drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color) {
	Ogre::ColourValue ogreColor = OgreUtils::toOgreColor(color);

	lines->position(OgreUtils::toOgreVector(from));
	lines->colour(ogreColor);
	lines->position(OgreUtils::toOgreVector(to));
	lines->colour(ogreColor);
}

void OgreDebugDrawer::drawContactPoint(const btVector3 &PointOnB, const btVector3 &normalOnB, btScalar distance, int lifeTime, const btVector3 &color) {
	ContactPoint p;
	p.from = OgreUtils::toOgreVector(PointOnB);
	p.to = p.from + OgreUtils::toOgreVector(normalOnB) *  distance;
	p.dieTime = Ogre::Root::getSingleton().getTimer()->getMilliseconds() + lifeTime;
	p.color = OgreUtils::toOgreColor(color);

	contactList.push_back(p);
}

void OgreDebugDrawer::reportErrorWarning(const char *warningString) {
	Ogre::LogManager::getSingleton().getDefaultLog()
			->logMessage(warningString, Ogre::LML_CRITICAL);
}

void OgreDebugDrawer::draw3dText(const btVector3 &location, const char *textString) {
	static std::string text = "draw3dText: ";
	text.append(textString);
	Ogre::LogManager::getSingleton().getDefaultLog()
			->logMessage(text, Ogre::LML_NORMAL);
}

void OgreDebugDrawer::setDebugMode(int debugMode) {
	this->drawMode = (DebugDrawModes) debugMode;
}

int OgreDebugDrawer::getDebugMode() const {
	return drawMode;
}

void OgreDebugDrawer::drawTriangle(const btVector3 &v0, const btVector3 &v1, const btVector3 &v2, const btVector3 &color, btScalar alpha) {
	Ogre::ColourValue ogreColor = OgreUtils::toOgreColor(color, alpha);

	triangles->position(OgreUtils::toOgreVector(v0));
	triangles->colour(ogreColor);
	triangles->position(OgreUtils::toOgreVector(v1));
	triangles->colour(ogreColor);
	triangles->position(OgreUtils::toOgreVector(v2));
	triangles->colour(ogreColor);
}

bool OgreDebugDrawer::frameEnded(const Ogre::FrameEvent &evt) {
	const ulint now = Ogre::Root::getSingleton().getTimer()->getMilliseconds();
	if(now > timer) {
		timer = now + 250;

		lines->clear();
		triangles->clear();
	}

	lines->begin(materialName, Ogre::RenderOperation::OT_LINE_LIST);
	triangles->begin(materialName, Ogre::RenderOperation::OT_TRIANGLE_LIST);
	return true;
}

bool OgreDebugDrawer::frameStarted(const Ogre::FrameEvent &evt) {
	drawContactPointList();
	lines->end();
	triangles->end();
	return true;
}

}
