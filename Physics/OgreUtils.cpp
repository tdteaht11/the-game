#include "OgreUtils.h"

namespace Physics {

namespace OgreUtils {

Ogre::Vector3 toOgreVector(const btVector3 &vector) {
	return Ogre::Vector3(vector.x(), vector.y(), vector.z());
}

Ogre::ColourValue toOgreColor(const btVector3 &vector, const btScalar &alpha) {
	Ogre::ColourValue color(vector.x(), vector.y(), vector.z(), alpha);
	color.saturate();
	return color;
}

btVector3 toBulletVector(const Ogre::Vector3 &vector) {
	return btVector3(vector.x, vector.y, vector.z);
}

btQuaternion toBulletQuaternion(const Ogre::Quaternion &quaternion) {
	return btQuaternion(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
}

}

}
