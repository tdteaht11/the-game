#include "PhysicsObject.h"
#include "OgreCollisionShape.h"
#include "OgreUtils.h"
#include "CustomData.h"

namespace Physics {

PhysicsObject::PhysicsObject(PhysicsCallback *pc, const usint &aps) :
	callback(pc),
	simulationTimeStep(1 / (double) aps),
	root(new PhysicsRoot()),
	base(0)
{
#ifdef _DEBUG
	assert(simulationTimeStep > 0);
#endif
}

PhysicsObject::~PhysicsObject() {
	btRigidBody *tmp;

	for(RigidBodyList::iterator it = bodies.begin(); it != bodies.end(); it++) {
		tmp = it->second;

		// Compiler whines when deleting a void pointer
		CustomData *cd = static_cast<CustomData *>(tmp->getUserPointer());
		delete cd;

		delete tmp->getMotionState();
		root->removeRigidBody(tmp);
		delete tmp;
	}

	if(base) {
		root->removeCollisionBody(base);
		delete base;
	}

	delete root;
}

void PhysicsObject::setEntityVelocity(Ogre::Entity *entity, const Ogre::Vector3 &velocity, const bool &gravity) {
	RigidBodyList::const_iterator it = bodies.find(entity->getName());
	if (it == bodies.end()) {
		throw std::runtime_error(entity->getName() + " does not exist in the physics simulation");
	}

	btRigidBody *body = it->second;

	if(!gravity) {
		body->setGravity(btVector3(0, 0, 0));
	}

	body->setLinearVelocity(OgreUtils::toBulletVector(velocity));
}

void PhysicsObject::setEntityImpulse(Ogre::Entity *entity, const Ogre::Vector3 &impulse) {
	RigidBodyList::const_iterator it = bodies.find(entity->getName());
	if (it == bodies.end()) {
		throw std::runtime_error(entity->getName() + " does not exist in the physics simulation");
	}

	btRigidBody *body = it->second;

	body->applyCentralImpulse(OgreUtils::toBulletVector(impulse));
}

CollisionPair::List PhysicsObject::checkCollisions() const {
	const uint numManifolds = root->getDynamicsWorld()->getDispatcher()->getNumManifolds();
	CollisionPair::List list;

	for (uint i = 0; i < numManifolds; i++) {
		btPersistentManifold *contactManifold =  root->getDynamicsWorld()->getDispatcher()->getManifoldByIndexInternal(i);
		const btCollisionObject *obA = static_cast<const btCollisionObject*>(contactManifold->getBody0());
		const btCollisionObject *obB = static_cast<const btCollisionObject*>(contactManifold->getBody1());

		CustomData *cdA = static_cast<CustomData *>(obA->getUserPointer());
		CustomData *cdB = static_cast<CustomData *>(obB->getUserPointer());

		if(specialBodies.inMap(cdA->getName()) || specialBodies.inMap(cdB->getName())) {
			CollisionPair pair;
			pair.setA(cdA->getName());
			pair.setB(cdB->getName());
			list.insert(pair);
		}
	}

	checkBase();

	return list;
}

void PhysicsObject::performRaycast() const {
	// Get player
	btRigidBody *body = bodies.getValue("PLAYER_1");

	// Get origins and radius
	btVector3 absoluteOrigin = body->getWorldTransform().getOrigin();
	btVector3 relativeOrigin;
	btScalar radius;
	body->getCollisionShape()->getBoundingSphere(relativeOrigin, radius);

	// Coordinates for ray
	btVector3 startRay(absoluteOrigin.getX(), absoluteOrigin.getY(), absoluteOrigin.getZ());
	btVector3 endRay(absoluteOrigin.getX(), absoluteOrigin.getY() - radius, absoluteOrigin.getZ());
	btCollisionWorld::ClosestRayResultCallback res(startRay, endRay);

	// Do raytest
	root->getDynamicsWorld()->rayTest(startRay, endRay, res);

	// If hit, it's on the ground
	if(res.hasHit()){
		CustomData *cd = static_cast<CustomData *>(body->getUserPointer());
		cd->setAirborne(false);
	}
}

void PhysicsObject::updateBodies() const {
	btRigidBody *body;
	btTransform trans;
	btQuaternion orientation;

	for(RigidBodyList::const_iterator it = bodies.begin(); it != bodies.end(); it++){
		body = it->second;

		if(body && body->getMotionState()){
			body->getMotionState()->getWorldTransform(trans);
			orientation = trans.getRotation();

			CustomData *cd = static_cast<CustomData *>(body->getUserPointer());

			Ogre::SceneNode *sceneNode = cd->getSceneNode();
			sceneNode->setPosition(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ());
		}
	}
}

void PhysicsObject::setEntityPosition(btRigidBody *const body, const Ogre::Vector3 &position) {
	body->activate(true);

	btTransform trans;
	body->getMotionState()->getWorldTransform(trans);
	trans.setOrigin(OgreUtils::toBulletVector(position));

	body->setWorldTransform(trans);
}

void PhysicsObject::setEntityRotation(btRigidBody *const body, const Ogre::Quaternion &rotation) {
	body->activate(true);
	btTransform trans = body->getWorldTransform();
	trans.setRotation(OgreUtils::toBulletQuaternion(rotation));
	body->setWorldTransform(trans);
}

void PhysicsObject::checkBase() const
{
	for(int i = 0; i < base->getNumOverlappingObjects(); i++) {
		btCollisionObject *character = base->getOverlappingObject(i);
		CustomData *data = static_cast<CustomData *>(character->getUserPointer());
		callback->onBaseContact(data->getName());
	}
}

void PhysicsObject::registerStaticEntity(Ogre::Entity *entity, Ogre::SceneNode *node) {
	registerEntity(entity, node, 0);
}

btRigidBody *PhysicsObject::registerEntity(Ogre::Entity *entity, Ogre::SceneNode *node, const btScalar &mass, bool special) {

	// Get name, orientation and position from entity and node
	const std::string name = entity->getName();
	Ogre::Quaternion orientation = node->getOrientation();
	Ogre::Vector3 position = node->getPosition();

	// Create shape from entity
	OgreCollisionShape ogreShape(entity->getMesh());
	btCollisionShape *shape = ogreShape.convertDynamic();
	shape->setLocalScaling(OgreUtils::toBulletVector(node->getScale()));

	// Motionstate for the body
	btDefaultMotionState *motionState = new btDefaultMotionState(btTransform(OgreUtils::toBulletQuaternion(orientation),
										 OgreUtils::toBulletVector(position)));
	// Calculate inertia for body
	btVector3 fallInertia(0, 0, 0);
	shape->calculateLocalInertia(mass, fallInertia);

	// Create body
	btRigidBody *body = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(mass,
										     motionState,
										     shape,
										     fallInertia));
	body->setFriction(1);

	// They say this will remove the "bouncyness"
	body->setRestitution(0);

	// Set custom data to body
	body->setUserPointer(new CustomData(node, name));

	// Add body to world and our vector
	root->addRigidBody(body);
	bodies[name] = body;

	if(special) {
		// Add btRigidBody to vector
		specialBodies[name] = body;
	}

	return body;
}

void PhysicsObject::removeEntity(Ogre::Entity *entity) {
	removeEntity(entity->getName());
}

void PhysicsObject::removeEntity(const std::string name) {
#ifdef _DEBUG
	// Absolutely not OK to remove entities during simulation
	assert(!isSimulating);
#endif

	btRigidBody *tmp = bodies.getValue(name);
	bodies.erase(name);

	// Remove from special bodies, if it exists
	RigidBodyList::iterator specialIt = specialBodies.find(name);
	if(specialIt != specialBodies.end()){
		specialBodies.erase(specialIt);
	}

	// Compiler whines when deleting a void pointer
	CustomData *cd = static_cast<CustomData *>(tmp->getUserPointer());
	delete cd;

	delete tmp->getMotionState();
	root->removeRigidBody(tmp);
	delete tmp;
}

void PhysicsObject::moveEntity(Ogre::MovableObject *entity, const Ogre::Vector3 &transVec) {
	btRigidBody *body = bodies.getValue(entity->getName());
	CustomData *cd = static_cast<CustomData *>(body->getUserPointer());
	btScalar y = body->getLinearVelocity().y();
	btVector3 vec = OgreUtils::toBulletVector(transVec);
	vec.setY(y);

	body->setLinearVelocity(vec);

	!vec.x() && !vec.z() ? cd->setMoving(false) : cd->setMoving(true);

	/*body->activate(true);
	if(!cd->isAirborne()){
		body->setLinearVelocity(OgreUtils::toBulletVector(transVec));
	}*/
}

void PhysicsObject::jumpEntity(Ogre::MovableObject *entity) {
	btRigidBody *body = bodies.getValue(entity->getName());
	CustomData *cd = static_cast<CustomData *>(body->getUserPointer());

	if(!cd->isAirborne()) {
		body->applyCentralImpulse(btVector3(0, 200, 0));
		cd->setAirborne(true);
	}
}

void PhysicsObject::setEntityPosition(Ogre::MovableObject *entity, const Ogre::Vector3 &position) {
	btRigidBody *body = bodies.getValue(entity->getName());
	setEntityPosition(body, position);
}

void PhysicsObject::setEntityRotation(Ogre::MovableObject *entity, const Ogre::Quaternion &rotation) {
	btRigidBody *body = bodies.getValue(entity->getName());
	setEntityRotation(body, rotation);
}

void PhysicsObject::setAsCharacter(const std::string &name) const {
	btRigidBody *body = bodies.getValue(name);

#ifdef _DEBUG
	assert(body);
#endif

	body->setAngularFactor(btVector3(0, 0, 0));
}

bool PhysicsObject::raycast(const Ogre::Vector3 &start, const Ogre::Vector3 &end) const {
	return true;
	btVector3 bStart = OgreUtils::toBulletVector(start);
	btVector3 bEnd = OgreUtils::toBulletVector(end);

	btVector3 direction = bEnd - bStart;
	direction.normalize();

	btCollisionWorld::ClosestRayResultCallback res(bStart + 1.4*direction, bEnd - 1.4*direction);

	// Do raytest
	root->getDynamicsWorld()->rayTest(bStart + 1.4*direction, bEnd - 1.4*direction, res);
	/*std::cout<<"Start "<<bStart.x()<<" "<<bStart.y()<<" "<<bStart.z()<<std::endl;
	std::cout<<"End"<<bEnd.x()<<" "<<bEnd.y()<<" "<<bEnd.z()<<std::endl;
	std::cout <<"Hitt "<< res.m_hitPointWorld.x() << " " << res.m_hitPointWorld.y() << " " << res.m_hitPointWorld.z() << std::endl;*/

	return res.hasHit();
}

void PhysicsObject::setCollision(const std::string &name) {
	btRigidBody *body = bodies.getValue(name);
	body->setCollisionFlags(body->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
	specialBodies[name] = body;
}

CustomData* PhysicsObject::getCustomData(const std::string &name) const {
	btRigidBody *body = bodies.getValue(name);
	CustomData *cd = static_cast<CustomData *>(body->getUserPointer());

	return cd;
}

void PhysicsObject::simulate() {
#ifdef _DEBUG
	isSimulating = true;

	if(root->getDynamicsWorld()->getDebugDrawer()) {
		root->getDynamicsWorld()->debugDrawWorld();
	}
#endif

	// Step simulation
	root->getDynamicsWorld()->stepSimulation(simulationTimeStep);

	// Loop through all collisions
	CollisionPair::List collisionList = checkCollisions();

	// Raycast for jumping purposes
	performRaycast();

	// Translate changes
	updateBodies();

#ifdef _DEBUG
	isSimulating = false;
#endif

	callback->onContact(collisionList);
}

void PhysicsObject::registerBase(Ogre::SceneNode *node)
{
	base = new Base(node);

	CustomData *data = new CustomData(node, "Base");
	base->setUserPointer(data);

	root->addCollisionBody(base);
}

}
