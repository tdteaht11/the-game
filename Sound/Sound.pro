TEMPLATE = lib
CONFIG -= app_bundle
CONFIG -= qt

!win32: debug: DEFINES += _DEBUG

macx: CONFIG += lib

TARGET = sound
DESTDIR = $${PWD}/../lib

SOURCES += \
    SoundObject.cpp
HEADERS += \
    Sound.h \
    PreProcessor.h \
    SoundObject.h \
    SoundPath.h \
    SoundPath.h \
    SoundPath.h

OTHER_FILES +=

include(../SFML.pri)
include(../Ogre.pri)
