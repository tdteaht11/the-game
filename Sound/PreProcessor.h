#ifndef SOUND_PREPROCESSOR_H
#define SOUND_PREPROCESSOR_H

// stl
#include <stdexcept>
#include <vector>
#include <string.h>
#include <list>
#include <map>
#include <iostream>
#include <fstream>

#pragma GCC diagnostic ignored "-Wall"
#include <SFML/Audio.hpp>
#include <Ogre.h>
#pragma GCC diagnostic pop

typedef short int sint;
typedef unsigned short int usint;
typedef unsigned int uint;
typedef long int lint;
typedef unsigned long int ulint;
typedef unsigned char uchar;

#endif // SOUND_PREPROCESSOR_H
