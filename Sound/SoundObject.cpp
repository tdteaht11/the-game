#include "SoundObject.h"
#include "SoundPath.h"

namespace Sound {

SoundObject::SoundObject() {
	populateSounds();
}

SoundObject::~SoundObject() {
	clearSounds();
}

void SoundObject::addCharacter(const std::string &name) {
	footsteps[name] = createSound(SOUND_FOOTSTEPS, true);
}

void SoundObject::removeCharacter(const std::string &name) {
	sf::Sound *tmp = getCharacterFootsteps(name);
	footsteps.erase(name);
	delete tmp;
}

void SoundObject::playSound(const SoundID &id, const Ogre::Vector3 &position) {
	sf::Sound *sound = createSound(id);
	sound->setPosition(position.x, position.y, position.z);
	sound->play();

	playingSounds.push_back(sound);
}

void SoundObject::playFootsteps(const std::string &name, const Ogre::Vector3 &position) {
	sf::Sound *sound = getCharacterFootsteps(name);
	sound->setPosition(position.x, position.y, position.z);

	if(position == Ogre::Vector3::ZERO) {
		sound->setRelativeToListener(true);
	}

	if(sound->getStatus() != sf::Sound::Playing) sound->play();
}

void SoundObject::stopFootsteps(const std::string &name) {
	sf::Sound *sound = getCharacterFootsteps(name);
	sound->stop();
}

void SoundObject::setListener(const Ogre::Vector3 &position, const Ogre::Vector3 &orientation) const {
	sf::Listener::setPosition(position.x, position.y, position.z);
	sf::Listener::setDirection(orientation.x, orientation.y, orientation.z);
}

void SoundObject::garbageCollector() {
	soundVector::iterator it;
	sf::Sound *tmp;

	for(it = playingSounds.begin(); it != playingSounds.end(); ) {
		tmp = *it;
		if(tmp->getStatus() == sf::Sound::Stopped) {
			delete tmp;
			playingSounds.erase(it);
		}else{
			it++;
		}
	}
}

sf::Sound* SoundObject::createSound(const SoundID &id, const bool &loop, const float &volume) const {
	sf::Sound *sound = new sf::Sound(sounds[id]);
	sound->setLoop(loop);
	sound->setVolume(volume);

	return sound;
}

void SoundObject::populateSounds() {
	std::string filenames[] = {
		"die.ogg", "footsteps.ogg", "swoosch.ogg", "sword_high.ogg", "sword_low.ogg"
	};

	for(int i = 0; i < SOUND_COUNT; i++) {
		if(!sounds[i].loadFromFile(SOUNDPATH "/" + filenames[i])){
			throw std::runtime_error("Could not load file: " SOUNDPATH "/" + filenames[i]);
		}
	}
}

sf::Sound* SoundObject::getCharacterFootsteps(const std::string &name) const {
	soundList::const_iterator it = footsteps.find(name);

	if(it != footsteps.end()){
		return it->second;
	}
}

void SoundObject::clearSounds() {
	soundVector::iterator it;
	for(it = playingSounds.begin(); it != playingSounds.end(); it++) {
		delete *it;
	}

	soundList::iterator lit;
	for(lit = footsteps.begin(); lit != footsteps.end(); lit++) {
		delete lit->second;
	}
}

}
