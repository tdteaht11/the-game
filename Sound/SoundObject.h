#ifndef SOUNDOBJECT_H
#define SOUNDOBJECT_H

#include "PreProcessor.h"

namespace Sound {

enum SoundID {
	SOUND_DIE,
	SOUND_FOOTSTEPS,
	SOUND_SWOOSCH,
	SOUND_SWORD_HIGH,
	SOUND_SWORD_LOW,
	SOUND_COUNT
};

class SoundObject
{
public:
	SoundObject();
	virtual ~SoundObject();

	/**
	 * @brief Adds a character to SoundObject for use with looping sounds
	 * @param name std::string name of the character
	 */
	void addCharacter(const std::string &name);

	/**
	 * @brief Removes a character from SoundObject
	 * @param name std::string name of the character
	 */
	void removeCharacter(const std::string &name);

	/**
	 * @brief Play wanted sound on a specific position
	 * @param id SoundID of the sound
	 * @param position sf::Vector3f with the position of the sound
	 */
	void playSound(const SoundID &id, const Ogre::Vector3 &position);

	/**
	 * @brief Play footsteps
	 * @param name std::string name of the character
	 * @param position sf::Vector3f with the position of the sound
	 */
	void playFootsteps(const std::string &name, const Ogre::Vector3 &position = Ogre::Vector3::ZERO);

	/**
	 * @brief Stop playing footsteps
	 * @param name std::string name of the character
	 */
	void stopFootsteps(const std::string &name);

	/**
	 * @brief Set position and orientation of the listener (Player)
	 * @param position Ogre::Vector3
	 * @param orientation Ogre::Vector3
	 */
	void setListener(const Ogre::Vector3 &position, const Ogre::Vector3 &orientation) const;

	/**
	 * @brief Remove finished sound. Used by garbage collectors
	 */
	void garbageCollector();
private:
	typedef std::map<std::string, sf::Sound *> soundList;
	typedef std::vector<sf::Sound *> soundVector;

	/**
	 * @brief Get wanted sound to play
	 * @param id SoundID of the sound
	 * @param loop bool if the sound should loop or not. False by default
	 * @param volume float the volume of the sound [0-100]. 100 by default
	 * @return sf::Sound object
	 */
	sf::Sound *createSound(const SoundID &id, const bool &loop = false, const float &volume = 100) const;

	/**
	 * @brief Populates the soundbuffer array with all the sounds
	 */
	void populateSounds();

	/**
	 * @brief Get footstep sound of character
	 * @param name std::string name of character
	 * @return sf::Sound object
	 */
	sf::Sound *getCharacterFootsteps(const std::string &name) const;

	/**
	 * @brief Clear sounds
	 */
	void clearSounds();

	sf::SoundBuffer sounds[SOUND_COUNT];
	soundList footsteps;
	soundVector playingSounds;
};

}

#endif // SOUNDOBJECT_H
