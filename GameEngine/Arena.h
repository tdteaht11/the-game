#ifndef ARENA_H
#define ARENA_H

#include "PreProcessor.h"
#include "Player.h"

/**
 * @brief Contains the arena map, enemies, obstacle, etc.
 */
class Arena :
		private OgreDotSceneParser::ParseListener,
		public Physics::PhysicsCallback,
		public AIS::AIMessenger {
public:
	Arena(Ogre::SceneManager *scene);
	Arena(const Arena &arena);
	virtual ~Arena();

	void init();

	void updateCharacterAnimations(const int &interval);

	inline Player *getPlayer() const {
		return player;
	}

	inline Ogre::SceneManager* getSceneManager() const {
		return scene;
	}

	void spawnEnemies();
	void loadMap();

	Ogre::SceneNode *getBase() const;
private:
	typedef std::map<std::string, AnimatedCharacter *> CharacterList;

	Ogre::SceneManager *scene;
	Ogre::SceneNode *rootNode;

	Player *player;

	CharacterList characters;
	CharacterList enemies;

	Ogre::SceneNode *base;
	Ogre::SceneNode *spawn;

	void createPlayer();
	void removeCharacter(Character *c);
	Character *getCharacter(const std::string& name);

	static const uchar maxEnemies;

	// ParseListener interface
public:
	virtual void onEntity(Ogre::Entity *entity, Ogre::SceneNode *node, const OgreDotSceneParser::Entity &dotEntity);
	virtual void onStaticEntity(Ogre::Entity *entity, Ogre::SceneNode *node);
	virtual void onNavmesh(Ogre::Entity *entity, Ogre::SceneNode *node);
	virtual void onNode(Ogre::SceneNode *node);

	// PhysicsCallback interface
public:
	virtual void onContact(const Physics::CollisionPair::List &collisionList);
	virtual void onBaseContact(const std::string &name);

	// AIMessenger interface
public:
	virtual void requestMove(const std::string &id);
	virtual void requestAttack(const std::string &id);
	virtual void requestRangedAttack(const std::string &id, const Ogre::Radian &angle);
	virtual void requestBlock(const std::string &id);
	virtual Ogre::Vector3 requestLocation(const std::string &id);
	virtual Ogre::Vector3 requestPlayerLocation();
	virtual int requestActiveToolInfo(const std::string &id);
	virtual bool requestLineOfSightCheck(const Ogre::Vector3 &origin, const Ogre::Vector3 &target);
	virtual void requestTurn(const std::string &id, const Ogre::Radian &angle);
	virtual void requestTurn(const std::string &id, const Ogre::Vector3 &pos);
	//virtual void requestStop(const std::string &id);
	void setBase(Ogre::SceneNode *value);
};

#endif // ARENA_H
