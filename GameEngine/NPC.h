#ifndef NPC_H
#define NPC_H

#include "PreProcessor.h"
#include "AnimatedCharacter.h"

/**
 * @brief Base class of Non Player Characters (NPC)
 */
class NPC : public AnimatedCharacter {
public:
	NPC(const Ogre::String &mesh);
	NPC(const NPC &obj);
	virtual ~NPC();
};

#endif // NPC_H
