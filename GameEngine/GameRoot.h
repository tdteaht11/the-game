#ifndef GAMEROOT_H
#define GAMEROOT_H

#include "PreProcessor.h"
#include "WindowEventListener.h"
#include "Arena.h"

/**
 * @brief The GameRoot class
 */
class GameRoot : public QtOgre::WindowEventListener {
public:
	/**
	 * @brief getInstance
	 * @return GameRoot Reference to the root of the games
	 */
	static GameRoot& getInstance();
	virtual ~GameRoot();

	/**
	 * @brief getRenderWindow
	 * @return Ogre::RenderWindow* Pointer to the render window
	 */
	inline Ogre::RenderWindow* getRenderWindow() const {
		return ogreWindow;
	}

	/**
	 * @brief Get the physics object
	 * @return Pointer to the physics object
	 */
	inline Physics::PhysicsObject* getPhysics() const {
		return physics;
	}

	/**
	 * @brief Get the sound object
	 * @return Pointer to the sound object
	 */
	inline Sound::SoundObject* getSound() const {
		return sound;
	}

	/**
	 * @brief Get the scene manager
	 * @return Pointer to the scene manager
	 */
	inline Ogre::SceneManager *getSceneManager() const {
		return scene;
	}

	inline AIS::AIObject *getAIObject() const {
		return ai;
	}

private:
	GameRoot();

	// Disable copy
	GameRoot(const GameRoot &gameRoot) :
		WindowEventListener(gameRoot),
		root(0),
		scene(0),
		gcThread(&GameRoot::garbageCollectorFunc, this){
		throw std::runtime_error("Not allowed to copy GameRoot objects");
	}

	/**
	 * @brief setRenderSystem
	 */
	void setRenderSystem() const;

	/**
	 * @brief loadResources
	 */
	void loadResources();

	/**
	 * @brief handleInput
	 */
	void handleInput();

	/**
	 * @brief Garbage collector
	 */
	void garbageCollector();

	/**
	 * @brief garbageCollectorFunc
	 */
	void garbageCollectorFunc();

	Ogre::Root *const root;
	Ogre::SceneManager *const scene;
	Ogre::RenderWindow *ogreWindow;
	Arena *arena;
	Physics::PhysicsObject *physics;
	AIS::AIObject *ai;
	Sound::SoundObject *sound;
	sf::Thread gcThread;

#if OGRE_DEBUG_MODE == 1
	Physics::OgreDebugDrawer *debugDrawer;
#endif

	// WindowEventListener functions
public:
	virtual void onGraphics();
	virtual void onAction(const int &interval);
	virtual void onKeyPressed(const uchar &key);
	virtual void onKeyReleased(const uchar &key);
	virtual void onMouseMoved(const sint &x, const sint &y);
	virtual void onSetup(Ogre::RenderWindow* renderSystem);

private:
	std::bitset<QtOgre::WindowEventListener::KEY_COUNT> currentKeys;
	std::bitset<QtOgre::WindowEventListener::KEY_COUNT> previousKeys;
};

#endif // GAMEROOT_H
