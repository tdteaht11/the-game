#ifndef MOVABLECONTROLLER_H
#define MOVABLECONTROLLER_H

#include "PreProcessor.h"

/**
 * @brief Base class for objects that should be movable
 */
class MovableObject {
public:
	MovableObject(Ogre::SceneNode *root, Ogre::Entity *entity, Ogre::Real mass = 70);
	MovableObject(const MovableObject &obj);
	virtual ~MovableObject();

	/**
	 * @brief Move the object, relative to its current position
	 * @param Position to move to
	 */
	virtual void setVelocity(const Ogre::Vector3 &position);

	/**
	 * @brief Make a movable object stop
	 */
	virtual void stopVelocity();

	/**
	 * @brief Move the object to the given absolute coordinates
	 * @param Position to set object at
	 */
	virtual void setPosition(const Ogre::Vector3 &position);

	/**
	 * @brief Rotate the object
	 * @param Amount of radians to rotate
	 */
	virtual void rotate(const Ogre::Radian &radian);

	/**
	 * @brief Rotate the object looking at a specific point
	 * @param The coordinates to look at
	 */
	void rotate(const Ogre::Vector3 &lookAt);

	/**
	 * @brief Move the object forwards
	 */
	virtual void moveForward();

	/**
	 * @brief Move the object backwards
	 */
	virtual void moveBackwards();

	/**
	 * @brief Get the root node of the object
	 * @return Pointer to the root node of the object
	 */
	inline Ogre::SceneNode* getRootNode() const {
		return root;
	}

	/**
	 * @brief Get the mesh model of the object
	 * @return Pointer to the object mesh
	 */
	inline Ogre::Entity* getEntity() const {
		return entity;
	}

	/**
	 * @brief Get the node that is changed when yawing (moving, etc.). If setYawNode() has not been called, this will be the root
	 * @return Node that is changed when yawing
	 */
	inline Ogre::SceneNode* getYawNode() const {
		return yawNode;
	}

	/**
	 * @brief How many meters per second the object can travel
	 * @return  Meters per second the object cal travel
	 */
	virtual usint getMovementSpeed() const = 0;

protected:
	/**
	 * @brief Set the node that should be handled when yawing (moving, etc). Default is root node.
	 * @param The node to yaw at
	 */
	inline void setYawNode(Ogre::SceneNode *yawNode) {
		this->yawNode = yawNode;
	}

private:
	Ogre::SceneNode *root;
	Ogre::SceneNode *yawNode;
	Ogre::Entity *entity;

	void init(Ogre::Real mass = 70);
};

#endif // MOVABLECONTROLLER_H
