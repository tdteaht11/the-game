#ifndef ENEMYSINBAD_H
#define ENEMYSINBAD_H

#include "PreProcessor.h"
#include "Enemy.h"

class EnemySinbad : public Enemy {
public:
	EnemySinbad();
	virtual ~EnemySinbad();
};

#endif // ENEMYSINBAD_H
