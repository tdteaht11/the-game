#include "Enemy.h"

uchar Enemy::nEnemies = 0;

Enemy::Enemy(const Ogre::String &mesh) :
	NPC(mesh)
{
	nEnemies++;
}

Enemy::Enemy(const Enemy &enemy) :
	NPC(enemy)
{

}

Enemy::~Enemy()
{
	nEnemies--;
}

uchar Enemy::getAmountEnemies()
{
	return nEnemies;
}

