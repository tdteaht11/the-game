TEMPLATE = app

SOURCES += main.cpp \
    GameRoot.cpp \
    Arena.cpp \
    Player.cpp \
    CharacterUtils.cpp \
    NPC.cpp \
    Tools/Weapon.cpp \
    Tools/Tool.cpp \
    Tools/MeleeWeapon.cpp \
    Tools/RangedWeapon.cpp \
    Tools/Spell.cpp \
    Tools/OffensiveSpell.cpp \
    Tools/DefensiveSpell.cpp \
    Tools/Bow.cpp \
    Tools/Arrow.cpp \
    Tools/Projectile.cpp \
    MoveableObject.cpp \
    Character.cpp \
    Enemy.cpp \
    Interfaces/Hp.cpp \
    AnimatedCharacter.cpp \
    Sinbad.cpp \
    EnemySinbad.cpp

HEADERS += \
    GameRoot.h \
    PreProcessor.h \
    Arena.h \
    Player.h \
    CharacterUtils.h \
    NPC.h \
    Tools/Weapon.h \
    Tools/Tool.h \
    Tools/MeleeWeapon.h \
    Tools/RangedWeapon.h \
    Tools/Spell.h \
    Tools/OffensiveSpell.h \
    Tools/DefensiveSpell.h \
    Tools/Bow.h \
    Tools/Arrow.h \
    Tools/Projectile.h \
    Tools/ProjectileFactory.h \
    Interfaces/Damage.h \
    MovableObject.h \
    Character.h \
    Utils.h \
    Enemy.h \
    Interfaces/Hp.h \
    Exceptions/InvalidHp.h \
    MathUtils.h \
    AnimatedCharacter.h \
    Sinbad.h \
    EnemySinbad.h

OTHER_FILES = ogre/*
DEFINES += DEBUG _DEBUG

# Custom libs
LIBS += -L$${PWD}/../lib

# Physics
INCLUDEPATH += $${PWD}/../Physics
DEPENDPATH += $${PWD}/../Physics
LIBS += -lphysics

# SFML Ogre
INCLUDEPATH += $${PWD}/../QtOgre
DEPENDPATH += $${PWD}/../QtOgre
LIBS += -lqtogre

# Ogre DotSceneParser
INCLUDEPATH += $${PWD}/../OgreDotSceneParser
DEPENDPATH += $${PWD}/../OgreDotSceneParser
LIBS += -logredotsceneparser

# AI
INCLUDEPATH += $${PWD}/../AI
DEPENDPATH += $${PWD}/../AI
LIBS += -lai

# Sound
INCLUDEPATH += $${PWD}/../Sound
DEPENDPATH += $${PWD}/../Sound
LIBS += -lsound


include(../Ogre.pri)
include(../Bullet.pri)
include(../Qt.pri)
include(../TinyXML.pri)
include(../SFML.pri)

unix:!macx: LIBS += -lboost_system -lzzip
