#ifndef INVALIDHP_H
#define INVALIDHP_H

#include "PreProcessor.h"

namespace Exceptions {

class InvalidHp : public std::exception {
public:
	enum Code {
		HP_DEAD,
		HP_FULL
	};

	inline InvalidHp(const Code &code) :
		code(code) {

	}

	inline InvalidHp(const InvalidHp &hp) :
		code(hp.code){

	}

	inline virtual const char *what() {
		return "HP is invalid";
	}

	inline Code getCode() const {
		return code;
	}

private:
	const Code  code;
};

}

#endif // INVALIDHP_H
