#ifndef CHARACTERCONTROLLER_H
#define CHARACTERCONTROLLER_H

#include "PreProcessor.h"
#include "MovableObject.h"
#include "Tools/Tool.h"
#include "Interfaces/Hp.h"

/**
 * @brief Abstract class that handles a character
 */
class Character : public MovableObject, public Interfaces::Hp {
public:
	enum Diagonal {
		DIAGONAL_FORWARD_LEFT,
		DIAGONAL_FORWARD_RIGHT,
		DIAGONAL_BACKWARD_LEFT,
		DIAGONAL_BACKWARD_RIGHT
	};

	Character(Ogre::Entity *entity);
	Character(const Character &obj);
	virtual ~Character();

	/**
	 * @brief Strafe left with the character
	 */
	virtual void strafeLeft();

	/**
	 * @brief Strafe right with the character
	 */
	virtual void strafeRight();

	virtual void moveDiagonal(const Diagonal &diagonal);

	/**
	 * @brief Move the object, relative to its current position
	 * @param Position to move to
	 */
	virtual void setVelocity(const Ogre::Vector3 &position);

	/**
	 * @brief Make a movable object stop
	 */
	virtual void stopVelocity();

	/**
	 * @brief Make the character jump
	 */
	virtual void jump();

	/**
	 * @brief Make the character attack
	 */
	virtual void attack();

	Tool *getEquippedTool() const { return equippedTool; }

	/**
	 * @brief Equip a tool
	 * @param Tool to add
	 */
	void equipTool(Tool *const tool);

	/**
	 * @brief Equip a tool
	 * @param Iterator pointing to the tool
	 */
	void equipTool(const Tool::List::const_iterator &it);

	/**
	 * @brief Equip the first tool available
	 */
	void equipFirstTool();

	/**
	 * @brief Add a tool to the character
	 * @param Tool to add
	 */
	void addTool(const Tool &tool);

	/**
	 * @brief Remove a specified tool
	 * @param Tool to remove
	 */
	void removeTool(Tool *const tool);

	/**
	 * @brief Reset tools clearing everything
	 */
	void resetTools();

	/**
	 * @brief Get the node that the character should pitch
	 * @return A pointer to the Ogre scene node to pitch for this character
	 */
	inline Ogre::SceneNode *getPitchNode() const {
		return pitchNode;
	}

	virtual inline usint getMovementSpeed() const {
		return 10;
	}

	virtual inline usint getMaxHp() const {
		return 100;
	}

protected:

	/**
	 * @brief Set the node to handle pitching on the character
	 * @param The Ogre scene node to pitch
	 */
	inline void setPitchNode(Ogre::SceneNode *pitchNode) {
		this->pitchNode = pitchNode;
	}

private:
	Tool::List tools;
	Tool *equippedTool;
	Ogre::SceneNode *pitchNode;

	void init();
};

#endif // CHARACTERCONTROLLER_H
