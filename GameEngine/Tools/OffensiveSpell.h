#ifndef OFFENSIVESPELL_H
#define OFFENSIVESPELL_H

#include "PreProcessor.h"
#include "Spell.h"

/**
 * @brief The OffensiveSpell class handles spells that make damage
 */
class OffensiveSpell : public Spell {
public:
	OffensiveSpell(const Character *const weilder);
	OffensiveSpell(const OffensiveSpell &spell);
	virtual ~OffensiveSpell();
};

#endif // OFFENSIVESPELL_H
