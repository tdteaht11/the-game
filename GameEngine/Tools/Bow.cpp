#include "Bow.h"
#include "ProjectileFactory.h"

Bow::Bow(const Character *const weilder) :
	RangedWeapon(weilder)
{
}

Bow::Bow(const Bow &bow) :
	RangedWeapon(bow)
{

}

Bow::~Bow()
{

}

Bow *Bow::clone() const
{
	return new Bow(*this);
}

std::string Bow::getName() const
{
	return "Bow";
}

ProjectileFactory::Projectiles Bow::getProjectile() const
{
	return ProjectileFactory::PROJECTILE_ARROW;
}
