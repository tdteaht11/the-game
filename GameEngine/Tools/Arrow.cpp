#include "Arrow.h"

Arrow::Arrow(const Ogre::Vector3 &pos, const Character *const shooter) :
	Projectile("arrow.mesh", pos, shooter)
{
}

Arrow::Arrow(const Arrow &arrow) :
	Projectile(arrow)
{

}

Arrow::~Arrow()
{

}
