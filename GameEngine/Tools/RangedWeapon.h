#ifndef RANGEDWEAPON_H
#define RANGEDWEAPON_H

#include "PreProcessor.h"
#include "Weapon.h"
#include "Projectile.h"
#include "ProjectileFactory.h"

/**
 * @brief The RangedWeapon class is the base class for ranged weapons
 */
class RangedWeapon : public Weapon {
public:
	RangedWeapon(const Character *const weilder);
	RangedWeapon(const RangedWeapon &weapon);
	virtual ~RangedWeapon();

	/**
	 * @brief Alias of use()
	 */
	inline void shoot() {
		use();
	}

	/**
	 * @brief An enum representing what kind of projectile class this ranged weapon shoots
	 * @return A value from the projectiles enum
	 */
	virtual ProjectileFactory::Projectiles getProjectile() const = 0;

private:

	// Tool interface
public:
	virtual void use();
};

#endif // RANGEDWEAPON_H
