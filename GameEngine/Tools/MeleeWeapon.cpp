#include "MeleeWeapon.h"

MeleeWeapon::MeleeWeapon(const Character *const weilder) :
	Weapon(weilder)
{
}

MeleeWeapon::MeleeWeapon(const MeleeWeapon &weapon) :
	Weapon(weapon)
{

}

MeleeWeapon::~MeleeWeapon()
{

}
