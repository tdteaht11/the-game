#include "RangedWeapon.h"
#include "GameRoot.h"
#include "CharacterUtils.h"

RangedWeapon::RangedWeapon(const Character *const weilder) :
	Weapon(weilder)
{
}

RangedWeapon::RangedWeapon(const RangedWeapon &weapon) :
	Weapon(weapon)
{

}

RangedWeapon::~RangedWeapon()
{
}

void RangedWeapon::use()
{	// Get the front of the character
	const Ogre::Radian yaw = getWeilder()->getYawNode()->getOrientation().getYaw();
	Ogre::Vector3 characterForward = CharacterUtils::calculateMovement(yaw);
	characterForward.normalise();

	// Calculate how much of the bounding box dimensions should be used
	Ogre::AxisAlignedBox box = getWeilder()->getEntity()->getBoundingBox();
	Ogre::Vector3 weilderSize = box.getSize();
	weilderSize.x *= characterForward.x;
	weilderSize.z *= characterForward.z;

	// Calculate where the projectile should spawn
	const Ogre::Vector3 weilderPosition = getWeilder()->getRootNode()->getPosition();

	Ogre::Vector3 projectilePos = weilderPosition + weilderSize;
	// Create the projectile
	Projectile *p = ProjectileFactory::getInstance().create(getProjectile(), projectilePos, getWeilder());

	Ogre::SceneNode *const node = p->getRootNode();

	// Set entity properly in physics
	PHYSICS->setEntityPosition(p->getEntity(), node->getPosition());
	PHYSICS->setEntityRotation(p->getEntity(), node->getOrientation());

	// Register projectile at physics library
	const Ogre::Radian pitch = getWeilder()->getPitchNode()->getOrientation().getPitch();

	const double height = CharacterUtils::calculatePitch(pitch);
	Ogre::Vector3 characterLookForward = CharacterUtils::calculateMovement(yaw);
	characterLookForward.y += height;
	characterLookForward.normalise();

	const Ogre::Vector3 impulse = characterLookForward * p->getMovementSpeed();
	PHYSICS->setEntityImpulse(p->getEntity(), impulse);
}
