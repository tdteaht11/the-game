#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "PreProcessor.h"
#include "MovableObject.h"
#include "Character.h"
#include "Interfaces/Damage.h"

/**
 * @brief The Projectile class handles objects that should be spawned from weapons and spells
 */
class Projectile :  public MovableObject, public Interfaces::Damage {
public:
	Projectile(const std::string &meshName, const Ogre::Vector3 &pos, const Character *const shooter);
	Projectile(const Projectile &projectile);
	virtual ~Projectile();

	/**
	 * @brief Wether the projectile be drawn down by gravity
	 * @return true if reacting to gravity
	 */
	virtual bool reactToGravity() const;

	/**
	 * @brief Get who shot the projectile
	 * @return The character that shot the projectile
	 */
	const Character *getShooter() const;

private:
	const Character *const shooter;

	void init();

// Interface for damage
public:
	virtual inline usint getDamage() const {
		return 20;
	}
};

#endif // AMMO_H
