#ifndef ARROW_H
#define ARROW_H

#include "PreProcessor.h"
#include "Projectile.h"
#include "Character.h"

/**
 * @brief Shoot arrows from for instance a bow
 */
class Arrow : public Projectile {
public:
	Arrow(const Ogre::Vector3 &pos, const Character *const shooter);
	Arrow(const Arrow &arrow);
	virtual ~Arrow();

	virtual inline usint getMovementSpeed() const {
		return 500;
	}
};

#endif // ARROW_H
