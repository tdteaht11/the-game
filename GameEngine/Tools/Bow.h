#ifndef BOW_H
#define BOW_H

#include "PreProcessor.h"
#include "RangedWeapon.h"
#include "AnimatedCharacter.h"
#include "Arrow.h"

/**
 * @brief The Bow class
 */
class Bow : public RangedWeapon {
public:
	Bow(const Character *const weilder);
	Bow(const Bow &bow);
	virtual ~Bow();
	virtual Bow *clone() const;

	// Tool interface
public:
	virtual std::string getName() const;
	virtual inline uchar getAnimationState() const {
		return (uchar) ANIM_SLICE_VERTICAL;
	}

	virtual inline Sound::SoundID getSound() const {
		return Sound::SOUND_SWOOSCH;
	}

	// RangedWeapon interface
public:
	virtual ProjectileFactory::Projectiles getProjectile() const;
};

#endif // BOW_H
