#include "DefensiveSpell.h"

DefensiveSpell::DefensiveSpell(const Character *const weilder) :
	Spell(weilder)
{
}

DefensiveSpell::DefensiveSpell(const DefensiveSpell &spell) :
	Spell(spell)
{

}

DefensiveSpell::~DefensiveSpell()
{

}
