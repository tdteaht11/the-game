#ifndef DEFENSIVESPELL_H
#define DEFENSIVESPELL_H

#include "PreProcessor.h"
#include "Spell.h"

/**
 * @brief The DefensiveSpell class handles spells that are non-damaging
 */
class DefensiveSpell : public Spell {
public:
	DefensiveSpell(const Character *const weilder);
	DefensiveSpell(const DefensiveSpell &spell);
	virtual ~DefensiveSpell();
};

#endif // DEFENSIVESPELL_H
