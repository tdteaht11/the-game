#include "Tool.h"

Tool::Tool(const Character *const weilder) :
	weilder(weilder)
{
}

Tool::Tool(const Tool &tool) :
	weilder(tool.weilder)
{

}

Tool::~Tool()
{

}

bool Tool::operator==(const Tool &rhs) const {
	return getName() == rhs.getName();
}

bool Tool::operator<(const Tool &rhs) const {
	return getName() < rhs.getName();
}

bool Tool::operator>(const Tool &rhs) const {
	return getName() > rhs.getName();
}
const Character *Tool::getWeilder() const
{
	return weilder;
}

