#ifndef MELEEWEAPON_H
#define MELEEWEAPON_H

#include "PreProcessor.h"
#include "Weapon.h"

/**
 * @brief The MeleeWeapon class
 */
class MeleeWeapon : public Weapon {
public:
	MeleeWeapon(const Character *const weilder);
	MeleeWeapon(const MeleeWeapon &weapon);
	virtual ~MeleeWeapon();
};

#endif // MELEEWEAPON_H
