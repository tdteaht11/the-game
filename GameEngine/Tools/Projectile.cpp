#include "Projectile.h"
#include "GameRoot.h"

Projectile::Projectile(const std::string &meshName, const Ogre::Vector3 &pos, const Character *const shooter) :
	MovableObject(
		SCENE->getRootSceneNode()->createChildSceneNode(pos),
		SCENE->createEntity(meshName), 1),
	shooter(shooter)
{
	init();
}

Projectile::Projectile(const Projectile &projectile) :
	MovableObject(projectile),
	shooter(projectile.shooter)
{
	init();
}

Projectile::~Projectile()
{

}

bool Projectile::reactToGravity() const
{
	return true;
}

const Character *Projectile::getShooter() const
{
	return shooter;
}

void Projectile::init() {
	getRootNode()->attachObject(getEntity());
	PHYSICS->setCollision(getEntity()->getName());
}

