#ifndef PROJECTILEFACTORY_H
#define PROJECTILEFACTORY_H

#include "PreProcessor.h"
#include "Projectile.h"
#include "Arrow.h"
#include "Character.h"

/**
 * @brief The ProjectileFactory class constructs projectiles
 */
class ProjectileFactory {
public:

	inline static ProjectileFactory &getInstance() {
		static ProjectileFactory reference;
		return reference;
	}

	inline virtual ~ProjectileFactory() {
		clearProjectiles();
	}

	/**
	 * @brief Projectile enum that maps an int to each projectile class
	 */
	enum Projectiles {
		PROJECTILE_ARROW
	};

	/**
	 * @brief Create a projectile based on input. The factory will not handle the Projectile() lifecycle!
	 * @return A projectile mapped to the PROJECTILES enum
	 */
	inline Projectile *create(const Projectiles &projectile, const Ogre::Vector3 &pos,  const Character *const shooter) {
		Projectile *projectilePtr;
		//const std::string name = "projectile" + Ogre::StringConverter::toString(nProjectiles++);

		switch(projectile) {
			case PROJECTILE_ARROW:
				projectilePtr = new Arrow(pos, shooter);
				break;
			default:
				throw std::runtime_error("No projectile with enum id " + projectile);
		}

		addProjectile(projectilePtr);
		return projectilePtr;
	}

	/**
	 * @brief Add a projectile to the projectile list
	 * @param Name of the projectile
	 * @param Projectile object to add
	 */
	inline void addProjectile(Projectile *const projectile) {
		list[projectile->getEntity()->getName()] = projectile;
	}

	/**
	 * @brief Remove a projectile from the projectile list
	 * @param Name of the projectile
	 * @throw std::runtime_error
	 */
	inline void removeProjectile(const std::string &name) {
		ProjectileList::iterator it = getProjectileIterator(name);
		list.erase(it);
		delete it->second;

	}

	/**
	 * @brief Get a projectile in the projectile list
	 * @param The name of the projectile
	 * @return A pointer to the projectile
	 * @throw std::runtime_error
	 */
	inline Projectile *getProjectile(const std::string &name) {
		ProjectileList::iterator it = getProjectileIterator(name);
		return it->second;
	}

	/**
	 * @brief Delete all projectiles on the field
	 */
	inline void clearProjectiles() {
#if OGRE_DEBUG_MODE == 1
		std::cout << "Clearing projectiles" << std::endl;
#endif
		for(ProjectileList::iterator it = list.begin(); it != list.end(); it++) {
			delete it->second;
		}
		list.clear();
		nProjectiles = 0;
	}

	inline void garbageCollector() {
		ProjectileList::iterator it;
		Projectile *tmp;
		Ogre::Vector3 pos;

		Ogre::AxisAlignedBox box = SCENE->getSceneNode("Ground")->_getWorldAABB();

		for(it = list.begin(); it != list.end(); ) {
			tmp = it->second;
			pos = tmp->getRootNode()->getPosition();

			if(pos.x > box.getMinimum().x && pos.x < box.getMaximum().x &&
					pos.z > box.getMinimum().z && pos.z < box.getMaximum().z && pos.y > 0){
				it++;
			}else{
				delete tmp;
				list.erase(it++);
			}
		}
	}

private:
	// Singleton
	inline ProjectileFactory() {

	}

	// Not allowed to copy
	inline ProjectileFactory(const ProjectileFactory &factory) {
		throw std::runtime_error("Not allowed to copy ProjectileFactory");
	}

	typedef std::map<std::string, Projectile *> ProjectileList;

	/**
	 * @brief Contains a reference to all projectiles created by the factory
	 */
	ProjectileList list;

	/**
	 * @brief Amount of spawned projectiles
	 */
	uint nProjectiles;

	/**
	 * @brief Get an iterator to the projectile
	 * @param Name of the projectile
	 * @return The iterator to the name
	 * @throw std::runtime_error
	 */
	inline ProjectileList::iterator getProjectileIterator(const std::string &name) {
		ProjectileList::iterator it = list.find(name);
		if(it == list.end()) {
			throw std::runtime_error("Could not find projectile " + name);
		}
		return it;
	}
};

#endif // PROJECTILEFACTORY_H
