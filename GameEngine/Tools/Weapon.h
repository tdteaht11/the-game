#ifndef WEAPON_H
#define WEAPON_H

#include "PreProcessor.h"
#include "Tool.h"

/**
 * @brief The Weapon class is the base class for all weapon objects
 */
class Weapon : public Tool {
public:
	Weapon(const Character *const weilder);
	Weapon(const Weapon &weapon);
	virtual ~Weapon();

	/**
	 * @brief Alias of use()
	 */
	inline void attack() {
		use();
	}
};

#endif // WEAPON_H
