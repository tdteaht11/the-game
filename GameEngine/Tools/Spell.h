#ifndef SPELL_H
#define SPELL_H

#include "PreProcessor.h"
#include "Tool.h"

/**
 * @brief All magical spells inherit from this
 */
class Spell : public Tool {
public:
	Spell(const Character *const weilder);
	Spell(const Spell &spell);
	virtual ~Spell();

	/**
	 * @brief Alias of use()
	 */
	inline void cast() {
		use();
	}
};

#endif // SPELL_H
