#ifndef TOOL_H
#define TOOL_H

#include "PreProcessor.h"

class Character;

/**
 * @brief The Tool class is the base class for all tools, such as weapons and spells
 */
class Tool {
public:
	// Compare functions
	typedef bool (*ComparePtr)(Tool const*, Tool const*);
	typedef bool (*Compare)(const Tool &, const Tool &);
	// "Sets" only store unique elements, perfect!
	typedef std::set<Tool *, ComparePtr> List;

	Tool(const Character *const weilder);
	Tool(const Tool &tool);
	virtual ~Tool();

	/**
	 * @brief Needed for abstract class copying
	 * @return Tool object
	 */
	virtual Tool *clone() const = 0;

	/**
	 * @brief Use the tool, such as attacking or casting spells
	 */
	virtual void use() = 0;

	/**
	 * @brief Unique id of the tool
	 * @return A unique id of the tool
	 */
	virtual std::string getName() const = 0;

	/**
	 * @brief Get the weilder of the weapon
	 * @return The weilder of the weapon
	 */
	const Character *getWeilder() const;

	/**
	 * @brief Get the animation name from the weapon
	 * @return std::string with name of animation
	 */
	virtual uchar getAnimationState() const = 0;

	virtual Sound::SoundID getSound() const = 0;

	// Operator overloading
	bool operator==(const Tool &rhs) const;
	bool operator<(const Tool &rhs) const;
	bool operator>(const Tool &rhs) const;
private:
	const Character *const weilder;
};

// Compare contant references
inline bool compareTools(const Tool &lhs, const Tool &rhs) {
	return lhs < rhs;
}

// Compare pointers
inline bool compareTools(Tool *const lhs, Tool *const rhs) {
	return compareTools(*lhs, *rhs);
}

#endif // TOOL_H
