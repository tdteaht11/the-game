#ifndef ENEMY_H
#define ENEMY_H

#include "PreProcessor.h"
#include "NPC.h"

class Enemy : public NPC {
public:
	Enemy(const Ogre::String &mesh);
	Enemy(const Enemy &enemy);
	virtual ~Enemy();

	/**
	 * @brief getAmountEnemies
	 * @return The amount of enemies on the map
	 */
	static uchar getAmountEnemies();

private:
	static uchar nEnemies;
};

#endif // ENEMY_H
