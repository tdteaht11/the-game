#include "GameRoot.h"
#include "Utils.h"
#include "Tools/ProjectileFactory.h"
#include "CharacterUtils.h"

#define OGRE_PLUGINS_CFG OGRE_DIR "plugins" SUFFIX ".cfg"
#define OGRE_CFG OGRE_DIR "ogre" SUFFIX ".cfg"
#define OGRE_LOG OGRE_DIR "log" SUFFIX ".log"
#define OGRE_RESOURCES  OGRE_DIR "resources" SUFFIX ".cfg"

// Respawn time in milliseconds
#define RESPAWNTIME 5000

GameRoot::GameRoot() :
	// Actions per second
	QtOgre::WindowEventListener(200),
	root(new Ogre::Root(OGRE_PLUGINS_CFG, OGRE_CFG, OGRE_LOG)),
	scene(root->createSceneManager(Ogre::ST_GENERIC, SCENE_MANAGER)),
	ogreWindow(0),
	arena(new Arena(scene)),
	physics(new Physics::PhysicsObject(arena, getActionsPerSecond())),
	ai(new AIS::AIObject(arena)),
	sound(new Sound::SoundObject()),
	gcThread(&GameRoot::garbageCollectorFunc, this)
{

	setRenderSystem();
	root->initialise(false);
}

GameRoot& GameRoot::getInstance() {
	static GameRoot reference;
	return reference;
}

GameRoot::~GameRoot() {
#if OGRE_DEBUG_MODE == 1
	delete debugDrawer;
#endif
	delete arena;
	delete ai;
	delete sound;
	delete physics;
	delete root;
}

void GameRoot::setRenderSystem() const {
	const Ogre::RenderSystemList &renderSystems = root->getAvailableRenderers();

#if OGRE_DEBUG_MODE == 1
	// Make sure there are any render systems
	assert(!renderSystems.empty());
#endif

	root->setRenderSystem(*renderSystems.begin());
}

void GameRoot::loadResources(){
	Ogre::ConfigFile cf;
	cf.load(OGRE_RESOURCES);

	// Go through all sections & settings in the file
	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

	while (seci.hasMoreElements()) {

		Ogre::String secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;

		for (i = settings->begin(); i != settings->end(); ++i) {
			Ogre::String typeName = i->first;
			Ogre::String archName = i->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
						archName, typeName, secName);
		}

	}

	// Initialize resource groups
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void GameRoot::handleInput() {
	Ogre::Vector3 moveVec = Ogre::Vector3::ZERO;

	const Ogre::Radian yaw = arena->getPlayer()->getYawNode()->getOrientation().getYaw();

	for(uchar i = 0; i < currentKeys.size(); i++){
		if(!currentKeys[i]) continue;

		switch(i){
		case QtOgre::WindowEventListener::KEY_SHUTDOWN:
			QApplication::exit();
			break;
		case QtOgre::WindowEventListener::KEY_MOVE_FORWARD:
			moveVec += CharacterUtils::calculateMovement(yaw);
			break;
		case QtOgre::WindowEventListener::KEY_MOVE_BACKWARDS:
			moveVec -= CharacterUtils::calculateMovement(yaw);
			break;
		case QtOgre::WindowEventListener::KEY_STRAFE_LEFT:
			moveVec += CharacterUtils::calculateStrafeLeft(yaw);
			break;
		case QtOgre::WindowEventListener::KEY_STRAFE_RIGHT:
			moveVec += CharacterUtils::calculateStrafeRight(yaw);
			break;
		}

		if(currentKeys[i] == previousKeys[i]) continue;

		switch(i){
		case QtOgre::WindowEventListener::KEY_ATTACK:
			arena->getPlayer()->attack();
			break;
		case QtOgre::WindowEventListener::KEY_USE:
			ProjectileFactory::getInstance().clearProjectiles();
			break;
		case QtOgre::WindowEventListener::KEY_JUMP:
			arena->getPlayer()->jump();
			break;
		}
	}

	moveVec.normalise();
	arena->getPlayer()->setVelocity(moveVec);
	previousKeys = currentKeys;
}

void GameRoot::garbageCollector() {
	gcThread.launch();
}

void GameRoot::garbageCollectorFunc() {
	SOUND->garbageCollector();
	ProjectileFactory::getInstance().garbageCollector();
}

void GameRoot::onGraphics(){
	ogreWindow->update(false);
	ogreWindow->swapBuffers();
	root->renderOneFrame();
}

void GameRoot::onAction(const int &interval){
	// Update bots
	ai->onUpdate();
	handleInput();

	//Sound
	SOUND->setListener(arena->getPlayer()->getRootNode()->getPosition(),
			   arena->getPlayer()->getRootNode()->getOrientation().zAxis());

	physics->simulate();

	arena->updateCharacterAnimations(interval);
	Ogre::Viewport *view = ogreWindow->getViewport(0);
#if OGRE_DEBUG_MODE  == 1
	assert(view);
#endif
	Player *player = arena->getPlayer();
#if OGRE_DEBUG_MODE == 1
	assert(player);
	assert(player->getDeadCamera());
#endif
	if(view->getCamera() == player->getDeadCamera() && player->getDeathTime() > RESPAWNTIME) {
		arena->getPlayer()->respawn();
	}

	arena->spawnEnemies();
}

void GameRoot::onKeyPressed(const uchar &key){
	currentKeys.set(key);
}

void GameRoot::onKeyReleased(const uchar &key) {
	currentKeys.reset(key);
}

void GameRoot::onMouseMoved(const sint &x, const sint &y) {
	arena->getPlayer()->handleMouse(x, y);
}

void GameRoot::onSetup(Ogre::RenderWindow* renderSystem) {
	this->ogreWindow = renderSystem;
	loadResources();

#if OGRE_DEBUG_MODE == 1
	// Draw lines in physics simulation
	debugDrawer = new Physics::OgreDebugDrawer(scene);
	physics->setDebugDrawer(debugDrawer);
#endif

	arena->loadMap();
	ai->setUp(arena->getBase()->getPosition());
	arena->init();
}
