#include "AnimatedCharacter.h"
#include "Tools/RangedWeapon.h"
#include "GameRoot.h"

AnimatedCharacter::AnimatedCharacter(Ogre::Entity *entity) : Character(entity) {
	setupAnimations();
}

AnimatedCharacter::AnimatedCharacter(const AnimatedCharacter &obj) : Character(obj) {
}

AnimatedCharacter::~AnimatedCharacter() {

}

void AnimatedCharacter::setTopAnimation(const AnimID &id, const bool &reset) {
	if (topAnimID >= 0 && topAnimID < ANIM_COUNT) {
		// Fade out old animation
		fadingIn[topAnimID] = false;
		fadingOut[topAnimID] = true;
	}

	topAnimID = id;

	if(id != ANIM_NONE){
		animations[id]->setEnabled(true);
		animations[id]->setWeight(0);

		fadingIn[id] = true;
		fadingOut[id] = false;

		if(reset) animations[id]->setTimePosition(0);
	}
}

void AnimatedCharacter::setBaseAnimation(const AnimID &id, const bool &reset) {
	if (baseAnimID >= 0 && baseAnimID < ANIM_COUNT) {
		// Fade out old animation
		fadingIn[baseAnimID] = false;
		fadingOut[baseAnimID] = true;
	}

	baseAnimID = id;

	if(id != ANIM_NONE){
		animations[id]->setEnabled(true);
		animations[id]->setWeight(0);

		fadingIn[id] = true;
		fadingOut[id] = false;

		if(reset) animations[id]->setTimePosition(0);
	}
}

void AnimatedCharacter::setupAnimations() {
	getEntity()->getSkeleton()->setBlendMode(Ogre::ANIMBLEND_CUMULATIVE);

	std::string animNames[] =
		{"IdleBase", "IdleTop", "RunBase", "RunTop", "HandsClosed", "HandsRelaxed", "DrawSwords",
		"SliceVertical", "SliceHorizontal", "Dance", "JumpStart", "JumpLoop", "JumpEnd"};

	// populate our animation list
	for (int i = 0; i < ANIM_COUNT; i++) {
		animations[i] = getEntity()->getAnimationState(animNames[i]);
		animations[i]->setLoop(true);

		fadingIn[i] = false;
		fadingOut[i] = false;
	}

	setIdleAnimation();

	animations[ANIM_HANDS_RELAXED]->setEnabled(true);
}

void AnimatedCharacter::setIdleAnimation() {
	setBaseAnimation(ANIM_IDLE_BASE);
	setTopAnimation(ANIM_IDLE_TOP);
}

void AnimatedCharacter::updateAnim(const Ogre::Real &offset) {
	bool airborne = PHYSICS->getCustomData(getEntity()->getName())->isAirborne();
	bool moving = PHYSICS->getCustomData(getEntity()->getName())->isMoving();

	Ogre::Real baseAnimSpeed = 1;
	Ogre::Real topAnimSpeed = 1;
	timer += offset;

	// ATTACKS
	if(topAnimID == ANIM_SLICE_HORIZONTAL || topAnimID == ANIM_SLICE_VERTICAL) {
		if(timer >= animations[topAnimID]->getLength()) {
			// If base is idling
			if(baseAnimID == ANIM_IDLE_BASE) {
				setTopAnimation(ANIM_IDLE_TOP);
			}else{
				setTopAnimation(ANIM_RUN_TOP);
				// Synchronize top and base animations
				animations[ANIM_RUN_TOP]->setTimePosition(animations[ANIM_RUN_BASE]->getTimePosition());
			}
		}

		// Don't sway hips when attacking
		if(baseAnimID == ANIM_IDLE_BASE) baseAnimSpeed = 0;
	}
	// JUMP START
	else if(baseAnimID == ANIM_JUMP_START) {
		if(timer >= animations[baseAnimID]->getLength()) {
			// Jump start is finished, continue with loop
			setBaseAnimation(ANIM_JUMP_LOOP, true);
		}
	}
	// JUMP LOOP
	else if(baseAnimID == ANIM_JUMP_LOOP && !airborne){
		setBaseAnimation(ANIM_JUMP_END, true);
	}
	// JUMP END
	else if(baseAnimID == ANIM_JUMP_END) {
		if(timer >= animations[baseAnimID]->getLength()) {
			// Set running och idling animation depending on moving
			if(moving) {
				setBaseAnimation(ANIM_RUN_BASE, true);
				setTopAnimation(ANIM_RUN_TOP, true);
			}else{
				setBaseAnimation(ANIM_IDLE_BASE);
				setTopAnimation(ANIM_IDLE_TOP);
			}
		}
	}else if(!moving && baseAnimID != ANIM_JUMP_LOOP){
		setIdleAnimation();
	}

	// MOVING
	if(moving && baseAnimID == ANIM_IDLE_BASE) {
		// Start running
		setBaseAnimation(ANIM_RUN_BASE, true);

		// If idling on top, set running top
		if(topAnimID == ANIM_IDLE_TOP) setTopAnimation(ANIM_RUN_TOP, true);
	}

	// Update animations
	if(baseAnimID != ANIM_NONE) animations[baseAnimID]->addTime(offset * baseAnimSpeed);
	if(topAnimID != ANIM_NONE) animations[topAnimID]->addTime(offset * topAnimSpeed);

	// Fade transitions between animations
	fadeAnimations(offset);
}

void AnimatedCharacter::fadeAnimations(const Ogre::Real &offset) {
	for(int i = 0; i < ANIM_COUNT; i++) {
		if(fadingIn[i]) {
			// slowly fade this animation in until it has full weight
			Ogre::Real newWeight = animations[i]->getWeight() + offset * ANIM_FADE_SPEED;
			animations[i]->setWeight(Ogre::Math::Clamp<Ogre::Real>(newWeight, 0, 1));
			if (newWeight >= 1) fadingIn[i] = false;

		} else if(fadingOut[i]) {
			// slowly fade this animation out until it has no weight, and then disable it
			Ogre::Real newWeight = animations[i]->getWeight() - offset * ANIM_FADE_SPEED;
			animations[i]->setWeight(Ogre::Math::Clamp<Ogre::Real>(newWeight, 0, 1));
			if(newWeight <= 0) {
				animations[i]->setEnabled(false);
				fadingOut[i] = false;
			}
		}
	}
}

void AnimatedCharacter::jump() {
	Character::jump();

	if(topAnimID == ANIM_IDLE_TOP || topAnimID == ANIM_RUN_TOP) {
		setBaseAnimation(ANIM_JUMP_START, true);
		setTopAnimation(ANIM_NONE);

		// Reset timer
		timer = 0;
	}
}

void AnimatedCharacter::attack() {
	AnimID anim = (AnimID) getEquippedTool()->getAnimationState();

	if(animations[anim]->hasEnded() || !animations[anim]->getEnabled()) {
		setTopAnimation(anim, true);

		// Reset timer
		timer = 0;

		Character::attack();
	}
}
