#ifndef PLAYER_H
#define PLAYER_H

#include "PreProcessor.h"
#include "AnimatedCharacter.h"

/**
 * @brief Playable characters
 */
class Player : public AnimatedCharacter {
public:
	Player(const Ogre::String &mesh = "Sinbad.mesh");
	Player(const Player &player);
	virtual ~Player();

	/**
	 * @brief Handle mouse input and react the character accordingly
	 * @param Delta X-coords
	 * @param Delta Y-coords
	 */
	virtual void handleMouse(const int &x, const int &y);

	/**
	 * @brief Handles what will happen when a player dies
	 */
	void die();

	/**
	 * @brief Handles what will happen when a player respawns
	 */
	void respawn();

	/**
	 * @brief Get amount of player controlled characters
	 * @return Amount of player controlled characters
	 */
	inline static uchar getPlayers() {
		return nPlayers;
	}

	inline Ogre::Camera *getDeadCamera() const {
		return deadCamera;
	}

	inline Ogre::Camera *getPlayerCamera() const {
		return playerCamera;
	}

	inline unsigned long getDeathTime() const {
		return Ogre::Root::getSingleton().getTimer()->getMilliseconds() - deathTimer;
	}

	Ogre::SceneNode *getEntityNode() const;
	void setEntityNode(Ogre::SceneNode *value);

private:
	Ogre::SceneNode *entityNode;
	Ogre::Camera *deadCamera;
	Ogre::Camera *playerCamera;

	unsigned long deathTimer;

	/**
	 * @brief Create camera and viewport
	 * @param The scene manager
	 */
	void createCamera(Ogre::SceneManager *scene);

	/**
	 * @brief Handle pitch calculations
	 * @param The angle the player is facing right now
	 */
	void handlePitch(Ogre::Radian angle);

	inline void setDeadCamera(Ogre::Camera *cam) {
		deadCamera = cam;
	}

	inline void setPlayerCamera(Ogre::Camera *cam) {
		playerCamera = cam;
	}

	inline void setDeathTime(const unsigned long &dt) {
		deathTimer = dt;
	}

	static uchar nPlayers;
};

#endif // PLAYER_H
