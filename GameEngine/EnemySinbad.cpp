#include "EnemySinbad.h"

EnemySinbad::EnemySinbad() :
	Enemy("Sinbad.mesh")
{
	Ogre::Entity *entity = getEntity();
	entity->detachFromParent();
	Ogre::SceneNode *entityNode = getRootNode()->createChildSceneNode();
	entityNode->yaw(Ogre::Radian(Ogre::Math::PI));
	entityNode->attachObject(entity);
}

EnemySinbad::~EnemySinbad()
{

}
