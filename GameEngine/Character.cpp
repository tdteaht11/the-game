#include "Character.h"
#include "CharacterUtils.h"
#include "GameRoot.h"

Character::Character(Ogre::Entity *entity) :
	MovableObject(SCENE->getRootSceneNode()->createChildSceneNode(), entity),
	Hp(),
	pitchNode(getRootNode()) {

	init();
	setHp(getMaxHp());
}

Character::Character(const Character &obj) :
	MovableObject(obj),
	Hp(obj){

	// Copy all tools
	Tool::List::const_iterator it = obj.tools.begin();
	while(it != obj.tools.end()) {
		Tool *t = *it;
		tools.insert(t->clone());
	}

	init();
}

Character::~Character() {
	Tool::List::iterator it = tools.begin();
	while (it != tools.end()) {
		// Increment, but save a reference to the previous object
		Tool::List::iterator tmp = it;
		it++;

		Tool *t = *tmp;
		// Delete the previous object
		delete t;
	}

	SOUND->removeCharacter(getEntity()->getName());
}

void Character::strafeLeft() {
	Ogre::Radian yaw = getYawNode()->getOrientation().getYaw();
	setVelocity(CharacterUtils::calculateStrafeLeft(yaw));
}

void Character::strafeRight() {
	Ogre::Radian yaw = getYawNode()->getOrientation().getYaw();
	setVelocity(CharacterUtils::calculateStrafeRight(yaw));
}

void Character::moveDiagonal(const Character::Diagonal &diagonal)
{
	Ogre::Vector3 moveVec = Ogre::Vector3::ZERO;
	const Ogre::Radian yaw = getYawNode()->getOrientation().getYaw();

	// Move character forwards
	Ogre::Vector3 straight = CharacterUtils::calculateMovement(yaw);
	// Oops, was it backwards? Set to opposite direction
	if(diagonal == DIAGONAL_BACKWARD_RIGHT || diagonal == DIAGONAL_BACKWARD_LEFT) {
		straight = -straight;
	}
	moveVec += straight;

	if(diagonal == DIAGONAL_BACKWARD_LEFT || diagonal == DIAGONAL_FORWARD_LEFT) {
		// Move character diagonally left
		moveVec += CharacterUtils::calculateStrafeLeft(yaw);
	} else {
		// Move character diagonally right
		moveVec += CharacterUtils::calculateStrafeRight(yaw);
	}

	setVelocity(moveVec);
}

void Character::setVelocity(const Ogre::Vector3 &position) {
	if(position != Ogre::Vector3::ZERO) {
		SOUND->playFootsteps(getEntity()->getName(), getRootNode()->getPosition());
	}else{
		SOUND->stopFootsteps(getEntity()->getName());
	}

	MovableObject::setVelocity(position);
}

void Character::stopVelocity() {
	SOUND->stopFootsteps(getEntity()->getName());

	MovableObject::stopVelocity();
}

void Character::jump() {
	GameRoot::getInstance().getPhysics()->jumpEntity(getEntity());
}

void Character::attack() {
	if(equippedTool == 0) {
		throw std::runtime_error("Select a tool before attacking");
	}

	SOUND->playSound(equippedTool->getSound(), getRootNode()->getPosition());
	equippedTool->use();
}

void Character::equipTool(Tool *const tool) {
	equipTool(tools.find(tool));
}

void Character::equipTool(const Tool::List::const_iterator &it) {
	// Could not find tool
	if(it == tools.end()) {
		throw std::runtime_error("Cant find tool");
	}

	equippedTool = *it;
}

void Character::equipFirstTool() {
	equipTool(tools.begin());
}

void Character::addTool(const Tool &tool) {
	tools.insert(tool.clone());
}

void Character::removeTool(Tool *const tool) {
	Tool::List::iterator it = tools.find(tool);

	// Could not find tool
	if(it == tools.end()) {
		throw std::runtime_error("Cant find tool");
	}

	tools.erase(it);
}

void Character::resetTools() {
	tools.clear();
}

void Character::init() {
	PHYSICS->setAsCharacter(getEntity()->getName());
	SOUND->addCharacter(getEntity()->getName());
}
