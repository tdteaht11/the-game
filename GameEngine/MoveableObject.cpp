#include "MovableObject.h"
#include "GameRoot.h"
#include "CharacterUtils.h"
#include "Utils.h"
#include "MathUtils.h"

MovableObject::MovableObject(Ogre::SceneNode *root, Ogre::Entity *entity, Ogre::Real mass) :
	root(root),
	yawNode(root),
	entity(entity)
{
	root->setPosition(Ogre::Vector3::ZERO);
	init(mass);
}

MovableObject::MovableObject(const MovableObject &obj) :
	root(obj.root),
	yawNode(obj.yawNode),
	entity(obj.entity) {

	init();
}

MovableObject::~MovableObject() {
	// Remove physics simulation
	PHYSICS->removeEntity(entity->getName());
	// Destroy all entities
	Utils::destroyAllAttachedMovableObjects(root);
	// Destroy nodes
	root->removeAndDestroyAllChildren();
	root->getCreator()->destroySceneNode(root);
}

void MovableObject::setVelocity(const Ogre::Vector3 &position) {
	PHYSICS->moveEntity(entity, position * getMovementSpeed());
}

void MovableObject::stopVelocity()
{
	setVelocity(Ogre::Vector3::ZERO);
}

void MovableObject::setPosition(const Ogre::Vector3 &position)
{
	getRootNode()->setPosition(position);
	PHYSICS->setEntityPosition(entity, position);
}

void MovableObject::rotate(const Ogre::Radian &radian) {
	getYawNode()->yaw(radian);
	PHYSICS->setEntityRotation(getEntity(), getYawNode()->getOrientation());
}

void MovableObject::rotate(const Ogre::Vector3 &lookAt)
{
	Ogre::Vector3 look(lookAt.x, getRootNode()->getPosition().y, lookAt.z);
	getRootNode()->lookAt(look, Ogre::Node::TS_WORLD);
	PHYSICS->setEntityRotation(getEntity(), getYawNode()->getOrientation());
}

void MovableObject::moveForward() {
	Ogre::Radian yaw = yawNode->getOrientation().getYaw();
	setVelocity(CharacterUtils::calculateMovement(yaw));
}

void MovableObject::moveBackwards() {
	Ogre::Radian yaw = yawNode->getOrientation().getYaw();
	setVelocity(-CharacterUtils::calculateMovement(yaw));
}

void MovableObject::init(Ogre::Real mass) {
	PHYSICS->registerEntity(entity, root, mass);
}
