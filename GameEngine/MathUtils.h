#ifndef MATHUTILS_H
#define MATHUTILS_H

#include "PreProcessor.h"

namespace MathUtils {

typedef uint Quadrant;

/**
 * @brief Determine which quadrant the coordinates are in
 * @param X-coordinates
 * @param Y-coordinates
 * @return Quadrant, i.e. value 1, 2, 3 or 4
 */
inline Quadrant quadrant(const Ogre::Real &x, const Ogre::Real &y) {
	if(x >= 0 && y >= 0) {
		return 1;
	}

	if(x < 0 && y >= 0) {
		return 2;
	}

	if(x < 0 && y < 0) {
		return 3;
	}

	return 4;

}

}

#endif // MATHUTILS_H
