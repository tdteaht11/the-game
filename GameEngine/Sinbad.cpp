#include "Sinbad.h"

Sinbad::Sinbad() :
	Player("Sinbad.mesh"),
	// Create swords
	sword1(SCENE->createEntity(getEntity()->getName() + "_sword1", "Sword.mesh")),
	sword2(SCENE->createEntity(getEntity()->getName() + "_sword2", "Sword.mesh"))
{
	initSwords();
	getEntityNode()->yaw(Ogre::Radian(Ogre::Math::PI));
}

Sinbad::Sinbad(const Sinbad &sinbad) :
	Player(sinbad)
{

}

Sinbad::~Sinbad()
{

}

void Sinbad::initSwords()
{
	Ogre::Entity *body = getEntity();
	body->attachObjectToBone("Handle.L", sword1);
	body->attachObjectToBone("Handle.R", sword2);
}
