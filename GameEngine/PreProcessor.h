#ifndef GAME_ENGINE_PREPROCESSOR_H
#define GAME_ENGINE_PREPROCESSOR_H

// std
#include <stdexcept>
#include <iostream>
#include <exception>
#include <list>
#include <set>
#include <bitset>

#pragma GCC diagnostic ignored "-Wall"
#include <QApplication>
#include <Ogre.h>
#include <QtOgre.h>
#include <Physics.h>
#include <Sound.h>
#include <OgreDotSceneParser.h>
#include <AIS.h>
#pragma GCC diagnostic pop

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include <window.h>
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define DIR_SEPARATOR "\\"
#else
#define DIR_SEPARATOR "/"
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define SUFFIX "_d"
#else
#define SUFFIX ""
#endif

// Scene manager identifier
#define SCENE_MANAGER "THE_SCENE_MANAGER"
#define WINDOW_TITLE "The Game"
#define OGRE_DIR "GameEngine" DIR_SEPARATOR "ogre" DIR_SEPARATOR

#define PHYSICS GameRoot::getInstance().getPhysics()
#define SOUND GameRoot::getInstance().getSound()
#define SCENE Ogre::Root::getSingleton().getSceneManager(SCENE_MANAGER)

typedef short int sint;
typedef unsigned short int usint;
typedef unsigned int uint;
typedef long int lint;
typedef unsigned long int ulint;
typedef unsigned char uchar;

#define LOSE_TEXT "You R NOOB. U no win bcz enemy pwnzor u!! NOOB!!"

#endif // PREPROCESSOR_H
