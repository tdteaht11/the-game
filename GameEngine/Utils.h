#ifndef UTILS_H
#define UTILS_H

#include "PreProcessor.h"

namespace Utils {

/**
 * http://www.ogre3d.org/forums/viewtopic.php?f=2&t=53647
 * @brief Destory all attached movable objects of a bode
 * @param node
 */
inline void destroyAllAttachedMovableObjects(Ogre::SceneNode* node) {
	// Destroy all the attached objects
	Ogre::SceneNode::ObjectIterator it = node->getAttachedObjectIterator();

	while ( it.hasMoreElements() ) {
		Ogre::MovableObject* pObject = static_cast<Ogre::MovableObject*>(it.getNext());
		node->getCreator()->destroyMovableObject(pObject);
	}

	// Recurse to child SceneNodes
	Ogre::SceneNode::ChildNodeIterator itChild = node->getChildIterator();

	while (itChild.hasMoreElements()) {
		Ogre::SceneNode* child = static_cast<Ogre::SceneNode*>(itChild.getNext());
		destroyAllAttachedMovableObjects(child);
	}
}

}

#endif // UTILS_H
