#include "PreProcessor.h"
#include "GameRoot.h"

int main(int argc, char** argv) {
	try {
		QtOgre::QOgreApplication app(argc, argv);

		GameRoot *gameRoot = &GameRoot::getInstance();
		QtOgre::QOgreWindow window(gameRoot);
		window.show();

		return app.exec();
	} catch(const std::exception &ex) {
		// Error
		std::cerr << ex.what() << std::endl;
	}
}

