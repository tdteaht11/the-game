#include "Player.h"
#include "GameRoot.h"
#include "CharacterUtils.h"
#include "Tools/Bow.h"

uchar Player::nPlayers = 0;

Player::Player(const Ogre::String &mesh) :
	AnimatedCharacter(SCENE->createEntity("PLAYER_1", mesh)) {

	nPlayers++;

	//move(Ogre::Vector3(0, 5, 0));

	setPosition(Ogre::Vector3(40, 3, 0));

	Ogre::SceneNode *yawNode = getRootNode()->createChildSceneNode();
	setYawNode(yawNode);

	setPitchNode(yawNode->createChildSceneNode());

	entityNode = yawNode->createChildSceneNode();
	entityNode->attachObject(getEntity());


	createCamera(SCENE);

	// Add a bow
	Bow bow(this);
	addTool(bow);
	// Equip first available tool
	equipFirstTool();
}

Player::Player(const Player &player) :
	AnimatedCharacter(player),
	entityNode(player.entityNode)
{
}

Player::~Player() {
	nPlayers--;
}

void Player::createCamera(Ogre::SceneManager *scene) {
	// Player camera
	setPlayerCamera(scene->createCamera("PLAYER_CAMERA"));

	Ogre::Viewport *viewport = GameRoot::getInstance().getRenderWindow()
			->addViewport(playerCamera);
	viewport->setAutoUpdated(true);

	const double aspectRatio = (double) viewport->getActualWidth() / (double) viewport->getActualHeight();
	playerCamera->setAspectRatio(aspectRatio);
	playerCamera->setNearClipDistance(1.5);
	playerCamera->setFarClipDistance(3000);

	getPitchNode()->attachObject(playerCamera);

	// Move the camera up on the "head" of the entity
	Ogre::Vector3 position = getEntity()->getBoundingBox().getSize();
	position.x = position.z = 0;
	position.z = 0.5;
	getPitchNode()->translate(position);

	// Dead camera
	setDeadCamera(scene->createCamera("DEAD_CAMERA"));

	deadCamera->setAspectRatio(aspectRatio);
	deadCamera->setNearClipDistance(1.5);
	deadCamera->setFarClipDistance(3000);

	Ogre::SceneNode *deadCameraNode = SCENE->getRootSceneNode()->createChildSceneNode();

	deadCameraNode->attachObject(deadCamera);
	deadCameraNode->translate(Ogre::Vector3(0, 40, 0));
	deadCameraNode->pitch(Ogre::Radian(-Ogre::Math::HALF_PI));
}

// DO NOT MAKE ANGLE CONST OR REFERENCE! The angle is modified within the function,
// but should not be modified outside
void Player::handlePitch(Ogre::Radian angle) {
	Ogre::Radian curPitch = getPitchNode()->getOrientation().getPitch();

	static const Ogre::Radian pitchLock(Ogre::Math::PI / 2);
	if(curPitch + angle > pitchLock) {
		angle = pitchLock - curPitch;
	} else if(curPitch + angle < -pitchLock) {
		angle = -pitchLock - curPitch;
	}

	getPitchNode()->pitch(angle);
}

void Player::handleMouse(const int &x,  const int &y) {
	static const double sensitivity = 0.1;
	handlePitch(Ogre::Radian(y * 0.01 * sensitivity));
	getYawNode()->yaw(Ogre::Radian(x * 0.01 * sensitivity));
	GameRoot::getInstance().getPhysics()->setEntityRotation(getEntity(), getYawNode()->getOrientation());
}

void Player::die() {
	SCENE->getCurrentViewport()->setCamera(deadCamera);
	setDeathTime(Ogre::Root::getSingleton().getTimer()->getMilliseconds());
	setPosition(Ogre::Vector3(0, 100, 0));
}

void Player::respawn() {
	setHp(getMaxHp());
	setDeathTime(0);
	SCENE->getCurrentViewport()->setCamera(playerCamera);

	setPosition(Ogre::Vector3(0, 3, 0));
}
Ogre::SceneNode *Player::getEntityNode() const
{
	return entityNode;
}

void Player::setEntityNode(Ogre::SceneNode *value)
{
	entityNode = value;
}

