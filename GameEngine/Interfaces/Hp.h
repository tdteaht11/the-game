#ifndef HP_H
#define HP_H

#include "PreProcessor.h"

namespace Interfaces {

/**
 * @brief Objects with HP should inherit from this class
 */
class Hp {
public:
	Hp();
	Hp(const Hp &hp);
	virtual ~Hp();

	sint getHp() const;

	/**
	 * @brief Set HP of the object
	 * @param New HP to set
	 * @throw Exceptions::InvalidHp
	 */
	void setHp(const sint &value);

	/**
	 * @brief Add the amount of HP to the object
	 * @param Amount to add
	 * @throw Exceptions::InvalidHp
	 */
	void addHp(const usint  &value);

	/**
	 * @brief Subtract the amount of HP to the object
	 * @param Amount to subtract
	 * @throw Exceptions::InvalidHp
	 */
	void subtractHp(const usint &value);

	/**
	 * @brief Max HP of the object
	 * @return
	 */
	virtual usint getMaxHp() const = 0;

private:
	sint hp;
};

}

#endif // HP_H
