#include "Hp.h"
#include "Exceptions/InvalidHp.h"

namespace Interfaces {

Hp::Hp()
{
}

Hp::Hp(const Hp &hp) :
	hp(hp.hp)
{

}

Hp::~Hp()
{

}

sint Hp::getHp() const
{
	return hp;
}

void Hp::setHp(const sint &value)
{
	// Dead
	if(value <= 0) {
		hp = 0;
		throw Exceptions::InvalidHp(Exceptions::InvalidHp::HP_DEAD);
	}

	// Max hp
	if(value > getMaxHp()) {
		hp = getMaxHp();
		throw Exceptions::InvalidHp(Exceptions::InvalidHp::HP_FULL);
	}

	hp = value;
}

void Hp::addHp(const usint &value)
{
	const sint newHp = hp + value;

	// Max hp
	if(newHp >= getMaxHp()) {
		hp = getMaxHp();
		throw Exceptions::InvalidHp(Exceptions::InvalidHp::HP_FULL);
	}

	hp = newHp;
}

void Hp::subtractHp(const usint &value)
{
	const sint newHp = hp - value;

	// Dead
	if(newHp <= 0) {
		hp = 0;
		throw Exceptions::InvalidHp(Exceptions::InvalidHp::HP_DEAD);
	}

	hp = newHp;
}


}
