#ifndef INTERFACES_DAMAGE_H
#define INTERFACES_DAMAGE_H

#include "PreProcessor.h"

namespace Interfaces {

/**
 * @brief Damage interface handles all objects that should have some kind of damage
 */
class Damage {
public:
	virtual usint getDamage() const = 0;
};

}

#endif // DAMAGE_H
