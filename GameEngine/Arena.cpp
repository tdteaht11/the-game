#include "Arena.h"
#include "GameRoot.h"
#include "Tools/ProjectileFactory.h"
#include "Tools/RangedWeapon.h"
#include "EnemySinbad.h"
#include "Enemy.h"
#include "Exceptions/InvalidHp.h"
#include "Sinbad.h"

#define MAP OGRE_DIR "Map.scene"

const uchar Arena::maxEnemies = 5;

Arena::Arena(Ogre::SceneManager *scene) :
	scene(scene),
	rootNode(scene->getRootSceneNode()),
	player(0) {

#if OGRE_DEBUG_MODE == 1
	assert(scene);
	assert(rootNode);
#endif
}

Arena::Arena(const Arena &arena) :
	scene(arena.scene),
	rootNode(arena.rootNode),
	player(arena.player != 0 ? new Player(*arena.player) : 0)
{
}

Arena::~Arena() {
	delete player;
}

void Arena::init() {
	createPlayer();
	spawnEnemies();
}

void Arena::updateCharacterAnimations(const int &interval) {
	CharacterList::iterator it = characters.begin();
	float offset = interval / 1000.0;

	AnimatedCharacter *temp;

	for(; it != characters.end(); it++){
		temp = it->second;
		temp->updateAnim(offset);
	}
}

void Arena::createPlayer() {
	if(player != 0) {
		throw std::runtime_error("Player already created");
	}

	player = new Sinbad();
	characters[player->getEntity()->getName()] = player;
}

void Arena::loadMap() {
	OgreDotSceneParser::SceneCreator createScene(scene, this);
	createScene.createScene(MAP);

	// Sky
	scene->setSkyDome(true, "Examples/CloudySky", 5, 8);

	// Shadows
	scene->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_MODULATIVE);
	scene->setAmbientLight(Ogre::ColourValue(255, 255, 255));

	// Create light
	Ogre::Light *sun = scene->createLight("SUN_LIGHT");
	sun->setType(Ogre::Light::LT_POINT);
	sun->setPosition(250, 200, 200);
	sun->setDiffuseColour(255, 255, 255);
}

void Arena::removeCharacter(Character *c) {
	Player *player = dynamic_cast<Player *>(c);

	if(player){
		player->die();
	}else{
		characters.erase(c->getEntity()->getName());

		CharacterList::iterator it = enemies.find(c->getEntity()->getName());
		if(it != enemies.end()){
			enemies.erase(c->getEntity()->getName());
		}

		delete c;
	}
}

Character *Arena::getCharacter(const std::string &name)
{
	CharacterList::const_iterator it = characters.find(name);
	if(it == characters.end()) {
		throw std::runtime_error("No such character " + name);
	}
	return it->second;
}

void Arena::spawnEnemies()
{
	for(uchar i = Enemy::getAmountEnemies(); i < 1; i++) {
#if OGRE_DEBUG_MODE == 1
		std::cout << "Spawning enemies" << std::endl;
#endif

		// Testkod för enemy
		Enemy *e = new EnemySinbad();
		characters[e->getEntity()->getName()] = e;
		enemies[e->getEntity()->getName()] = e;
		/*Ogre::Vector3 tmp;
		tmp.x = -43.6;
		tmp.y = 1;
		tmp.z = -2.4;
e->setPosition(tmp);
*/

		e->setPosition(spawn->getPosition());

	}
}

Ogre::SceneNode *Arena::getBase() const
{
	return base;
}

void Arena::onEntity(Ogre::Entity *entity, Ogre::SceneNode *node, const OgreDotSceneParser::Entity &dotEntity) {
	GameRoot::getInstance().getPhysics()->registerEntity(entity, node, dotEntity.getMass());
}

void Arena::onStaticEntity(Ogre::Entity *entity, Ogre::SceneNode *node) {
	GameRoot::getInstance().getPhysics()->registerStaticEntity(entity, node);
}

void Arena::onNavmesh(Ogre::Entity *entity, Ogre::SceneNode *node) {

}

void Arena::onNode(Ogre::SceneNode *node)
{
	if(node->getName() == "Spawn") {
		spawn = node;
		spawn->translate(0, 5, 0);
	} else if(node->getName() == "Base") {
		PHYSICS->registerBase(node);
		base = node;
	}
}

void Arena::onContact(const Physics::CollisionPair::List &collisionList) {
	typedef Physics::CollisionPair::List::const_iterator CollisionIterator;

	for(CollisionIterator it = collisionList.begin(); it != collisionList.end(); it++) {
		const Physics::CollisionPair pair = *it;

		CharacterList::iterator itA = characters.find(pair.getA());
		CharacterList::iterator itB = characters.find(pair.getB());

		if(itA != characters.end()){
			Character *character = itA->second;

			try{
				Projectile *proj = ProjectileFactory::getInstance().getProjectile(pair.getB());

				// Do not allow projectiles to daamge or be removed on self
				if(proj->getShooter() == character) {
					return;
				}

				character->subtractHp(proj->getDamage());
			}catch(const Exceptions::InvalidHp &ex){
				// Character died
				SOUND->playSound(Sound::SOUND_DIE, character->getRootNode()->getPosition());
				removeCharacter(character);
			}catch(const std::exception &ex){
				// B is not a projectile
			}
		}

		if(itB != characters.end()){
			Character *character = itB->second;

			try{
				Projectile *proj = ProjectileFactory::getInstance().getProjectile(pair.getA());

				// Do not allow projectiles to daamge or be removed on self
				if(proj->getShooter() == character) {
					return;
				}

				character->subtractHp(proj->getDamage());
			}catch(const Exceptions::InvalidHp &ex){
				// Character died
				SOUND->playSound(Sound::SOUND_DIE, character->getRootNode()->getPosition());
				removeCharacter(character);
			}catch(const std::exception &ex){
				// A is not a projectile
			}
		}


		try {
			ProjectileFactory::getInstance().removeProjectile(pair.getA());
		} catch(const std::exception &ex) {
			// A not a projectile
		}

		try {
			ProjectileFactory::getInstance().removeProjectile(pair.getB());
		} catch(const std::exception &ex) {
			// B not a projectile
		}
	}
}

void Arena::requestMove(const std::string &id)
{
	Character* c = getCharacter(id);
	c->moveForward();
}

void Arena::requestAttack(const std::string &id)
{
	Character* c = getCharacter(id);
	c->attack();
}

void Arena::requestRangedAttack(const std::string &id, const Ogre::Radian &angle)
{
	Character *c = getCharacter(id);
	RangedWeapon *ranged = reinterpret_cast<RangedWeapon *>(c->getEquippedTool());

	if(ranged){
		c->getPitchNode()->pitch(angle);
		c->attack();
		c->getPitchNode()->pitch(-angle);
	}else{
		throw std::runtime_error("Ranged weapon not equipped.");
	}
}

void Arena::requestBlock(const std::string &id)
{

}

Ogre::Vector3 Arena::requestLocation(const std::string &id)
{
	Character *c = getCharacter(id);
	return c->getRootNode()->getPosition();
}

Ogre::Vector3 Arena::requestPlayerLocation()
{
	return getPlayer()->getRootNode()->getPosition();
}

int Arena::requestActiveToolInfo(const std::string &id)
{

}

bool Arena::requestLineOfSightCheck(const Ogre::Vector3 &origin, const Ogre::Vector3 &target)
{
	return PHYSICS->raycast(origin, target);
}

void Arena::requestTurn(const std::string &id, const Ogre::Radian &angle)
{
	Character *c = getCharacter(id);
	c->rotate(angle);
}

void Arena::requestTurn(const std::string &id, const Ogre::Vector3 &pos)
{
	Character *c = getCharacter(id);
	c->rotate(pos);
}

/*void Arena::requestStop(const std::string &id)
{

}*/

void Arena::onBaseContact(const std::string &name)
{
	CharacterList::const_iterator it = enemies.find(name);
	if(it != enemies.end()) {
		// Its an enemy, LOOOOOOSE!
		QMessageBox msgBox;
		msgBox.setText(LOSE_TEXT);
		msgBox.exec();
		QApplication::quit();
	}
}
