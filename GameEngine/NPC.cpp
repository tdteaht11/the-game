#include "NPC.h"
#include "GameRoot.h"
#include "Tools/Bow.h"
NPC::NPC(const Ogre::String &mesh) :
	AnimatedCharacter(SCENE->createEntity(mesh))
{
	getRootNode()->attachObject(getEntity());

	// Register NPC with the AI library
	GameRoot::getInstance().getAIObject()
			->registerEntity(getEntity()->getName(), getRootNode());

	addTool(Bow(this));
	equipFirstTool();
}

NPC::NPC(const NPC &obj) :
	AnimatedCharacter(obj){

}

NPC::~NPC() {
	// Deregister the NPC from the AI library
	GameRoot::getInstance().getAIObject()
			->killEntity(getEntity()->getName());
}
