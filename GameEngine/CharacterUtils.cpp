#include "CharacterUtils.h"

namespace CharacterUtils {

Ogre::Vector3 calculateMovement(const Ogre::Radian &yaw) {

	Ogre::Vector3 movement(Ogre::Math::Sin(yaw),
						   0,
						   Ogre::Math::Cos(yaw));

	movement.normalise();

	return -movement;
}

double calculatePitch(const Ogre::Radian &pitch) {
	return Ogre::Math::Tan(pitch);
}

// No constant reference please!
Ogre::Vector3 calculateStrafeRight(const Ogre::Radian &yaw) {
	return CharacterUtils::calculateMovement(yaw - Ogre::Radian(Ogre::Math::HALF_PI));
}

// No constant reference please!
Ogre::Vector3 calculateStrafeLeft(const Ogre::Radian &yaw) {
	return CharacterUtils::calculateMovement(yaw + Ogre::Radian(Ogre::Math::HALF_PI));
}

}
