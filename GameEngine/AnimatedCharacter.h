#ifndef ANIMATEDCHARACTER_H
#define ANIMATEDCHARACTER_H

#include "PreProcessor.h"
#include "Character.h"

#define ANIM_FADE_SPEED 7.5f

enum AnimID {
	ANIM_IDLE_BASE,
	ANIM_IDLE_TOP,
	ANIM_RUN_BASE,
	ANIM_RUN_TOP,
	ANIM_HANDS_CLOSED,
	ANIM_HANDS_RELAXED,
	ANIM_DRAW_SWORDS,
	ANIM_SLICE_VERTICAL,
	ANIM_SLICE_HORIZONTAL,
	ANIM_DANCE,
	ANIM_JUMP_START,
	ANIM_JUMP_LOOP,
	ANIM_JUMP_END,
	ANIM_COUNT,
	ANIM_NONE
};


class AnimatedCharacter : public Character {
public:
	AnimatedCharacter(Ogre::Entity *entity);
	AnimatedCharacter(const AnimatedCharacter &obj);
	virtual ~AnimatedCharacter();

//	inline Ogre::AnimationState *getTopAnim() const { return topAnim; }
//	inline Ogre::AnimationState *getBaseAnim() const { return baseAnim; }

	void setTopAnimation(const AnimID &id, const bool &reset = false);
	void setBaseAnimation(const AnimID &id, const bool &reset = false);

	/**
	 * @brief Sets up everything the animations will need
	 */
	void setupAnimations();

	/**
	 * @brief Sets animation to idle
	 */
	void setIdleAnimation();

	/**
	 * @brief Updates the animation
	 * @param offset const Ogre::Real the offset since last update
	 */
	void updateAnim(const Ogre::Real &offset);

	/**
	 * @brief Fades in new animation and fades out old animation
	 * @param offset const Ogre::Real the offset since last update
	 */
	void fadeAnimations(const Ogre::Real &offset);

	/**
	 * @brief Make the character jump
	 */
	virtual void jump();

	/**
	 * @brief Make the character attack
	 */
	virtual void attack();

private:
//	Ogre::AnimationState *topAnim;
//	Ogre::AnimationState *baseAnim;

	Ogre::AnimationState *animations[ANIM_COUNT];
	bool fadingIn[ANIM_COUNT];
	bool fadingOut[ANIM_COUNT];

	// Base and full body animation ID
	AnimID baseAnimID;
	// Top body animation ID
	AnimID topAnimID;

	// Used for keeping track of animation time
	Ogre::Real timer;

};

#endif // ANIMATEDCHARACTER_H
