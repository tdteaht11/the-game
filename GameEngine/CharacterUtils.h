#ifndef CHARACTERUTILS_H
#define CHARACTERUTILS_H

#include "PreProcessor.h"

namespace CharacterUtils {

/**
 * @brief Calculate how much of the movement should be X-coords and how much Z-coords
 * @param The current yaw of the character
 * @return Vector with proper X- and Z-values
 */
Ogre::Vector3 calculateMovement(const Ogre::Radian &yaw);

/**
 * @brief Calculate pitch
 * @param The current pitch of the character
 * @return
 */
double calculatePitch(const Ogre::Radian &pitch);

Ogre::Vector3 calculateStrafeRight(const Ogre::Radian &yaw);

Ogre::Vector3 calculateStrafeLeft(const Ogre::Radian &yaw) ;

}

#endif // CHARACTERUTILS_H
