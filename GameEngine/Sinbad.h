#ifndef SINBAD_H
#define SINBAD_H

#include "PreProcessor.h"
#include "Player.h"

class Sinbad : public Player
{
public:
	Sinbad();
	Sinbad(const Sinbad &sinbad);
	virtual ~Sinbad();

private:
	void initSwords();

	Ogre::Entity *sword1;
	Ogre::Entity *sword2;
};

#endif // SINBAD_H
