!macx {
INCLUDEPATH += $${TINYXML_LOC}
DEPENDPATH += $${TINYXML_LOC}
}

macx{
INCLUDEPATH += /usr/local/include
DEPENDPATH += /usr/local/include
}

!win32 {
LIBS += /usr/local/lib/libtinyxml2.a
}
