#ifndef DOTSCENEFILE_H
#define DOTSCENEFILE_H

#include "PreProcessor.h"
#include "Node.h"

namespace OgreDotSceneParser {

/**
 * @brief The DotSceneFile class is the base of parsing a .scene file
 */
class DotSceneFile {
public:
	/**
	 * @brief DotSceneFile
	 * @param File name, including path, to the file you'd like to parse
	 */
	DotSceneFile(const std::string &file);
	DotSceneFile(const DotSceneFile &dotSceneFile);
	virtual ~DotSceneFile();

	/**
	 * @brief Parse the file. Call getNodesList() afterwards
	 */
	void parse();

	/**
	 * @brief Get a list of nodes, after the parsing has been done
	 * @return List of nodes
	 */
	inline Node::List getNodesList() const {
		return nodesList;
	}

	/**
	 * @brief Constant to identify nodes in the XML
	 */
	static const std::string NODES;
private:
	/**
	 * @brief The document to parse
	 */
	tinyxml2::XMLDocument document;

	/**
	 * @brief List of nodes. Will be set after parsing!
	 */
	Node::List nodesList;

	/**
	 * @brief Name of the XML document to parse
	 */
	const std::string name;

	/**
	 * @brief Parse a node
	 * @param The node to parse
	 */
	void parseNodes(tinyxml2::XMLNode *nodes);
};

}

#endif // DOTSCENEFILE_H
