#include "Node.h"

namespace OgreDotSceneParser {

Node::Node() :
	entity(0),
	light(0) {
}

Node::~Node() {
	delete entity;
	delete light;
}

Node::Node(const Node &node) :
	position(node.position),
	scale(node.scale),
	rotation(node.rotation),
	name(node.name),
	entity(node.entity != 0 ? new Entity(*node.entity) : 0),
	light(node.light != 0 ? new Ogre::Light(*node.light) : 0) {
}

Ogre::Vector3 Node::getPosition() const {
	return position;
}

void Node::setPosition(const Ogre::Vector3 &value) {
	position = value;
}

Ogre::Vector3 Node::getScale() const {
	return scale;
}

void Node::setScale(const Ogre::Vector3 &value) {
	scale = value;
}

Ogre::Quaternion Node::getRotation() const {
	return rotation;
}

void Node::setRotation(const Ogre::Quaternion &value) {
	rotation = value;
}

std::string Node::getName() const {
	return name;
}

void Node::setName(const std::string &value) {
	name = value;
}

Entity *Node::getEntity() const {
	if(entity == 0) {
		throw std::runtime_error("No entity found on the node");
	}

	return entity;
}

void Node::setEntity(const Entity &value) {
	if(entity != 0) {
		delete entity;
	}

	entity = new Entity(value);
}

Ogre::Light *Node::getLight() const {
	if(light == 0) {
		throw std::runtime_error("No light found on the node");
	}

	return light;
}

void Node::setLight(const Ogre::Light &value) {
	if(light != 0) {
		delete light;
	}

	light = new Ogre::Light(value);
}

}
