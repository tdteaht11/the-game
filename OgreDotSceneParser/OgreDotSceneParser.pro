TEMPLATE = lib
!macx:CONFIG += staticlib
CONFIG -= app_bundle
CONFIG -= qt

TARGET = ogredotsceneparser
DESTDIR = $${PWD}/../lib

SOURCES += \
    DotSceneFile.cpp \
    Entity.cpp \
    Node.cpp \
    SceneCreator.cpp \
    ParseFunctions.cpp

HEADERS += \
    OgreDotSceneParser.h \
    PreProcessor.h \
    DotSceneFile.h \
    Entity.h \
    Node.h \
    SceneCreator.h \
    ParseFunctions.h \
    ParseListener.h

include(../Ogre.pri)
include(../TinyXML.pri)
