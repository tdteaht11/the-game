#include "SceneCreator.h"
#include "DotSceneFile.h"
#include "Entity.h"

namespace OgreDotSceneParser {

SceneCreator::SceneCreator(Ogre::SceneManager *scene, ParseListener *listener) :
	scene(scene),
	listener(listener) {

}

SceneCreator::SceneCreator(const SceneCreator &sc) :
	scene(sc.scene),
	listener(sc.listener){

}

SceneCreator::~SceneCreator() {

}

void SceneCreator::createScene(const Node::List &list) {
#if OGRE_DEBUG_MODE == 1
	assert(list.size() > 0);
#endif

	// Iterate through the list and create the scene
	for(Node::List::const_iterator it = list.begin(); it != list.end(); it++) {
		const Node &node = *it;

		try {
			Entity *entity = node.getEntity();
			attachEntity(*entity, node);
		} catch (const std::exception &ex) {
			// Entity does not exist on node
			attachNode(node);
		}

		try {
			Ogre::Light *light = node.getLight();
			Ogre::SceneNode *lightNode = scene->getRootSceneNode()
					->createChildSceneNode(node.getName());
			lightNode->attachObject(light);
		} catch (const std::exception &ex) {
			// Light does not exist on node
		}
	}
}

void SceneCreator::attachNode(const Node &node)
{
	Ogre::SceneNode *sceneNode = scene->getRootSceneNode()
			->createChildSceneNode(node.getName());
	sceneNode->setPosition(node.getPosition());
	sceneNode->setOrientation(node.getRotation());
	sceneNode->setScale(node.getScale());
	listener->onNode(sceneNode);
}

void SceneCreator::createScene(const std::string &sceneFile) {
	DotSceneFile dotScene(sceneFile);
	dotScene.parse();
	Node::List list = dotScene.getNodesList();
	createScene(list);
}

void SceneCreator::attachEntity(const Entity &entity, const Node &node) {
	Ogre::Entity *ogreEntity = scene->createEntity(entity.getName(), entity.getMeshFile());

	Ogre::SceneNode *sceneNode = scene->getRootSceneNode()
			->createChildSceneNode(node.getName());
	sceneNode->setPosition(node.getPosition());
	sceneNode->setOrientation(node.getRotation());
	sceneNode->setScale(node.getScale());
	sceneNode->attachObject(ogreEntity);

	if(entity.getPhysicsType() == Entity::PHYSICS_STATIC) {
		listener->onStaticEntity(ogreEntity, sceneNode);
	} else if(entity.getPhysicsType() == Entity::PHYSICS_NAVMESH) {
		listener->onNavmesh(ogreEntity, sceneNode);
	} else {
		listener->onEntity(ogreEntity, sceneNode, entity);
	}
}

}
