#ifndef OGRE_DOTSCENE_PARSERPREPROCESSOR_H
#define OGRE_DOTSCENE_PARSERPREPROCESSOR_H

// STL
#include <string.h>
#include <stdexcept>
#include <iostream>
#include <list>

#include <Ogre.h>
#include <tinyxml2.h>

typedef short int sint;
typedef unsigned short int usint;
typedef unsigned int uint;
typedef long int lint;
typedef unsigned long int ulint;
typedef unsigned char uchar;

#endif // PREPROCESSOR_H
