#ifndef OGRE_DOTSCENE_PARSER_ENTITY_H
#define OGRE_DOTSCENE_PARSER_ENTITY_H

#include "PreProcessor.h"

namespace OgreDotSceneParser {

/**
 * @brief DotSceneEntity to handle the extra information set in .scene files and not in Ogre::Entities
 */
class Entity {
public:
	Entity();
	virtual ~Entity();
	Entity(const Entity &entity);

	const std::string getPhysicsType() const;
	void setPhysicsType(const std::string &value);

	double getMass() const;
	void setMass(const double &value);

	std::string getMeshFile() const;
	void setMeshFile(const std::string &value);

	std::string getName() const;
	void setName(const std::string &value);

	static const std::string PHYSICS_STATIC;
	static const std::string PHYSICS_NAVMESH;

private:
	std::string physicsType;
	double mass;
	std::string meshFile;
	std::string name;
};

}

#endif // ENTITY_H
