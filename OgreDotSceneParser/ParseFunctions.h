#ifndef PARSEUTILS_H
#define PARSEUTILS_H

#include "PreProcessor.h"
#include "Entity.h"
#include "Node.h"

namespace OgreDotSceneParser {

const std::string POSITION = "position";
const std::string ROTATION = "rotation";
const std::string SCALE = "scale";
const std::string ENTITY = "entity";
const std::string LIGHT = "light";
const std::string ATTENUATION = "attenuation";
const std::string COLOR_DIFFUSE = "colourDiffuse";
const std::string COLOR_SPECULAR = "colourSpecular";

/**
 * @brief Parse XML node to node
 * @param The XML node to parse
 * @return Node object
 */
Node parseNode(tinyxml2::XMLNode *node);

/**
 * @brief Map a string to an Ogre light type
 * @param The string to map
 * @return Ogre light type value
 * @throw runtime_error if not a valid type
 */
Ogre::Light::LightTypes parseLightType(const std::string &type);

/**
 * @brief Parse XLM element to position
 * @param The XML element to parse
 * @return 3 dimensional position vector
 */
Ogre::Vector3 parsePosition(tinyxml2::XMLElement *position);

/**
 * @brief Parse XML element to rotation
 * @param The XML element to parse
 * @return Rotation quaternion
 */
Ogre::Quaternion parseRotation(tinyxml2::XMLElement *rotation);

/**
 * @brief Parse XML element to scale
 * @param The XML element to parse
 * @return 3 diensional scale vector
 */
Ogre::Vector3 parseScale(tinyxml2::XMLElement *scale);

/**
 * @brief Parse XML element to color
 * @param The XML element to parse
 * @return ColorValue object
 */
Ogre::ColourValue parseColor(tinyxml2::XMLElement *color);

/**
 * @brief Parse XML element to attenuation
 * @param The XML element to parse
 * @param The light object to apply the attenuation to
 */
void parseAttenuation(tinyxml2::XMLElement *att, Ogre::Light &light);

/**
 * @brief Parse XML element to entity
 * @param The XML element to parse
 * @return Entity object
 */
Entity parseEntity(tinyxml2::XMLElement *entity);

/**
 * @brief Parse XML element to light
 * @param The XML element to parse
 * @return Light object
 */
Ogre::Light parseLight(tinyxml2::XMLElement *light);

void parseLightRange(tinyxml2::XMLElement *range, Ogre::Light &light);

}

#endif // PARSEUTILS_H
