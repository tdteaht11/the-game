#ifndef OGRE_DOT_SCENE_PARSE_NODES_H
#define OGRE_DOT_SCENE_PARSE_NODES_H

#include "PreProcessor.h"
#include "Entity.h"
#include "Node.h"

namespace OgreDotSceneParser {

class NodeParser {
public:
	NodeParser(CLASS_PREFIX XmlNode *node);
	virtual  ~NodeParser();

	Node parse();

	inline Node getNode() const {
		return nodeData;
	}

	static const std::string POSITION;
	static const std::string ROTATION;
	static const std::string SCALE;
	static const std::string ENTITY;

private:
	CLASS_PREFIX XmlNode *node;
	Node nodeData;

	Ogre::Vector3 parsePosition(CLASS_PREFIX XmlElement *position);
	Ogre::Quaternion parseRotation(CLASS_PREFIX XmlElement *rotation);
	Ogre::Vector3 parseScale(CLASS_PREFIX XmlElement *scale);
	Entity parseEntity(CLASS_PREFIX XmlElement *entity);
};

}

#endif // NODES_H
