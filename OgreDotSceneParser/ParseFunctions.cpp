#include "ParseFunctions.h"

namespace OgreDotSceneParser {

Node parseNode(tinyxml2::XMLNode *node) {
	const std::string name = node->ToElement()->Attribute("name");
	Node nodeData;
	nodeData.setName(name);

	// Loop through each element
	for(tinyxml2::XMLElement *it = node->FirstChildElement(); it; it = it->NextSiblingElement()) {

		const std::string value = it->Value();

		if(value  == POSITION) {
			nodeData.setPosition(parsePosition(it));
		} else if(value  == ROTATION) {
			nodeData.setRotation(parseRotation(it));
		} else if(value  == SCALE) {
			nodeData.setScale(parseScale(it));
		} else if(value  == ENTITY) {
			nodeData.setEntity(parseEntity(it));
		} else if(value == LIGHT) {
			nodeData.setLight(parseLight(it));
		}

	}

	return nodeData;
}

Ogre::Light::LightTypes parseLightType(const std::string &type) {
	if(type == "point") {
		return Ogre::Light::LT_POINT;
	} else if(type == "directional") {
		return Ogre::Light::LT_DIRECTIONAL;
	} else if(type == "spot") {
		return Ogre::Light::LT_SPOTLIGHT;
	} else if(type == "radPoint") {
		throw std::runtime_error("Type radPoint not yet implemented");
	}

	throw std::runtime_error(type + " is not a valid light type");
}

Ogre::Vector3 parsePosition(tinyxml2::XMLElement *position){
	Ogre::Vector3 positionVec;
	positionVec.x  = Ogre::StringConverter::parseReal(position->Attribute("x"));
	positionVec.y  = Ogre::StringConverter::parseReal(position->Attribute("y"));
	positionVec.z  = Ogre::StringConverter::parseReal(position->Attribute("z"));
	return positionVec;
}

Ogre::Quaternion parseRotation(tinyxml2::XMLElement *rotation) {
	Ogre::Quaternion q;
	q.w = Ogre::StringConverter::parseReal(rotation->Attribute("qw"));
	q.x = Ogre::StringConverter::parseReal(rotation->Attribute("qx"));
	q.y = Ogre::StringConverter::parseReal(rotation->Attribute("qy"));
	q.z = Ogre::StringConverter::parseReal(rotation->Attribute("qz"));
	return q;
}

Ogre::Vector3 parseScale(tinyxml2::XMLElement *scale) {
	// Same implementation as position
	return parsePosition(scale);
}

Ogre::ColourValue parseColor(tinyxml2::XMLElement *color) {
	const double red = Ogre::StringConverter::parseReal(color->Attribute("r"));
	const double green = Ogre::StringConverter::parseReal(color->Attribute("g"));
	const double blue = Ogre::StringConverter::parseReal(color->Attribute("b"));
	return Ogre::ColourValue(red, green, blue);
}

void parseAttenuation(tinyxml2::XMLElement *att, Ogre::Light &light) {
	const usint range = Ogre::StringConverter::parseInt(att->Attribute("range"));
	const double quad = Ogre::StringConverter::parseReal(att->Attribute("quadratic"));
	const double constant = Ogre::StringConverter::parseReal(att->Attribute("constant"));
	const double linear = Ogre::StringConverter::parseReal(att->Attribute("linear"));
	light.setAttenuation(range, constant, linear, quad);
}

Entity parseEntity(tinyxml2::XMLElement *entity) {
	const std::string name = entity->Attribute("name");
	const std::string mesh = entity->Attribute("meshFile");
	const double mass = Ogre::StringConverter::parseReal(entity->Attribute("mass"));
	const std::string physicsType = entity->Attribute("physics_type");

	Entity dotSceneEntity;
	dotSceneEntity.setName(name);
	dotSceneEntity.setMeshFile(mesh);
	dotSceneEntity.setMass(mass);
	dotSceneEntity.setPhysicsType(physicsType);

	return dotSceneEntity;
}

Ogre::Light parseLight(tinyxml2::XMLElement *light) {
	const double powerScale = Ogre::StringConverter::parseReal(light->Attribute("powerScale"));

	Ogre::Light ogreLight(light->Attribute("name"));
	ogreLight.setPowerScale(powerScale);
	ogreLight.setType(parseLightType(light->Attribute("type")));

	for(tinyxml2::XMLElement *it = light->FirstChildElement(); it; it = it->NextSiblingElement()) {

		const std::string value = it->Value();

		if(value == POSITION) {
			ogreLight.setPosition(parsePosition(it));
		} else if(value == ATTENUATION) {
			parseAttenuation(it, ogreLight);
		} else if(value == COLOR_DIFFUSE) {
			ogreLight.setDiffuseColour(parseColor(it));
		} else if(value == COLOR_SPECULAR){
			ogreLight.setSpecularColour(parseColor(it));
		} else if(value == "lightRange") {
			parseLightRange(it, ogreLight);
		}

	}

	return ogreLight;
}

void parseLightRange(tinyxml2::XMLElement *range, Ogre::Light &light) {
	const double inner = Ogre::StringConverter::parseReal(range->Attribute("inner"));
	const double outer = Ogre::StringConverter::parseReal(range->Attribute("outer"));
	const double falloff = Ogre::StringConverter::parseReal("falloff");
	light.setSpotlightRange(Ogre::Radian(inner), Ogre::Radian(outer), falloff);
}

}
