#include "DotSceneFile.h"
#include "ParseFunctions.h"

namespace OgreDotSceneParser {

const std::string DotSceneFile::NODES = "nodes";

DotSceneFile::DotSceneFile(const std::string &file) :
	name(file) {

	if(document.LoadFile(file.c_str()) != tinyxml2::XML_SUCCESS) {
		throw std::runtime_error("Could not load file " + file);
	}
}

DotSceneFile::DotSceneFile(const DotSceneFile &dotSceneFile) :
	name(dotSceneFile.name),
	nodesList(dotSceneFile.nodesList) {

	if(document.LoadFile(dotSceneFile.name.c_str()) != tinyxml2::XML_SUCCESS) {
		throw std::runtime_error("Could not load file " + dotSceneFile.name);
	}
}

DotSceneFile::~DotSceneFile() {

}

void DotSceneFile::parse() {
#if OGRE_DEBUG_MODE == 1
	std::cout << "DotScene" << std::endl;
#endif

	// The root node only has 1 child
	tinyxml2::XMLNode *root = document.FirstChild();

	// The first level will contain categories, as "environment" or "nodes"
	for(tinyxml2::XMLNode *it = root->FirstChild(); it; it = it->NextSibling()) {
		// Store the value in a STL string for comparing
		const std::string value = it->Value();

#if OGRE_DEBUG_MODE == 1
		std::cout << value << std::endl;
#endif

		if(value == NODES) {
			this->parseNodes(it);
		}

	}

}

void DotSceneFile::parseNodes(tinyxml2::XMLNode *nodes){
#if OGRE_DEBUG_MODE == 1
	std::cout << "Parsing nodes" << std::endl;
#endif

	for(tinyxml2::XMLNode *it = nodes->FirstChild(); it; it = it->NextSibling()) {
		nodesList.push_back(parseNode(it));
	}
}

}
