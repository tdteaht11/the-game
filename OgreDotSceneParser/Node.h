#ifndef NODE_H
#define NODE_H

#include "PreProcessor.h"
#include "Entity.h"

namespace OgreDotSceneParser {

/**
 * @brief Each parsed node
 */
class Node {
public:
	typedef std::list<Node> List;

	Node();
	virtual ~Node();
	Node(const Node &node);

	Ogre::Vector3 getPosition() const;
	void setPosition(const Ogre::Vector3 &value);

	Ogre::Vector3 getScale() const;
	void setScale(const Ogre::Vector3 &value);

	Ogre::Quaternion getRotation() const;
	void setRotation(const Ogre::Quaternion &value);

	std::string getName() const;
	void setName(const std::string &value);

	/**
	 * @brief Get entity of the node
	 * @return The DotSceneEntity
	 * @throw std::runtime_error
	 */
	Entity *getEntity() const;
	void setEntity(const Entity &value);

	/**
	 * @brief Get light of the node
	 * @return The Ogre::Light
	 * @throw std::runtime_error
	 */
	Ogre::Light *getLight() const;
	void setLight(const Ogre::Light &value);
private:
	Ogre::Vector3 position;
	Ogre::Vector3 scale;
	Ogre::Quaternion rotation;
	std::string name;

	Entity *entity;
	Ogre::Light *light;
};

}

#endif // NODE_H
