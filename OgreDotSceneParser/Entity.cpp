#include "Entity.h"

namespace OgreDotSceneParser {

const std::string Entity::PHYSICS_STATIC = "STATIC";
const std::string Entity::PHYSICS_NAVMESH = "NAVMESH";

Entity::Entity() {

}

Entity::~Entity() {

}

Entity::Entity(const Entity &entity) :
	physicsType(entity.physicsType),
	mass(entity.mass),
	meshFile(entity.meshFile),
	name(entity.name) {
}

const std::string Entity::getPhysicsType() const {
	return physicsType;
}

void Entity::setPhysicsType(const std::string &value) {
	physicsType = value;
}

double Entity::getMass() const {
	return mass;
}

void Entity::setMass(const double &value) {
	mass = value;
}

std::string Entity::getMeshFile() const {
	return meshFile;
}

void Entity::setMeshFile(const std::string &value) {
	meshFile = value;
}

std::string Entity::getName() const {
	return name;
}

void Entity::setName(const std::string &value) {
	name = value;
}

}
