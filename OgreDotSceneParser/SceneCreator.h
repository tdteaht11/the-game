#ifndef SCENECREATOR_H
#define SCENECREATOR_H

#include "PreProcessor.h"
#include "Node.h"
#include "ParseListener.h"

namespace OgreDotSceneParser {

/**
 * @brief Class will take the parsing result and create a scene from it
 */
class SceneCreator {
public:
	SceneCreator(Ogre::SceneManager *scene, ParseListener *listener = 0);
	SceneCreator(const SceneCreator &sc);
	virtual ~SceneCreator();

	/**
	 * @brief Create a scene from the specified Node::List parsing result
	 * @param Node::List parsing result
	 */
	void createScene(const Node::List &list);

	/**
	 * @brief Create a scene directly! This function will handle the parsing
	 * @param Scene file to parse
	 */
	void createScene(const std::string &sceneFile);
private:
	Ogre::SceneManager *scene;
	ParseListener *listener;

	/**
	 * @brief Attach an entity on the scene
	 * @param Entity to attach
	 * @param Node data
	 */
	void attachEntity(const Entity &entity, const Node &node);

	/**
	 * @brief attachNode Attach a node
	 * @param node
	 */
	void attachNode(const Node &node);
};

}

#endif // SCENECREATOR_H
