#ifndef PARSELISTENER_H
#define PARSELISTENER_H

#include "PreProcessor.h"
#include "Entity.h"

namespace OgreDotSceneParser {

/**
 * @brief Listen on nodes that are being attached during scene creation
 */
class ParseListener {
public:
	/**
	 * @brief Called when entities have been attached
	 * @param Ogre entity that has been attached
	 * @param Data about the XML node
	 * @param DotSceneEntity data
	 */
	virtual void onEntity(Ogre::Entity *entity, Ogre::SceneNode *node, const Entity &dotEntity) = 0;

	/**
	 * @brief Called when static entities have been attached
	 * @param Ogre entity that has been attached
	 * @param Data about the XML node
	 */
	virtual void onStaticEntity(Ogre::Entity *entity, Ogre::SceneNode *node) = 0;

	/**
	 * @brief Called when a navmesh has been attached
	 * @param Ogre entity that has been attached
	 * @param Data about the XML node
	 */
	virtual void onNavmesh(Ogre::Entity *entity, Ogre::SceneNode *node) = 0;

	/**
	 * @brief onNode Called on just nodes
	 * @param node
	 */
	virtual void onNode(Ogre::SceneNode *node) = 0;
};

}

#endif // PARSELISTENER_H
