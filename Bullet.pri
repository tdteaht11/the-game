macx {
# Include source files
INCLUDEPATH += $${BULLET_LOC}/src
DEPENDPATH += $${BULLET_LOC}/src

## Libraries that are not frameworks
#LIBS += -L$${BULLET_LOC}/lib

# Libraries
LIBS += -F/Library/Frameworks \
-framework BulletCollision -framework BulletDynamics -framework LinearMath
}


unix:!macx {
INCLUDEPATH += /usr/include/bullet
DEPENDPATH += /usr/include/bullet

LIBS += -lBulletCollision -lBulletDynamics -lLinearMath
}
