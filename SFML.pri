!win32 {
INCLUDEPATH += $${SFML_LOC}/include
DEPENDPATH += $${SFML_LOC}/include
}

macx {
LIBS += -L/usr/local/lib
}

unix:!macx {
LIBS += -L$${SFML_LOC}/lib
}

LIBS += -lsfml-audio -lsfml-system
