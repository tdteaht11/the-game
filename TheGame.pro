cache()

TEMPLATE = subdirs

# The order of the values in SUBDIRS should matter!
CONFIG += ordered

DEFINES += DEBUG _DEBUG

SUBDIRS += \
    Physics \
    QtOgre \
    OgreDotSceneParser \
    AI \
    Sound \
    GameEngine

GameEngine.depends = Physics
GameEngine.depends = QtOgre
GameEngine.depends = OgreDotSceneParser
GameEngine.depends = AI
GameEngine.depends = Sound

OTHER_FILES += lib/*

CONFIG(debug, release|debug): DEFINES += _DEBUG DEBUG

macx {
# Target OS X 10.8 and above
QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.8
}
