#include "SeekState.h"
#include "DefaultState.h"
#include "AttackState.h"
#include "IdleState.h"
#include "AIRoot.h"
#include "Navmesh.h"

namespace AIS{

SeekState &SeekState::Instance()
{
	static SeekState instance;

	return instance;
}

void SeekState::Enter(AI *ai)
{

}

void SeekState::Execute(AI *ai)
{
	Ogre::Vector3 currentPos = AIRoot::Instance().getMessenger()->requestLocation(ai->getID());
	Ogre::Vector3 playerPos(11.3,0,20.9);
	std::cout<<AIRoot::Instance().getMessenger()->requestLocation(ai->getID())<<std::endl;

	Ogre::Vector3 tmp;
	if(ai->getCheckpointBool() == false){

		ai->setCheckPointVector(ai->getNavmesh().runPathfinder(currentPos,playerPos));
		ai->setCheckpointBool(true);
	}else{
		tmp = *ai->getCheckPoint();
		float length = (tmp - currentPos).length();
		if(length<1){
			ai->incCheckPoint();
			if(ai->getCheckPoint() != ai->getCheckpointVector().end()){
				tmp = *ai->getCheckPoint();
			}else{
				ai->getMyStateMachine()->changeState(&IdleState::Instance());
				ai->setCheckpointBool(false);
			}

		}
		AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),tmp);
		AIRoot::Instance().getMessenger()->requestMove(ai->getID());
	}
	std::cout<<currentPos<<std::endl;
	AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),tmp);
	AIRoot::Instance().getMessenger()->requestMove(ai->getID());
	std::cout<<AIRoot::Instance().getMessenger()->requestLocation(ai->getID())<<std::endl;

}

void SeekState::Exit(AI *ai)
{

}

SeekState::~SeekState()
{

}

}
