#ifndef SEEKSTATE_H
#define SEEKSTATE_H
#include "State.h"


namespace AIS {


class SeekState:public State{


	SeekState():State(){}

public:

	/**
	 * @brief Instance Creates a singelton of this object
	 * @return
	 */
	static SeekState &Instance();


	void Enter(AI *ai);
	void Execute(AI *ai);
	void Exit(AI *ai);

	virtual ~SeekState();



};


}


#endif // SEEKSTATE_H
