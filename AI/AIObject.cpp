#include "AIObject.h"
#include "AIRoot.h"
#include <iostream>
#include "DefaultState.h"

namespace AIS {

AIObject::AIObject(AIMessenger *msgr)
{
	AIRoot::Instance().setMessenger(msgr);
}

AIObject::AIObject(const AIObject &obj)
{

}

AIObject::~AIObject()
{

}

/**
 * @brief AIObject::registerEntity registers a AI unit
 * @param entity the AI id
 * @param node the position of the AI
 * @param c determines wich AI class the AI become
 */

void AIObject::registerEntity(const std::string &name, Ogre::SceneNode *node)
{

	GroundAI *tmp = new GroundAI;
	tmp->setLocation(node->getPosition());
	tmp->setID(name);
	tmp->setNavmesh(AIRoot::Instance().getNavmesh());
	std::cout<<"-----------AI-ID----------"<<std::endl;
	std::cout<<tmp->getID()<<std::endl;

	//Place the AI in map
	AIRoot::Instance().setAIEntities(tmp->getID(),tmp);
}

/**
 * @brief AIObject::killEntity will erase the AI from the game
 * @param id is the id of the AI to be erased
 */

void AIObject::killEntity(const std::string &name)
{
	AIRoot::AIList entitys = AIRoot::Instance().getEntities();
	AIRoot::AIList::iterator it = entitys.find(name);
	if(it == entitys.end()) {
		throw std::runtime_error("Could not find " + name);
	}
	delete it->second;
	entitys.erase(name);
}

void AIObject::updatePlayers(Ogre::Vector3 loc, std::string id)
{
	AIRoot::getInstance().setPlayerMap(id, loc);
}

void AIObject::setUp(const Ogre::Vector3 &basePos)
{
	//NavmeshParser tmp;
	plyNavmeshParser tmp;
	AIRoot::Instance().setNavmesh(tmp.run());
	DefaultState::Instance().setGoal(basePos);

}

//"Main"
void AIObject::onUpdate()
{

	AIRoot::AIList aiList = AIRoot::getInstance().getEntities();
	for(AIRoot::AIList::iterator it = aiList.begin(); it != aiList.end(); it++) {
		AI *ai = it->second;
		ai->getMyStateMachine()->update();
	}
}


}
