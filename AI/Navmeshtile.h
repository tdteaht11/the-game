#ifndef NAVMESHTILE_H
#define NAVMESHTILE_H
#include "PreProcessor.h"
namespace AIS {


class NavmeshTile{
	Ogre::Vector3 point1;
	Ogre::Vector3 point2;
	Ogre::Vector3 point3;
	Ogre::Vector3 midpoint;
	double h,g,f;
	std::vector<int> adjacent;
	NavmeshTile *parent;

public:
	NavmeshTile();
	NavmeshTile(const NavmeshTile &in);
	void setBounds(Ogre::Vector3 v3, Ogre::Vector3 v2, Ogre::Vector3 v1);
	std::vector<Ogre::Vector3> getBounds();
	Ogre::Vector3 getMidpoint();
	void setH(double val);
	double getH();
	void setG(double val);
	double getG();
	void calcF();
	double getF();
	void addAdjacent(int i);
	std::vector<int> getAdjacent();
	void setParent(NavmeshTile *parent);
	NavmeshTile* getParent();

	bool operator< (const NavmeshTile &rhs) const{
			return this->f < rhs.f;
	}
	bool operator()(const NavmeshTile &rhs) const{
		return this->f >rhs.f;

	}

	bool operator== (const NavmeshTile &rhs)const{
		if(this->midpoint == rhs.midpoint && this->point1 == rhs.point1 && this->point2 == rhs.point2 && this->point3 == rhs.point3){
			return true;
		}
		return false;
	}

	NavmeshTile operator= (const NavmeshTile &rhs)const{
		NavmeshTile tmp(rhs);
		return tmp;
	}


};

}

#endif // NAVMESHTILE_H
