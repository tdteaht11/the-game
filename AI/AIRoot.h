#ifndef AIROOT_H
#define AIROOT_H

#include "PreProcessor.h"
#include "GroundAI.h"
#include "AIMessenger.h"
#include "Navmesh.h"

namespace AIS {
class AIRoot {
public:
	typedef std::map<std::string, AI*> AIList;
	typedef std::map<std::string, Ogre::Vector3> PlayerList;


private:
	AIList entitys;
	AIMessenger *msgr;


	PlayerList playerMap;
	Navmesh defaultNavmesh;

	AIRoot();
public:

	enum toolType{
		toolTypeRanged, toolTypeMeele, toolTypeSpell
	};

	inline static AIRoot &Instance(){
		static AIRoot instance;
		return instance;
	}

	inline static AIRoot &getInstance() {
		return Instance();
	}

	void setNavmesh(Navmesh in);
	Navmesh getNavmesh();

	AIRoot(const AIRoot &root);
	virtual ~AIRoot();

	PlayerList getPlayerMap() const;
	void setPlayerMap(std::string id, Ogre::Vector3 loc);

	AIList &getEntities();
	void setAIEntities(std::string id, AI *ai);

	AIMessenger *getMessenger() const;
	void setMessenger(AIMessenger *msgr);
};

}

#endif // AIROOT_H
