#ifndef AIOBJECT_H
#define AIOBJECT_H

#include "PreProcessor.h"
#include "AIMessenger.h"
#include "PlyNavmeshParser.h"

namespace AIS {

class AIObject {

	Navmesh GlobalNavmesh;

public:
	AIObject(AIS::AIMessenger *msgr);
	AIObject(const AIObject &obj);
	virtual ~AIObject();

	void registerEntity(const std::string &name, Ogre::SceneNode *node);
	void killEntity(const std::string &name);
	void updatePlayers(Ogre::Vector3 loc, std::string id);
	void setUp(const Ogre::Vector3 &basePos);

	/**
	 * @brief Called whenever the game loops change actions
	 */
	void onUpdate();
};

}

#endif // AIOBJECT_H
