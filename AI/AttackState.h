#ifndef ATTACKSTATE_H
#define ATTACKSTATE_H


#include "PreProcessor.h"
#include "AIMessenger.h"
#include "GroundAI.h"
#include "State.h"


namespace AIS{
class AttackState:public State{

	//Temp meleerange
	Ogre::Real meleeAttackRange;

	AttackState():State(), meleeAttackRange(0.5)
	{}

	/**
	 * @brief calcLowAngle calculates the low tradjectory for the missile
	 * @param myLocation the location of the shooter
	 * @param targetLocation the location of the target
	 * @return
	 */
	Ogre::Radian calcLowAngle(Ogre::Vector3 myLocation, Ogre::Vector3 targetLocation);

	/**
	 * @brief calcHighAngle calculates the high tradjectory for the missile
	 * @param myLocation the location of the shooter
	 * @param targetLocation the location of the target
	 * @return
	 */
	Ogre::Radian calcHighAngle(Ogre::Vector3 myLocation, Ogre::Vector3 targetLocation);

	/**
	 * @brief inAttackRange calculates if its possible to hit a meele attack
	 * @param myLocation
	 * @param targetLocation
	 * @return
	 */
	bool inAttackRange(Ogre::Vector3 myLocation, Ogre::Vector3 targetLocation);

	bool isLookingAtTarget(Ogre::Vector3 targetLocation);


public:

	static AttackState& Instance();

	void Enter(AI *ai);
	void Execute(AI *ai);

	void Exit(AI *ai);

	inline virtual ~AttackState(){

	}
};
}




#endif // ATTACKSTATE_H
