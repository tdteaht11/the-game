#ifndef STATE_H
#define STATE_H


#include "PreProcessor.h"


namespace AIS{
class AI;
class State{


public:

	inline State() {

	}

	virtual void Enter(AI *ai) = 0;
	virtual void Execute(AI *ai) = 0;
	virtual void Exit(AI *ai) = 0;

	inline virtual ~State(){

	}

};


}


#endif // STATE_H
