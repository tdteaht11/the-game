#ifndef IDLESTATE_H
#define IDLESTATE_H


#include "PreProcessor.h"
#include "State.h"

namespace AIS{
class IdleState:public State{

public:
	static IdleState &Instance();

	virtual ~IdleState();

	void Enter(AI *ai);
	void Execute(AI *ai);
	void Exit(AI *ai);



};


}
#endif // IDLESTATE_H
