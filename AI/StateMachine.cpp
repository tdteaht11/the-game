#include "StateMachine.h"
#include "DefaultState.h"
#include "AttackState.h"
#include "IdleState.h"
#include "SeekState.h"
#include "AI.h"

namespace AIS{

StateMachine::StateMachine(AI *owner):owner(owner),currentState(&DefaultState::Instance()),lastState(NULL),globalState(NULL){
}

void StateMachine::setCurrentState(State *state){
	currentState = state;
}

void StateMachine::setLastState(State *state){
	lastState = state;
}

void StateMachine::setGlobalState(State *state){
	globalState = state;
}

void StateMachine::update() const{

	//If global state is set call its execute
	if(globalState){
		globalState->Execute(owner);
	}
	//If a active state i set call its execute
	if(currentState){
		currentState->Execute(owner);
	}

}

void StateMachine::changeState(State *newState){

	lastState = currentState;
	currentState->Exit(owner);
	currentState = newState;
	currentState->Enter(owner);

}

void StateMachine::useLastState(){
	changeState(lastState);
}

}
