#include "Navmesh.h"
#include "AIRoot.h"
#include <algorithm>
#include "HelpFunctions.h"

namespace AIS {

Navmesh::Navmesh()
{
}


void Navmesh::addNavmeshTile(Ogre::Vector3 v3, Ogre::Vector3 v2, Ogre::Vector3 v1)
{
	NavmeshTile tmp;

	tmp.setBounds(v3,v2,v1);

	tileVector.push_back(tmp);


}

NavmeshTile *Navmesh::findTile(Ogre::Vector3 point)
{

	for(int i=0;i<tileVector.size();i++){
		NavmeshTile tmp = tileVector[i];

		if(PointInTriangle(point, tmp)){
			return &tileVector[i];
		}
	}
	return 0;

}

float Navmesh::sign(Ogre::Vector3 p1, Ogre::Vector3 p2, Ogre::Vector3 p3)
{
	return (p1.x - p3.x) * (p2.z - p3.z) - (p2.x - p3.x) * (p1.z - p3.z);
}

std::vector<Ogre::Vector3> Navmesh::findPath(NavmeshTile *start, NavmeshTile *goal)
{
	std::vector<NavmeshTile*> closed;
	std::vector<NavmeshTile*> open;
	std::vector<Ogre::Vector3> tmp;


	start->setG(0);
	start->calcF();


	open.push_back(start);

	std::make_heap(open.begin(),open.end());
	std::sort_heap(open.begin(),open.end());

	while(!open.empty()){
		// std::reverse(open.begin(),open.end());
		NavmeshTile *currentTile = open.front();


		for(int i=0;i<currentTile->getAdjacent().size();i++){
			if(*goal == tileVector[currentTile->getAdjacent()[i]]){
				return reconstructPath(*currentTile, start, goal);
			}
		}
		closed.push_back(currentTile);
		std::pop_heap(open.begin(),open.end());;
		open.pop_back();

		for(int i=0;i<currentTile->getAdjacent().size();i++){
			if(isInVector(tileVector[currentTile->getAdjacent()[i]], closed)){
				continue;
			}
			double walkcost = std::abs((currentTile->getMidpoint()-tileVector[currentTile->getAdjacent()[i]].getMidpoint()).length());
			double tmpScore =currentTile->getG() + walkcost;


			bool isInOpen = isInVector(tileVector[currentTile->getAdjacent()[i]],open);

			if(!isInOpen || tmpScore < tileVector[currentTile->getAdjacent()[i]].getG()){
				tileVector[currentTile->getAdjacent()[i]].setParent(currentTile);
				tileVector[currentTile->getAdjacent()[i]].setG(tmpScore);
				tileVector[currentTile->getAdjacent()[i]].calcF();
				if(!isInOpen){
					open.push_back(&tileVector[currentTile->getAdjacent()[i]]);
					std::push_heap(open.begin(),open.end());
				}
			}
		}

	}

	return tmp;

}

std::vector<Ogre::Vector3> Navmesh::reconstructPath(NavmeshTile lastTile, NavmeshTile *start, NavmeshTile *goal)
{
	std::vector<Ogre::Vector3> tmp;
	NavmeshTile *tmptile = &lastTile;
	Ogre::Vector3 tmpgoal = goal->getMidpoint();

	tmp.push_back(tmpgoal);
	while(true){
		Ogre::Vector3 tmpvector = tmptile->getMidpoint();
		tmp.push_back(tmpvector);
		tmptile = tmptile->getParent();
		if(tmptile == start){
			break;
		}
	}
	std::reverse(tmp.begin(),tmp.end());

	return smoothPath(tmp,goal);
}

void Navmesh::calcHeuristics(NavmeshTile *goal){
	for(int i=0;i<tileVector.size();i++){
		tileVector[i].setH(std::abs((goal->getMidpoint() - tileVector[i].getMidpoint()).length()));
	}
}

bool Navmesh::PointInTriangle(Ogre::Vector3 pt, NavmeshTile curr)
{
	Ogre::Vector3 v1, v2, v3;
	v1 = curr.getBounds()[0];
	v2 = curr.getBounds()[1];
	v3 = curr.getBounds()[2];
	bool b1, b2, b3;

	b1 = sign(pt, v1, v2) < 0.0f;
	b2 = sign(pt, v2, v3) < 0.0f;
	b3 = sign(pt, v3, v1) < 0.0f;

	return ((b1 == b2) && (b2 == b3));

}

bool Navmesh::isInVector(NavmeshTile tile, std::vector<NavmeshTile *> vec)
{
	for(int i=0;i<vec.size();i++){
		if(*vec[i] == tile){
			return true;
		}
	}
	return false;
}

std::vector<Ogre::Vector3> Navmesh::smoothPath(std::vector<Ogre::Vector3> in, NavmeshTile *goal)
{
	/*Ogre::Vector3 last;
	Ogre::Vector3 otherLast;
	std::vector<Ogre::Vector3> ret;
	ret.push_back(in[0]);
	Ogre::Vector3 currIt = *in.begin();
	std::vector<Ogre::Vector3>::iterator checkIt = in.begin();
	checkIt++;
	bool br = false;
	last = *checkIt;
	for(int i=0;checkIt != in.end();i++,checkIt++){
		if(currIt != *checkIt){
			bool lineOfsight = AIRoot::Instance().getMessenger()->requestLineOfSightCheck(currIt,*checkIt);
			if(lineOfsight){
				ret.push_back(last);
				currIt = last;
				checkIt = in.begin();
				checkIt+=i;
			}else{
				last = *checkIt;
				checkIt++;
			}
		}
	}
	ret.push_back(goal->getMidpoint());



	return ret;*/



	Ogre::Vector3 last;
	Ogre::Vector3 otherLast;
	std::vector<Ogre::Vector3> ret;
	ret.push_back(in[0]);
	Ogre::Vector3 currIt = *in.begin();
	std::vector<Ogre::Vector3>::iterator checkIt = in.begin();
	bool br = false;
	bool toggle = false;

	while(!br){
		while(checkIt != in.end() && !br){
			if(currIt != *checkIt){
				//bool line = AIRoot::Instance().getMessenger()->requestLineOfSightCheck(currIt,*checkIt);
				bool line = helpLineOfSight(currIt, *checkIt);
				if(!line){
					if(toggle){
						last = *checkIt;
						toggle = !toggle;
					}else {
						otherLast = *checkIt;
						toggle = !toggle;
					}
				}else if(last != Ogre::Vector3::ZERO){
					if(toggle){
						ret.push_back(last);
					}else {
						ret.push_back(otherLast);
					}
					currIt = *checkIt;
					checkIt++;
					if(checkIt == in.end()){
						in.push_back(*checkIt--);
						br = true;
					}
					break;
				}
			}
			checkIt++;
			if(checkIt == in.end()){
				in.push_back(*checkIt--);
				br = true;
			}
		}
		last = Ogre::Vector3::ZERO;
		if(checkIt == in.end()){
			in.push_back(*checkIt--);
			br = true;
		}
	}
	ret.push_back(goal->getMidpoint());

	for(int i=0;i<ret.size();i++){
		std::cout<<ret[i]<<std::endl;
	}
	return ret;
}


void Navmesh::setAdjacent(int current, int check)
{
	tileVector[current].addAdjacent(check);
}

std::vector<NavmeshTile> Navmesh::getTileVector()
{
	return this->tileVector;
}

int Navmesh::size()
{
	return this->tileVector.size();
}

std::vector<Ogre::Vector3> Navmesh::runPathfinder(Ogre::Vector3 startCord, Ogre::Vector3 goalCord)
{

	NavmeshTile *start = findTile(startCord);

	if(start == NULL){
		std::vector<Ogre::Vector3> tmp;
		return tmp;
	}
	NavmeshTile *goal = findTile(goalCord);
	if(goal == NULL){
		std::vector<Ogre::Vector3> tmp;
		return tmp;
	}
	calcHeuristics(goal);

	return findPath(start, goal);


}



















}
