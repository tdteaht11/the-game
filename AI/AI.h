#ifndef AI_H
#define AI_H

#include "PreProcessor.h"
#include "StateMachine.h"
#include "Navmesh.h"


namespace AIS{

class AI{

	Ogre::Vector3 location;
	std::string ID;
	std::string targetID;
	const double lineOfSightDist;
	double outOfSightTimer;
	double initTime;
	bool toggle;
	std::vector<Ogre::Vector3> checkPointVector;
	std::vector<Ogre::Vector3>::iterator checkPointIterator;
	bool chase;
	bool gotCheckpoint;

	Navmesh myNav;

public:

	AI();
	AI(const AI &ai);
	virtual ~AI();

	Ogre::Vector3 getLocation() const;
	void setLocation(const Ogre::Vector3 &value);
	std::string getID();
	void setID(std::string ID);
	void setCheckpointBool(bool in);
	bool getCheckpointBool();
	std::vector<Ogre::Vector3> getCheckpointVector();

	void setTarget(std::string targetID);
	Ogre::Vector3 findTargetLocation();
	std::string getTarget();

	void setChase(bool in);
	bool getChase();



	bool inLineOfSight();
	virtual StateMachine*getMyStateMachine() const = 0;
	void updateTimer();
	double getTimer();
	void resetTimer();
	bool getToggle();
	void Toggle();

	const std::vector<Ogre::Vector3>::iterator getCheckPoint();
	void setCheckPointVector(std::vector<Ogre::Vector3> in);
	void incCheckPoint();

	void setNavmesh(Navmesh nav);
	Navmesh getNavmesh();


	//virtual void setMyStateMachine(StateMachine *value) = 0;
};

}



#endif // AI_H
