TEMPLATE = lib
CONFIG -= app_bundle
CONFIG -= qt

TARGET = ai
DESTDIR = $${PWD}/../lib

!win32: debug: DEFINES += _DEBUG DEBUG

include(../Ogre.pri)
include(../TinyXML.pri)

HEADERS += \
    AIRoot.h \
    PreProcessor.h \
    AI.h \
    StateMachine.h \
    State.h \
    AIMessenger.h \
    IdleState.h \
    AttackState.h \
    GroundAI.h \
    AIS.h \
    AIObject.h \
    Navmesh.h \
    Navmeshtile.h \
    helpTools.h \
    DefaultState.h \
    PlyNavmeshParser.h \
    Paths.h \
    SeekState.h \
    HelpFunctions.h \
    Paths.h \
    Paths.h

SOURCES += \
    AIRoot.cpp \
    AI.cpp \
    AttackState.cpp \
    IdleState.cpp \
    AIObject.cpp \
    StateMachine.cpp \
    Navmesh.cpp \
    Navmeshtile.cpp \
    DefaultState.cpp \
    PlyNavmeshParser.cpp \
    SeekState.cpp
