#ifndef DEFAULTSTATE_H
#define DEFAULTSTATE_H

#include "State.h"
#include "PreProcessor.h"

namespace AIS {

class DefaultState:public State{

	DefaultState():State(){

	}
	Ogre::Vector3 goal;
	int i;
public:

	/**
	 * @brief Instance Creates a singelton of this object
	 * @return
	 */
	static DefaultState &Instance();


	void Enter(AI *ai);
	void Execute(AI *ai);
	void Exit(AI *ai);
	void setGoal(Ogre::Vector3 goal);

	virtual ~DefaultState();

};



}
#endif // DEFAULTSTATE_H
