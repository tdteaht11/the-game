#ifndef PLYNAVMESHPARSER_H
#define PLYNAVMESHPARSER_H
#include "Navmesh.h"
#include "Navmeshtile.h"
#include "PreProcessor.h"


namespace AIS{
class plyNavmeshParser{

	std::vector<Ogre::Vector3> vertexVector;

public:
	Navmesh run();

};
}



#endif // PLYNAVMESHPARSER_H
