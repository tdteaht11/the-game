#include "AI.h"
#include "AIRoot.h"


namespace AIS{
Ogre::Vector3 AI::getLocation() const
{
	return this->location;
}

void AI::setLocation(const Ogre::Vector3 &value)
{
	location = value;
}

std::string AI::getID()
{
	return ID;
}

void AI::setID(std::string ID)
{
	this->ID = ID;
}

void AI::setCheckpointBool(bool in)
{
	this->gotCheckpoint = in;
}

bool AI::getCheckpointBool()
{
	return this->gotCheckpoint;
}

std::vector<Ogre::Vector3> AI::getCheckpointVector()
{
	return this->checkPointVector;
}

void AI::setTarget(std::string targetID)
{
	this->targetID = targetID;
}

Ogre::Vector3 AI::findTargetLocation()
{
	Ogre::Vector3 location = AIRoot::Instance().getPlayerMap().find(this->targetID)->second;
	return location;
}

std::string AI::getTarget()
{
	return this->targetID;
}

void AI::setChase(bool in)
{
	this->chase = in;
}

bool AI::getChase()
{
	return chase;
}

Navmesh AI::getNavmesh()
{
	return this->myNav;
}

bool AI::inLineOfSight()
{
	AIRoot::PlayerList tmp = AIRoot::Instance().getPlayerMap();
	AIRoot::PlayerList::const_iterator it = tmp.find(this->targetID);
	Ogre::Vector3 loc;
	if(it != tmp.end() && this->getLocation().distance(loc)<lineOfSightDist){
		loc = it->second;
		AIRoot::Instance().getMessenger()->requestLineOfSightCheck(this->getLocation(),loc);
	}

	return false;

}

void AI::updateTimer()
{
	outOfSightTimer = Ogre::Root::getSingleton().getTimer()->getMilliseconds()-initTime;
}

double AI::getTimer()
{
	return outOfSightTimer;
}

void AI::resetTimer()
{
	initTime = Ogre::Root::getSingleton().getTimer()->getMilliseconds();
	outOfSightTimer = 0;
}

bool AI::getToggle()
{
	return toggle;
}

void AI::Toggle()
{
	toggle = !toggle;
}

const std::vector<Ogre::Vector3>::iterator AI::getCheckPoint()
{
	return this->checkPointIterator;
}

void AI::setCheckPointVector(std::vector<Ogre::Vector3> in)
{
	this->checkPointVector = in;
	this->checkPointIterator = this->checkPointVector.begin();
	this->setCheckpointBool(true);
}

void AI::incCheckPoint()
{
	checkPointIterator++;
}

void AI::setNavmesh(Navmesh nav)
{
	this->myNav = nav;
}

AI::AI() : lineOfSightDist(50), outOfSightTimer(0), initTime(Ogre::Root::getSingleton().getTimer()->getMilliseconds()), toggle(true), gotCheckpoint(false), chase(false)
{
	/*Ogre::Vector3 tmp1(20,0,10);
	Ogre::Vector3 tmp2(10,0,0);
	Ogre::Vector3 tmp3(20,0,-10);

	checkPointVector.push_back(tmp1);
	checkPointVector.push_back(tmp2);
	checkPointVector.push_back(tmp3);
	checkPointIterator = checkPointVector.begin();*/

}

AI::AI(const AI &ai): lineOfSightDist(50)
{

}

AI::AI::~AI()
{

}
}
