#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "PreProcessor.h"
#include "State.h"


namespace AIS{
class AI;
class StateMachine{

	//Pointer to the owner of this statemachine
	AI *owner;

	//Pointer to the active stateowner
	State *currentState;
	//Pointer to the last used state
	State *lastState;
	//Pointer to the global state
	State *globalState;


public:

	//Initialize the statemachine
	StateMachine(AI *owner);

	//Set the diffrent states
	void setCurrentState(State *state);
	void setLastState(State *state);
	void setGlobalState(State *state);

	/**
	 * @brief update executes the currentState and the globalState
	 */
	void update() const;

	//Changes the active state to a new state, the old active state becomes the last used state
	/**
	 * @brief changeState changes the currentState to a new state
	 * @param newState is the new state
	 */
	void changeState(State *newState);

	/**
	 * @brief useLastState reverts and uses the preverius state
	 */
	void useLastState();

};


}


#endif // STATEMACHINE_H
