
#include "DefaultState.h"
#include "AttackState.h"
#include "IdleState.h"
#include "AIRoot.h"
#include "Navmesh.h"
#include "HelpFunctions.h"

namespace AIS {


DefaultState &DefaultState::Instance()
{
	static DefaultState instance;

	return instance;

}

void DefaultState::Enter(AI *ai)
{

}

void DefaultState::Execute(AI *ai)
{

	//Locations of player and bott
	Ogre::Vector3 playerPos = AIRoot::Instance().getMessenger()->requestPlayerLocation();
	Ogre::Vector3 bottPos = AIRoot::Instance().getMessenger()->requestLocation(ai->getID());
	Ogre::Vector3 asd = goal;
	asd.y = 1.5;
	bool playerlineOfSight = AIRoot::Instance().getMessenger()->requestLineOfSightCheck(bottPos,playerPos);
	bool goalLineOfSight = AIRoot::Instance().getMessenger()->requestLineOfSightCheck(bottPos,goal);
	//std::cout<<"playerLineOfSight "<<playerlineOfSight<<std::endl;
	//bool goalLineOfSight =helpLineOfSight(bottPos,asd);
	if((playerPos - bottPos).length()< 10  /*&& !playerlineOfSight*/){
		std::cout<<"change state to attack"<<std::endl;

		ai->getMyStateMachine()->changeState(&AttackState::Instance());
	}else if((goal - bottPos).length()<3){
		ai->setCheckpointBool(false);
		ai->getMyStateMachine()->changeState(&IdleState::Instance());
	}else if(goalLineOfSight){
		if(ai->getCheckpointBool()){
			Ogre::Vector3 checkpoint = *ai->getCheckPoint();
			if((bottPos - checkpoint).length() < 2){
				ai->incCheckPoint();
				checkpoint = *ai->getCheckPoint();
			}
			AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),checkpoint);
			AIRoot::Instance().getMessenger()->requestMove(ai->getID());


		}else{
			ai->setCheckPointVector(ai->getNavmesh().runPathfinder(bottPos,goal));
			ai->setCheckpointBool(true);
			if(!ai->getToggle()){
				ai->Toggle();
			}
		}
	}/*else if(ai->getTimer()>3000 && !ai->getToggle()){
		AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),goal);
		AIRoot::Instance().getMessenger()->requestMove(ai->getID());
	}*/else{
	/*if(ai->getToggle()){
			ai->resetTimer();
			ai->Toggle();
		}

		ai->updateTimer();*/

		AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),goal);
		AIRoot::Instance().getMessenger()->requestMove(ai->getID());

	}


	AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),goal);
	AIRoot::Instance().getMessenger()->requestMove(ai->getID());




	//std::cout<<AIRoot::Instance().getMessenger()->requestLocation(ai->getID())<<std::endl;

	//Ogre::Vector3 myPos = AIRoot::Instance().getMessenger()->requestPlayerLocation();


	/*bool playerBottSight = AIRoot::Instance().getMessenger()->requestLineOfSightCheck(currentPos,playerPos);
	float lenght = (currentPos-playerPos).length();
	if((currentPos-playerPos).length()<15){
		//If a player comes to close to the bot i changes state to attack
		std::cout<<"change state to attack"<<std::endl;
		ai->resetTimer();
		ai->getMyStateMachine()->changeState(&AttackState::Instance());
	}else if((goal-currentPos).length()<3){
		//Do nothing
		ai->getMyStateMachine()->changeState(&IdleState::Instance());
	}else{
		AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),this->goal);
		AIRoot::Instance().getMessenger()->requestMove(ai->getID());
	}*/

	//std::cout<<AIRoot::Instance().getMessenger()->requestPlayerLocation()<<std::endl;



	//Ogre::Vector3 playerPos = -AIRoot::Instance().getMessenger()->requestPlayerLocation();

	//Ogre::Vector3 currentPos(14,0,-25.4);
	//Ogre::Vector3 playerPos(11.3,0,20.9);



}

void DefaultState::Exit(AI *ai)
{

}

void DefaultState::setGoal(Ogre::Vector3 goal)
{
	this->goal = goal;
}

DefaultState::~DefaultState()
{

}

}
