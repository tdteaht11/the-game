#include "AIRoot.h"

namespace AIS {

AIRoot::AIRoot()
{

}

void AIRoot::setNavmesh(Navmesh in)
{
	this->defaultNavmesh = in;

	//cout
	std::cout<<"Navmeshsize"<<std::endl;
	std::cout<<defaultNavmesh.size()<<std::endl;
	//
}

Navmesh AIRoot::getNavmesh()
{
	return defaultNavmesh;
}

AIRoot::~AIRoot()
{
	for(AIList::iterator it = entitys.begin(); it != entitys.end(); it++) {
		delete it->second;
	}
}

 AIRoot::PlayerList AIRoot::getPlayerMap() const
{
	return playerMap;
}

void AIRoot::setPlayerMap(std::string id, Ogre::Vector3 loc)
{
	this->playerMap[id] = loc;
}


AIRoot::AIList &AIRoot::getEntities()
{
	return entitys;
}

void AIRoot::setAIEntities(std::string id, AI *ai)
{
	std::cout<<"-------AIID-------"<<std::endl;
	std::cout<<id<<std::endl;
	this->entitys[id] = ai;
}

AIMessenger *AIRoot::getMessenger() const
{
	return msgr;
}

void AIRoot::setMessenger(AIMessenger *msgr)
{
	this->msgr = msgr;
}


}
