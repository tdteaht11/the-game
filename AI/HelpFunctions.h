#ifndef HELPFUNCTIONS_H
#define HELPFUNCTIONS_H

#include "PreProcessor.h"
#include "AIRoot.h"

namespace AIS{
inline bool helpLineOfSight(Ogre::Vector3 start, Ogre::Vector3 end){


	Ogre::Vector3 direction = end - start;
	direction.normalise();

	Ogre::Vector3 leftDir = direction.perpendicular();
	Ogre::Vector3 rightDir = -direction.perpendicular();

	Ogre::Vector3 leftStart = start +1.4*leftDir;
	Ogre::Vector3 rightStart = start + 1.4*rightDir;

	double l1 = (leftStart-start).length();
	double l2 = (rightStart - start).length();

	std::cout<<l1<<" "<<l2<<std::endl;

	bool leftSight = AIRoot::Instance().getMessenger()->requestLineOfSightCheck(leftStart,end);
	bool Sight = AIRoot::Instance().getMessenger()->requestLineOfSightCheck(start,end);
	bool rightSight = AIRoot::Instance().getMessenger()->requestLineOfSightCheck(rightStart,end);

	if(!leftSight && !Sight && !rightSight){
		return false;
	}

	return true;
}
}






#endif // HELPFUNCTIONS_H
