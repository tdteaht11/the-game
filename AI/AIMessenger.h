#ifndef AIMESSENGER_H
#define AIMESSENGER_H

#include "PreProcessor.h"

namespace AIS {

class AIMessenger{
public:

	/**
	 * @brief Request an AI to move forward
	 * @param id determines wich AI to be moved
	 */
	virtual void requestMove(const std::string &id) = 0;

	/**
	 * @brief requestStop turn of the movment of the ai
	 * @param id of the ai
	 */
	//virtual void requestStop(const std::string &id) = 0;

	/**
	 * @brief requestAttack requests a AI to preform a attack
	 * @param id determines wich AI to preform the attack
	 */
	virtual void requestAttack(const std::string &id) = 0;

	/**
	 * @brief requestAttack request a AI to preform a attack for  weapons with projektiles with gravity
	 * @param id determins wich AI to preform the attack
	 * @param angle the angle of the shoot
	 */
	virtual void requestRangedAttack(const std::string &id, const Ogre::Radian &angle) = 0;

	/**
	 * @brief requestBlock requests a AI to preform a block
	 * @param id determines wich AI to preform the block
	 */
	virtual void requestBlock(const std::string &id) = 0;

	/**
	 * @brief Request the location of a character
	 * @param id
	 */
	virtual Ogre::Vector3 requestLocation(const std::string &id) = 0;


	/**
	 * @brief requestActiveToolInfo check wich tool is active
	 * @param id of the desired unit
	 * @return
	 */
	virtual int requestActiveToolInfo(const std::string &id) = 0;

	/**
	 * @brief requestLineOfSightCheck check if two unites can see each other
	 * @param origin
	 * @param target
	 * @return true if they can see each other
	 */
	virtual bool requestLineOfSightCheck(const Ogre::Vector3 &origin, const Ogre::Vector3 &target) = 0;

	/**
	 * @brief requestTurn requests a turn of a ai, the turn angle can be big so the gameengine must take care of the turn
	 * so it isnt to big for one frame (ex: 90 degre turn in one fram is to big)
	 * @param id of the ai
	 * @param angle to turn
	 */
	virtual void requestTurn(const std::string &id, const Ogre::Radian &angle) = 0;
	virtual void requestTurn(const std::string &id, const Ogre::Vector3 &pos) = 0;

	virtual Ogre::Vector3 requestPlayerLocation() = 0;

};





}



#endif // AIMESSENGER_H
