#include "IdleState.h"
#include "AttackState.h"
#include "AIRoot.h"
#include "HelpFunctions.h"

AIS::IdleState &AIS::IdleState::Instance()
{
	static IdleState instance;

	return instance;
}

AIS::IdleState::~IdleState()
{

}

void AIS::IdleState::Enter(AIS::AI *ai)
{

}

void AIS::IdleState::Execute(AIS::AI *ai)
{
	Ogre::Vector3 myPos = AIRoot::Instance().getMessenger()->requestLocation(ai->getID());
	Ogre::Vector3 playerPos = AIRoot::Instance().getMessenger()->requestPlayerLocation();

	bool playerlineOfSight = AIRoot::Instance().getMessenger()->requestLineOfSightCheck(myPos,playerPos);

	std::cout<<playerlineOfSight<<std::endl;

	if((myPos-playerPos).length()<15){
		std::cout<<"Change to attack"<<std::endl;
		ai->getMyStateMachine()->changeState(&AttackState::Instance());
	}



}

void AIS::IdleState::Exit(AIS::AI *ai)
{

}
