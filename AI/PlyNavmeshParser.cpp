#include "PlyNavmeshParser.h"
#include "Paths.h"

namespace AIS{

Navmesh plyNavmeshParser::run()
{
	Navmesh nav;
	std::ifstream infile(NAVMESH);
	std::string line;
	int vertexCount;
	int faceCount;
	int headerPlacement;
	while(std::getline(infile,line)){
		std::size_t num=line.find("element vertex");
		std::size_t num2=line.find("element face");
		std::size_t num3=line.find("end_header");
		if(num!=std::string::npos){
			vertexCount = std::atoi(line.substr(14).c_str());
		}
		if(num2!=std::string::npos){
			faceCount = std::atoi(line.substr(12).c_str());
		}
		if(num3!=std::string::npos){
			Ogre::Vector3 tmp;
			std::getline(infile,line);
			for(int i=0;i<vertexCount;i++,std::getline(infile,line)){
				std::size_t found1 = line.find(" ");
				std::size_t found2 = line.find(" ",found1+1);
				std::size_t found3 = line.find(" ",found2+1);

				tmp.x = (Ogre::StringConverter::parseReal(line.substr(0,found1-1)));
				tmp.z = -(Ogre::StringConverter::parseReal(line.substr(found1+1,found2-found1)));
				tmp.y = (Ogre::StringConverter::parseReal(line.substr(found2+1,found3-found2)));
				vertexVector.push_back(tmp);
			}
			std::vector<std::vector<int> > vert;
			for(int i=0;i<faceCount;i++,std::getline(infile,line)){
				std::size_t found1 = line.find(" ");
				std::size_t found2 = line.find(" ",found1+1);
				std::size_t found3 = line.find(" ",found2+1);
				int v1 = std::atoi(line.substr(found1+1,found2-found1).c_str());
				int v3 = std::atoi(line.substr(found2+1,found3-found2).c_str());
				int v2 = std::atoi(line.substr(found3+1).c_str());
				nav.addNavmeshTile(vertexVector[v1],vertexVector[v2],vertexVector[v3]);

				std::vector<int> t;
				t.push_back(v1);
				t.push_back(v2);
				t.push_back(v3);
				vert.push_back(t);
			}
			for(int i=0;i<vert.size();i++){

				int v1 = vert[i][0];
				int v2 = vert[i][1];
				int v3 = vert[i][2];
				for(int j=0;j<vert.size();j++){
					if(i!=j){
						int count = 0;
						int cv1 = vert[j][0];
						int cv2 = vert[j][1];
						int cv3 = vert[j][2];

						if(v1 == cv1 || v1 == cv2 || v1 == cv3){
							count++;
						}
						if(v2 == cv1 || v2 == cv2 || v2 == cv3){
							count++;
						}
						if(v3 == cv1 || v3 == cv2 || v3 == cv3){
							count++;
						}
						if(count == 2){
							nav.setAdjacent(i,j);
						}
					}
				}
			}
		}

	}
	return nav;


}





}
