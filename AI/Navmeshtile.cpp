#include "Navmeshtile.h"
namespace AIS {


NavmeshTile::NavmeshTile()
{

}

NavmeshTile::NavmeshTile(const NavmeshTile &in)
{
	this->f = in.f;
	this->h = in.h;
	this->g = in.g;
	this->adjacent = in.adjacent;
	this->midpoint = in.midpoint;
	this->parent = in.parent;
	this->point1 = in.point1;
	this->point2 = in.point2;
	this->point3 = in.point3;
}


void NavmeshTile::setBounds(Ogre::Vector3 v3, Ogre::Vector3 v2, Ogre::Vector3 v1)
{
	this->point3 = v3;
	this->point2 = v2;
	this->point1 = v1;
	this->midpoint = (v3+v2+v1)/3;
}

std::vector<Ogre::Vector3> NavmeshTile::getBounds()
{
	std::vector <Ogre::Vector3> tmp;
	tmp.push_back(this->point1);
	tmp.push_back(this->point2);
	tmp.push_back(this->point3);

	return tmp;
}

Ogre::Vector3 NavmeshTile::getMidpoint()
{
	return this->midpoint;
}

void NavmeshTile::setH(double val)
{
	this->h = val;
}

double NavmeshTile::getH()
{
	return this->h;
}

void NavmeshTile::setG(double val)
{
	this->g = val;
}

double NavmeshTile::getG()
{
	return this->g;
}

void NavmeshTile::calcF()
{
	this->f = this->g + this->h;
}

double NavmeshTile::getF()
{
	return this->f;
}

void NavmeshTile::addAdjacent(int i)
{
	this->adjacent.push_back(i);
}

std::vector<int> NavmeshTile::getAdjacent()
{
	return this->adjacent;
}

void NavmeshTile::setParent(NavmeshTile *parent)
{
	this->parent = parent;
}

NavmeshTile *NavmeshTile::getParent()
{
	return this->parent;
}


}
