#ifndef NAVMESH_H
#define NAVMESH_H
#include "PreProcessor.h"
#include "Navmeshtile.h"

namespace AIS {
/**
 * @brief The Navmesh class Holds the whole maprepresentation
 */

class Navmesh
{

	std::vector<NavmeshTile> tileVector;

	//Pathfinding
	NavmeshTile* findTile(Ogre::Vector3 point);
	float sign(Ogre::Vector3 p1, Ogre::Vector3 p2, Ogre::Vector3 p3);
	std::vector<Ogre::Vector3> findPath(NavmeshTile *start, NavmeshTile *goal);
	std::vector<Ogre::Vector3> reconstructPath(NavmeshTile lastTile, NavmeshTile *start, NavmeshTile *goal);
	void calcHeuristics(NavmeshTile *goal);
	bool PointInTriangle(Ogre::Vector3 pt, NavmeshTile curr);
	bool isInVector(NavmeshTile tile, std::vector<NavmeshTile*> vec);
	std::vector<Ogre::Vector3> smoothPath(std::vector<Ogre::Vector3> in, NavmeshTile *goal);

public:
	Navmesh();
	void addNavmeshTile(Ogre::Vector3 v3, Ogre::Vector3 v2, Ogre::Vector3 v1);
	void setAdjacent(int current, int check);
	std::vector<NavmeshTile> getTileVector();
	int size();

	//Pathfinding relaterat
	std::vector<Ogre::Vector3> runPathfinder(Ogre::Vector3 startCord, Ogre::Vector3 goalCord);





};

}

#endif // NAVMESH_H

