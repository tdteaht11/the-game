#ifndef GROUNDAI_H
#define GROUNDAI_H



#include "PreProcessor.h"
#include "AI.h"
#include "Navmesh.h"

namespace AIS{
class GroundAI:public AI
{

	StateMachine *myStateMachine;
	Navmesh nav;
public:

	GroundAI() : AI()
	{
		myStateMachine = new StateMachine(this);
	}

	GroundAI(const GroundAI &ai);
	virtual ~GroundAI(){
		delete myStateMachine;
	}

	/**
	 * @brief update tells the StateMachine to preform the active State
	 */
	void update(){
		myStateMachine->update();
	}
	virtual StateMachine*getMyStateMachine() const{
		return myStateMachine;
	}


};
}
#endif // GROUNDAI_H
