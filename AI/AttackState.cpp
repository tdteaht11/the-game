#include "AttackState.h"
#include "AIRoot.h"
#include "DefaultState.h"
#include "SeekState.h"
#include "HelpFunctions.h"

namespace AIS{

Ogre::Radian AttackState::calcLowAngle(Ogre::Vector3 myLocation, Ogre::Vector3 targetLocation){
	Ogre::Real distance;
	Ogre::Real yDiff = myLocation.y - targetLocation.y;
	myLocation.y = 0;
	targetLocation.y = 0;

	distance = myLocation.distance(targetLocation);

	Ogre::Radian angle = Ogre::Radian(Ogre::Math::PI); // Ogre::Math::ATan(std::pow(80,2)-std::sqrt(std::pow(80,4)-9.8*(9.8*std::pow(distance,2)+2*yDiff*std::pow(80,2)))/(9.8*distance));
	return angle;
}

Ogre::Radian AttackState::calcHighAngle(Ogre::Vector3 myLocation, Ogre::Vector3 targetLocation)
{
	Ogre::Real distance;
	Ogre::Real yDiff = myLocation.y - targetLocation.y;
	myLocation.y = 0;
	targetLocation.y = 0;

	distance = myLocation.distance(targetLocation);

	Ogre::Radian angle = Ogre::Radian(Ogre::Math::PI); // Ogre::Math::ATan(std::pow(80,2)+std::sqrt(std::pow(80,4)-9.8*(9.8*std::pow(distance,2)+2*yDiff*std::pow(80,2)))/(9.8*distance));
	return angle;
}

bool AttackState::inAttackRange(Ogre::Vector3 myLocation, Ogre::Vector3 targetLocation)
{

	/*	if(std::abs(targetLocation - myLocation) < meleeAttackRange){
		return true;
	}*/

	return false;
}

bool AttackState::isLookingAtTarget(Ogre::Vector3 targetLocation)
{
	return true;
}

AttackState &AttackState::Instance()
{
	static AttackState instance;
	return instance;
}

void AttackState::Enter(AI *ai)
{

}

void AttackState::Execute(AI *ai)
{
	Ogre::Vector3 bottPos = AIRoot::Instance().getMessenger()->requestLocation(ai->getID());
	Ogre::Vector3 playerPos = AIRoot::Instance().getMessenger()->requestPlayerLocation();
	bool sight = AIRoot::Instance().getMessenger()->requestLineOfSightCheck(bottPos,playerPos);

	//Ranged kod
	//if(!sight){
		if((bottPos-playerPos).length()>10 && (bottPos-playerPos).length()<30){
			Ogre::Radian angle = calcLowAngle(bottPos, playerPos);
			AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),playerPos);
			AIRoot::Instance().getMessenger()->requestRangedAttack(ai->getID(),angle);
		}else if((bottPos-playerPos).length()>30 && (bottPos-playerPos).length()<60){
			AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),playerPos);
			AIRoot::Instance().getMessenger()->requestMove(ai->getID());
		}else if((bottPos-playerPos).length()>60){
			ai->getMyStateMachine()->changeState(&DefaultState::Instance());
		}else{
			AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),playerPos);
			AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),Ogre::Radian(Ogre::Math::PI));
			AIRoot::Instance().getMessenger()->requestMove(ai->getID());
		}
	/*}else if(sight && ai->getChase()){
		/*std::cout<<"change to seek"<<std::endl;
		ai->getMyStateMachine()->changeState(&SeekState::Instance());*/
	//}else{
		/*std::cout<<"change to default"<<std::endl;
		ai->getMyStateMachine()->changeState(&DefaultState::Instance());*/
	//}




	//if(AIRoot::Instance().getMessenger()->requestLineOfSightCheck(myPos,ai->getGoalPoint())){

	//Satserna överlappar varandra för att skapa en zon där botten inte skjuter på spelaren när den flyr
	//Fly
	/*if((myPos-playerPos).length()<10){
		//Temporär lösning
		AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),playerPos);
		AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),Ogre::Radian(Ogre::Math::PI));
		AIRoot::Instance().getMessenger()->requestMove(ai->getID());
		ai->setChase(true);
		ai->resetTimer();

	}else if( 5<(myPos-playerPos).length() && (myPos-playerPos).length()<30){
		//Attack
		//If the distance is to short to the target the bott retreats

		AIRoot::Instance().getMessenger()->requestTurn(ai->getID(), playerPos);
		AIRoot::Instance().getMessenger()->requestRangedAttack(ai->getID(),angle);
		ai->setChase(true);
		ai->resetTimer();

	}else if((myPos-playerPos).length()>30 && ai->getChase() == true){
		if(ai->getTimer()>10){
			ai->setChase(false);
			std::cout<<"Change state to default"<<std::endl;
			ai->getMyStateMachine()->changeState(&DefaultState::Instance());
		}
		AIRoot::Instance().getMessenger()->requestTurn(ai->getID(),playerPos);
		AIRoot::Instance().getMessenger()->requestMove(ai->getID());
		ai->updateTimer();
	}/*else{
		ai->getMyStateMachine()->changeState(&DefaultState::Instance());
	}*/



}

void AttackState::Exit(AI *ai)
{

}
}

