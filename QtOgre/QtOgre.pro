TEMPLATE = lib
macx: CONFIG += lib

TARGET = qtogre
DESTDIR = $${PWD}/../lib

SOURCES += \
    QOgreWindow.cpp \
    QOgreWidget.cpp
HEADERS += \
    PreProcessor.h \
    QtOgre.h \
    QOgreWindow.h \
    QOgreWidget.h \
    WindowEventListener.h \
    QOgreApplication.h

include(../Ogre.pri)
include(../Qt.pri)
